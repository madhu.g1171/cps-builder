package com.tui.uk.client.domain;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.Date;

public class ShorexExcursions implements Serializable
{
	/** The generated serial version id. */
	private static final long serialVersionUID = 6009048897295838337L;

	private String excursionName;
	private String portName;
	private String countryName;
	private int numberOfPassengers;
	private Money excursionCost;
	/**
    *  date object.
    */
   private Date date;
   private LocalTime localTime;
   /**
    *  This field is for displaying purpose.
    */
   private String formatedDateValue;
   /**
    *  returns formatedDate value in the form of E dd MMM yyyy.
    */
   public String getFormatedDateValue()
   {
      return formatedDateValue;
   }
   /**
    *  setting the formated Date value.
    */
   public void setFormatedDateValue(String formatedDateValue)
   {
      this.formatedDateValue = formatedDateValue;
   }
   /**
    *  days
    */
   private Integer days;
   /**
    *  returns days
    */
   public Integer getDays()
   {
      return days;
   }
   /**
    *  set days
    */
   public void setDays(Integer days)
   {
      this.days = days;
   }

   /**
    *  returns the dining time.
    */
   public LocalTime getLocalTime() {
      return localTime;
   }

   /**
    *  sets the dining time.
    */
   public void setLocalTime(LocalTime localTime) {
      this.localTime = localTime;
   } 
   /**
    * returns excursion time.
    */
   public Date getDate()
   {
      return date;
   }
   /**
    *  sets the excursion date and time .
    */
   public void setDate(Date date)
   {
      this.date = date;
   }
	/**
	 * @return the excursionName
	 */
	public String getExcursionName() {
		return excursionName;
	}
	/**
	 * @return the portName
	 */
	public String getPortName() {
		return portName;
	}
	/**
	 * @return the countryName
	 */
	public String getCountryName() {
		return countryName;
	}
	
	/**
	 * @return the excursionCost
	 */
	public Money getExcursionCost() {
		return excursionCost;
	}
	/**
	 * @param excursionName the excursionName to set
	 */
	public void setExcursionName(String excursionName) {
		this.excursionName = excursionName;
	}
	/**
	 * @param portName the portName to set
	 */
	public void setPortName(String portName) {
		this.portName = portName;
	}
	/**
	 * @param countryName the countryName to set
	 */
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	
	/**
	 * @param excursionCost the excursionCost to set
	 */
	public void setExcursionCost(Money excursionCost) {
		this.excursionCost = excursionCost;
	}
	/**
	 * @return the numberOfPassengers
	 */
	public int getNumberOfPassengers() {
		return numberOfPassengers;
	}
	/**
	 * @param numberOfPassengers the numberOfPassengers to set
	 */
	public void setNumberOfPassengers(int numberOfPassengers) {
		this.numberOfPassengers = numberOfPassengers;
	}

}
