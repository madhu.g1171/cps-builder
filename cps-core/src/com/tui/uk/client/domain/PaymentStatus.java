/**
 * 
 */
package com.tui.uk.client.domain;

/**
 * @author sreenivasulu.k
 *
 */
public enum PaymentStatus {

	/**
	 * Customer initiated the payment.
	 */
	INITIATED("Initiated"),

	/**
	 * Customer authenticated the payment.
	 */
	ATHENTICATED("Authenticated"),

	/**
	 * Customer Fulfilled the payment.
	 */
	FULFILLED("Fulfilled"),

	/**
	 * Customer Cancelled the payment.
	 */
	CANCELED("Canceled"),
	
	/**
	 *  Transaction Rejected
	 */
	REJECTED("Rejected");
	

	/**
	 * defines the payment status
	 */
	private String paymentStatus;
	
	/**
	 * defines datacash response code.
	 */
	private String datacashResponseCode;

	/**
	 * @returns datacash response code.
	 */
	public String getDatacashResponseCode() {
		return datacashResponseCode;
	}

	/**
	 * @param datacashResponseCode
	 */
	public void setDatacashResponseCode(String datacashResponseCode) {
		this.datacashResponseCode = datacashResponseCode;
	}

	/**
	 * constructor for initializing payment status.
	 * 
	 */
	PaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	/**
	 * gets the payment status
	 * 
	 * @return payment status.
	 */
	public String getPaymentStatus() {
		return paymentStatus;
	}

	/**
	 * sets the payment status
	 * 
	 * @param paymentStatus
	 */
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public static PaymentStatus findByCode(String paymentStatus) {
		for (PaymentStatus paymentStatusEnum : PaymentStatus.values()) {
			if (paymentStatusEnum.getPaymentStatus().equalsIgnoreCase(
					paymentStatus.trim())) {
				return paymentStatusEnum;
			}
		}
		throw new IllegalArgumentException("Unknown Payment Status:"
				+ paymentStatus);
	}

}
