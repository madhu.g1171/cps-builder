/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: Money.java,v $
 *
 * $Revision: 1.6 $
 *
 * $Date: 2008-05-13 09:03:26 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $ Revision 1.5 2008/05/13 08:48:38 sindhushree.g Implemented
 * Serializable for the Money class.
 *
 * Revision 1.4 2008/05/08 09:47:08 sindhushree.g Updated copyright information.
 *
 * Revision 1.3 2008/05/05 11:20:44 sindhushree.g Modified to resolve checkstyle issues.
 *
 * Revision 1.2 2008/05/02 09:20:55 sindhushree.g Added the constructor.
 *
 * Revision 1.1 2008/05/02 08:50:55 sindhushree.g Added the intial version for datacash service.
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

/**
 * This is an object class for money, which keeps the details such as amount
 * and currency.
 *
 * @author sindhushree.g
 *
 * @since 1.0 API
 */
public final class Money implements Serializable
{

   /** The serial version UID field. */
   private static final long serialVersionUID = -6562601517468038735L;

   /** The amount. */
   private BigDecimal amount;

   /** The currency. */
   private Currency currency;

   /** The symbol . */
   private String symbol;

   /**
    * Constructor to create Money object.
    *
    * @param amount The amount.
    * @param currency The currency of the amount specified.
    */
   public Money(BigDecimal amount, Currency currency)
   {
      this.amount = amount;
      this.currency = currency;
   }

   /**
    * Constructor to create Money object.
    *
    * @param amount The amount.
    * @param currency The currency of the amount specified.
    * @param locale Locale of currency.
    */
   public Money(BigDecimal amount, Currency currency, Locale locale)
   {
      this.amount = amount;
      this.currency = currency;
      this.symbol = this.currency.getSymbol(locale);
   }

   /**
    * Gets the amount.
    *
    * @return the amount
    */
   public BigDecimal getAmount()
   {
      return amount;
   }

   /**
    * Gets the currency.
    *
    * @return the currency
    */
   public Currency getCurrency()
   {
      return currency;
   }

   /**
    * Gets the Symbol of this currency.
    *
    * @return the Symbol of the currency.
    */
    public String getSymbol()
    {
       return symbol;
    }



   /**
     * Add the passed Money to this Money, returning a new Money.
     *
     * @param other the Money to be added to this Money.
     *
     * @throws RuntimeException may be thrown if the currencies aren't equal.
     *
     * @return a new Money representing the summed money.
     */
   public Money add(Money other)
   {
      this.assertSameCurrencyAs(other);
      return newMoney(amount.add(other.getAmount()));
   }

   /**
    * Subtract the passed Money from this Money, returning a new Money.
    *
    * @param other the Money to be subtracted from this Money.
    *
    * @return a new Money representing the resultant money.
    *
    * @throws RuntimeException may be thrown if the currencies aren't equal.
    */
   public Money subtract(Money other)
   {
      this.assertSameCurrencyAs(other);
      return newMoney(amount.subtract(other.getAmount()));
   }

   /**
    * Determines that the currency of another money instance is the same as this one.
    *
    * @param other the money instance to test against
    *
    * @throws RuntimeException if the currencies are not the same
    */
   private void assertSameCurrencyAs(Money other)
   {
      if (!currency.equals(other.getCurrency()))
      {
         throw new RuntimeException("Currency mismatch.");
      }
   }

   /**
    * Factory that constructs a new money instance with the same currency as this one.
    *
    * @param newAmount the newAmount
    *
    * @return the newly constructed instance
    */
   private Money newMoney(BigDecimal newAmount)
   {
      return new Money(newAmount, this.currency);
   }
}
