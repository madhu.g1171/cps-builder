package com.tui.uk.client.domain;

import java.io.Serializable;

public class LockYourPriceSummary implements Serializable
{

   /**
    * 
    */
   private static final long serialVersionUID = 1L;

   private boolean available;

   private boolean selected;

   private String ppPrice;

   private String totalPrice;

   private String cancellationPeriodInHours;

   private boolean mmbLockYourPriceFlow;

   private String clientApplication;

   private Money outstandingAmount;

   private Money outstandingAmountPP;

   /**
    * @return the available
    */
   public boolean isAvailable()
   {
      return available;
   }

   public Money getOutstandingAmount()
   {
      return outstandingAmount;
   }

   public void setOutstandingAmount(Money outstandingAmount)
   {
      this.outstandingAmount = outstandingAmount;
   }

   public Money getOutstandingAmountPP()
   {
      return outstandingAmountPP;
   }

   public void setOutstandingAmountPP(Money outstandingAmountPP)
   {
      this.outstandingAmountPP = outstandingAmountPP;
   }

   /**
    * @param available the available to set
    */
   public void setAvailable(boolean available)
   {
      this.available = available;
   }

   /**
    * @return the selected
    */
   public boolean isSelected()
   {
      return selected;
   }

   /**
    * @param selected the selected to set
    */
   public void setSelected(boolean selected)
   {
      this.selected = selected;
   }

   /**
    * @return the ppPrice
    */
   public String getPpPrice()
   {
      return ppPrice;
   }

   /**
    * @param ppPrice the ppPrice to set
    */
   public void setPpPrice(String ppPrice)
   {
      this.ppPrice = ppPrice;
   }

   /**
    * @return the totalPrice
    */
   public String getTotalPrice()
   {
      return totalPrice;
   }

   /**
    * @param totalPrice the totalPrice to set
    */
   public void setTotalPrice(String totalPrice)
   {
      this.totalPrice = totalPrice;
   }

   /**
    * @return the cancellationPeriodInHours
    */
   public String getCancellationPeriodInHours()
   {
      return cancellationPeriodInHours;
   }

   /**
    * @param cancellationPeriodInHours the cancellationPeriodInHours to set
    */
   public void setCancellationPeriodInHours(String cancellationPeriodInHours)
   {
      this.cancellationPeriodInHours = cancellationPeriodInHours;
   }

   public boolean isMmbLockYourPriceFlow()
   {
      return mmbLockYourPriceFlow;
   }

   public void setMmbLockYourPriceFlow(boolean mmbLockYourPriceFlow)
   {
      this.mmbLockYourPriceFlow = mmbLockYourPriceFlow;
   }

   public String getClientApplication()
   {
      return clientApplication;
   }

   public void setClientApplication(String clientApplication)
   {
      this.clientApplication = clientApplication;
   }
}
