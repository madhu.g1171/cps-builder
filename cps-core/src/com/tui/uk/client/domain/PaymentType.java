/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentType.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2008-06-02 07:44:25$
 *
 * $author: thomas.pm$
 *
 * $Log : $
 *
 */
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is responsible for holding all the payment details to be displayed
 * in the Payment Page. This holds the Payment Type to be rendered in Payment options
 * drop down in payment panel rendered by CPC. It holds the details such as payment type code,
 * payment description, payment charges, etc. Payment type can be different types of vouchers,
 * cash, different kinds of cheque, different types of cards, etc.,
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 */
public final class PaymentType implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = 3703605954472791970L;

   /** The description of payment type. */
   private String paymentDescription;

   /** The code of the payment type. */
   private String paymentCode;

   /** The payment method like  DataCash, PDQ, PIN PAD, Voucher, Cash, Cheque. */
   private String paymentMethod;

   /** The card details of this payment type. */
   private CardType cardType;

   /** The card category of this payment type. */
   private String cardCategory;


   /**
    * Constructor for the class with all attributes.
    *
    * @param paymentCode the payment code.
    * @param paymentDescription the payment description.
    * @param paymentMethod the payment method.
    */
   public PaymentType(String paymentCode, String paymentDescription, String paymentMethod)
   {
      this.paymentCode = paymentCode;
      this.paymentDescription = paymentDescription;
      this.paymentMethod = paymentMethod;
   }

   /**
    * Gets the payment code.
    *
    * @return the paymentCode
    */
   public String getPaymentCode()
   {
      return paymentCode;
   }

   /**
    * Gets the payment description.
    *
    * @return the paymentDescription
    */
   public String getPaymentDescription()
   {
      return paymentDescription;
   }

   /**
    * Gets the payment method.
    *
    * @return the paymentMethod
    */
   public String getPaymentMethod()
   {
      return paymentMethod;
   }

   /**
    * Gets the <code>CardType</code> object.
    *
    * @return the cardType.
    */
   public CardType getCardType()
   {
      return cardType;
   }

   /**
    * Sets the <code>CardType</code> object.
    *
    * @param cardType the cardType to set.
    */
   public void setCardType(CardType cardType)
   {
      this.cardType = cardType;
   }
  /**
    * Gets the card category.
    *
    * @return the card category
    */
   public String getCardCategory()
   {
     return cardCategory;
   }
   /**
    * Sets the cardCategory .
    *
    * @param cardCategory the cardCategory to set.
    */
   public void setCardCategory(String cardCategory)
   {
      this.cardCategory = cardCategory;
   }


}
