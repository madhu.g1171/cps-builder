package com.tui.uk.client.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 *Holds the value of shell details.
 * @author Rohitkumar.m
 *
 * @since 1.0 API
 */
public final class ShellSummary implements Serializable
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1511650863504466403L;


	/** The summary of accommodation details. */
	private List<AccommodationSummary> multiCentreAccommodationSummary;
	
	/** The summary of centers. */
	private List<CentreSummary> centresSummary;

	/** The start date of accommodation. */
	private Date startDate;

	/** The end date of accommodation. */
	private Date endDate;

	/** The duration of accommodation. */
	private int duration;

	/** The selling code. */
	private String sellingCode;

	/** The selling code. */
	private String name;
	
	/** The Image URL. */
	private String imageUrl;

	public ShellSummary(){
		
	}

	public ShellSummary(
			List<AccommodationSummary> multiCentreAccommodationSummary,
			List<CentreSummary> centresSummary, Date startDate, Date endDate,
			int duration, String sellingCode, String name, String imageUrl) {
		super();
		this.multiCentreAccommodationSummary = multiCentreAccommodationSummary;
		this.centresSummary = centresSummary;
		this.startDate = startDate;
		this.endDate = endDate;
		this.duration = duration;
		this.sellingCode = sellingCode;
		this.name = name;
		this.imageUrl = imageUrl;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = duration;
	}

	public String getSellingCode() {
		return sellingCode;
	}

	public void setSellingCode(String sellingCode) {
		this.sellingCode = sellingCode;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	
	public List<AccommodationSummary> getMultiCentreAccommodationSummary() {
		return multiCentreAccommodationSummary;
	}

	public void setMultiCentreAccommodationSummary(
			List<AccommodationSummary> multiCentreAccommodationSummary) {
		this.multiCentreAccommodationSummary = multiCentreAccommodationSummary;
	}

	public List<CentreSummary> getCentresSummary() {
		return centresSummary;
	}

	public void setCentresSummary(List<CentreSummary> centresSummary) {
		this.centresSummary = centresSummary;
	}
	

}