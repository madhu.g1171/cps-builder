/*
* Copyright (C)2006 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   BookingType.java$
*
* $Revision:   $
*
* $Date:   Dec 9, 2009$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.client.domain;

/**
 * Enumeration for PaymentFlow. This enumeration is used to distinguish the
 * payment flow name which is used to render the screen based on the name.
 * @author veena.bn
 *
 */
public enum PaymentFlow
{
   /** Constant for Payment. */
   PAYMENT("PAYMENT"),

   /** Constant for No Payment. */
   NO_PAYMENT("NO_PAYMENT"),

   /** Constant for Refund. */
   REFUND("REFUND");

   /**
    * Constructor for paymentFlow.
    * @param paymentFlow the paymentFlow.
    */
   private PaymentFlow(String paymentFlow)
   {
      this.paymentFlow = paymentFlow;
   }

   /** The paymentFlow. */
   private String paymentFlow;

   /**
    * Gets the paymentFlow.
    * @return paymentFlow the paymentFlow name.
    */
   public String getPaymentFlow()
   {
      return paymentFlow;
   }

   /**
    * Determine the payment flow name.
    *
    * @param paymentFlow the payment flow name.
    *
    * @return the paymentFlowName.
    *
    */
   public static PaymentFlow findByPaymentFlowCode(String paymentFlow)
   {
      for (PaymentFlow payFlow : values())
      {
         if (payFlow.getPaymentFlow().equals(paymentFlow))
         {
            return payFlow;
         }
      }
      return null;
   }

}
