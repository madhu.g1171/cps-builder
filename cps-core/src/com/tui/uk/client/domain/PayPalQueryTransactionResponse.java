package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalQueryTransactionResponse implements Serializable{

	/** The generated serial version id. */
	private static final long serialVersionUID = -3473835819078423669L;

	private String information;
	private String build;
	private String timestamp;
	private String transactionId;
	private String PayPalVersion;
	private String datacashReferance;
	private String status;
	private String reason;
	private PayPalTransaction payPalTransaction;
	/**
	 * @return the acknowledgement
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * @param acknowledgement the acknowledgement to set
	 */
	public void setInformation(String information) {
		this.information = information;
	}

	/**
	 * @return the build
	 */
	public String getBuild() {
		return build;
	}

	/**
	 * @param build the build to set
	 */
	public void setBuild(String build) {
		this.build = build;
	}

	/**
	 * @return the timestamp
	 */
	public String getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the transactionId
	 */
	public String getTransactionId() {
		return transactionId;
	}

	/**
	 * @param transactionId the transactionId to set
	 */
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	/**
	 * @return the payPalVersion
	 */
	public String getPayPalVersion() {
		return PayPalVersion;
	}

	/**
	 * @param payPalVersion the payPalVersion to set
	 */
	public void setPayPalVersion(String payPalVersion) {
		PayPalVersion = payPalVersion;
	}

	/**
	 * @return the datacashReferance
	 */
	public String getDatacashReferance() {
		return datacashReferance;
	}

	/**
	 * @param datacashReferance the datacashReferance to set
	 */
	public void setDatacashReferance(String datacashReferance) {
		this.datacashReferance = datacashReferance;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the payPalTransaction
	 */
	public PayPalTransaction getPayPalTransaction() {
		return payPalTransaction;
	}

	/**
	 * @param payPalTransaction the payPalTransaction to set
	 */
	public void setPayPalTransaction(PayPalTransaction payPalTransaction) {
		this.payPalTransaction = payPalTransaction;
	}

}
