package com.tui.uk.client.domain;

import java.io.Serializable;

public class AirlineItenaryData implements Cloneable, Serializable {

	private String clearingCount;
	private String clearingSequence;

	private String issuingCarrierCode;
	private String passengerName;

	private String restrictedTicket;
	private String ticketNumber;

	private String customerCode;
	private String issueDate;

	private String totalFare;
	private String totalFee;

	private String total_taxes;
	private String travelAgencyCode;

	private String travelAgencyName;
	private PaypalFlightDetails flightDetails;

	public String getClearingCount() {
		return clearingCount;
	}
	public void setClearingCount(String clearingCount) {
		this.clearingCount = clearingCount;
	}
	public String getClearingSequence() {
		return clearingSequence;
	}
	public void setClearingSequence(String clearingSequence) {
		this.clearingSequence = clearingSequence;
	}
	public String getIssuingCarrierCode() {
		return issuingCarrierCode;
	}
	public void setIssuingCarrierCode(String issuingCarrierCode) {
		this.issuingCarrierCode = issuingCarrierCode;
	}
	public String getPassengerName() {
		return passengerName;
	}
	public void setPassengerName(String passengerName) {
		this.passengerName = passengerName;
	}
	public String getRestrictedTicket() {
		return restrictedTicket;
	}
	public void setRestrictedTicket(String restrictedTicket) {
		this.restrictedTicket = restrictedTicket;
	}
	public String getTicketNumber() {
		return ticketNumber;
	}
	public void setTicketNumber(String ticketNumber) {
		this.ticketNumber = ticketNumber;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public String getTotalFare() {
		return totalFare;
	}
	public void setTotalFare(String totalFare) {
		this.totalFare = totalFare;
	}
	public String getTotalFee() {
		return totalFee;
	}
	public void setTotalFee(String totalFee) {
		this.totalFee = totalFee;
	}
	public String getTotal_taxes() {
		return total_taxes;
	}
	public void setTotal_taxes(String total_taxes) {
		this.total_taxes = total_taxes;
	}
	public String getTravelAgencyCode() {
		return travelAgencyCode;
	}
	public void setTravelAgencyCode(String travelAgencyCode) {
		this.travelAgencyCode = travelAgencyCode;
	}
	public String getTravelAgencyName() {
		return travelAgencyName;
	}
	public void setTravelAgencyName(String travelAgencyName) {
		this.travelAgencyName = travelAgencyName;
	}
	public PaypalFlightDetails getFlightDetails() {
		return flightDetails;
	}
	public void setFlightDetails(PaypalFlightDetails flightDetails) {
		this.flightDetails = flightDetails;
	}

}
