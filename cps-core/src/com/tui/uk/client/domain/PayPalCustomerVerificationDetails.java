package com.tui.uk.client.domain;

import java.io.Serializable;

public class PayPalCustomerVerificationDetails implements Cloneable,
		Serializable {

	private static final long serialVersionUID = -8L;

	private String acknowledgement;
	private String addressId;
	private String addressStatus;
	private String allowedPaymentMethod;
	private String billingAgreementAccepted;

	private String build;
	private String businessName;
	private String checkoutStatus;
	private String correlationId;
	private String countryCode;

	private String custom;
	private String customerEmailId;
	private String financingFeeAmout;
	private String financingMonthlyPayment;
	private String financingTerm;

	private String financingTotalCost;
	private String cartChangeTolarance;
	private String currencyCode;
	private String isFinancing;
	private String firstName;

	private String handlingAmount;
	private String invnum;
	private String insuranceAmount;
	private String insuranceOptionSelected;
	private String itemAmount;

	private String lastName;
	private String middleName;
	private String noteText;
	private String notifyUrl;
	private String payerId;

	private String payerStatus;
	private String phoneNumber;
	private String salutation;
	private String paymentRequestId;
	private ShippingAddress shippingAddress;

	private String shippingDiscountAmount;
	private String shippingCost;
	private String shippingCalculationMode;
	private String suffix;
	private String timestamp;

	private String token;
	private String PayPalVersion;
	private String billingAgreementId;
	private String currencyExchangeRate;
	private String amount;

	private String orderTime;
	private String paymentStatus;
	private String pamentType;
	private String settledAmount;
	private String taxCharged;

	private String transactionId;
	private String transactionType;
	private String datacashReferance;
	private String status;
	private String reason;
	private String paymentType;

	private String merchantReference;
	private BillingAddress billingAddress;

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getAcknowledgement() {
		return acknowledgement;
	}

	public void setAcknowledgement(String acknowledgement) {
		this.acknowledgement = acknowledgement;
	}

	public String getAddressId() {
		return addressId;
	}

	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}

	public String getAddressStatus() {
		return addressStatus;
	}

	public void setAddressStatus(String addressStatus) {
		this.addressStatus = addressStatus;
	}

	public String getAllowedPaymentMethod() {
		return allowedPaymentMethod;
	}

	public void setAllowedPaymentMethod(String allowedPaymentMethod) {
		this.allowedPaymentMethod = allowedPaymentMethod;
	}

	public String getBillingAgreementAccepted() {
		return billingAgreementAccepted;
	}

	public void setBillingAgreementAccepted(String billingAgreementAccepted) {
		this.billingAgreementAccepted = billingAgreementAccepted;
	}

	public String getBuild() {
		return build;
	}

	public void setBuild(String build) {
		this.build = build;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getCheckoutStatus() {
		return checkoutStatus;
	}

	public void setCheckoutStatus(String checkoutStatus) {
		this.checkoutStatus = checkoutStatus;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public String getCustom() {
		return custom;
	}

	public void setCustom(String custom) {
		this.custom = custom;
	}

	public String getCustomerEmailId() {
		return customerEmailId;
	}

	public void setCustomerEmailId(String customerEmailId) {
		this.customerEmailId = customerEmailId;
	}

	public String getFinancingFeeAmout() {
		return financingFeeAmout;
	}

	public void setFinancingFeeAmout(String financingFeeAmout) {
		this.financingFeeAmout = financingFeeAmout;
	}

	public String getFinancingMonthlyPayment() {
		return financingMonthlyPayment;
	}

	public void setFinancingMonthlyPayment(String financingMonthlyPayment) {
		this.financingMonthlyPayment = financingMonthlyPayment;
	}

	public String getFinancingTerm() {
		return financingTerm;
	}

	public void setFinancingTerm(String financingTerm) {
		this.financingTerm = financingTerm;
	}

	public String getFinancingTotalCost() {
		return financingTotalCost;
	}

	public void setFinancingTotalCost(String financingTotalCost) {
		this.financingTotalCost = financingTotalCost;
	}

	public String getCartChangeTolarance() {
		return cartChangeTolarance;
	}

	public void setCartChangeTolarance(String cartChangeTolarance) {
		this.cartChangeTolarance = cartChangeTolarance;
	}

	public String getCurrencyCode() {
		return currencyCode;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public String getIsFinancing() {
		return isFinancing;
	}

	public void setIsFinancing(String isFinancing) {
		this.isFinancing = isFinancing;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getHandlingAmount() {
		return handlingAmount;
	}

	public void setHandlingAmount(String handlingAmount) {
		this.handlingAmount = handlingAmount;
	}

	public String getInvnum() {
		return invnum;
	}

	public void setInvnum(String invnum) {
		this.invnum = invnum;
	}

	public String getInsuranceAmount() {
		return insuranceAmount;
	}

	public void setInsuranceAmount(String insuranceAmount) {
		this.insuranceAmount = insuranceAmount;
	}

	public String getInsuranceOptionSelected() {
		return insuranceOptionSelected;
	}

	public void setInsuranceOptionSelected(String insuranceOptionSelected) {
		this.insuranceOptionSelected = insuranceOptionSelected;
	}

	public String getItemAmount() {
		return itemAmount;
	}

	public void setItemAmount(String itemAmount) {
		this.itemAmount = itemAmount;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}

	public String getNoteText() {
		return noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public String getPayerId() {
		return payerId;
	}

	public void setPayerId(String payerId) {
		this.payerId = payerId;
	}

	public String getPayerStatus() {
		return payerStatus;
	}

	public void setPayerStatus(String payerStatus) {
		this.payerStatus = payerStatus;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getSalutation() {
		return salutation;
	}

	public void setSalutation(String salutation) {
		this.salutation = salutation;
	}

	public String getPaymentRequestId() {
		return paymentRequestId;
	}

	public void setPaymentRequestId(String paymentRequestId) {
		this.paymentRequestId = paymentRequestId;
	}

	public ShippingAddress getShippingAddress() {
		return shippingAddress;
	}

	public void setShippingAddress(ShippingAddress shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	public String getShippingDiscountAmount() {
		return shippingDiscountAmount;
	}

	public void setShippingDiscountAmount(String shippingDiscountAmount) {
		this.shippingDiscountAmount = shippingDiscountAmount;
	}

	public String getShippingCost() {
		return shippingCost;
	}

	public void setShippingCost(String shippingCost) {
		this.shippingCost = shippingCost;
	}

	public String getShippingCalculationMode() {
		return shippingCalculationMode;
	}

	public void setShippingCalculationMode(String shippingCalculationMode) {
		this.shippingCalculationMode = shippingCalculationMode;
	}

	public String getSuffix() {
		return suffix;
	}

	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPayPalVersion() {
		return PayPalVersion;
	}

	public void setPayPalVersion(String payPalVersion) {
		PayPalVersion = payPalVersion;
	}

	public String getBillingAgreementId() {
		return billingAgreementId;
	}

	public void setBillingAgreementId(String billingAgreementId) {
		this.billingAgreementId = billingAgreementId;
	}

	public String getCurrencyExchangeRate() {
		return currencyExchangeRate;
	}

	public void setCurrencyExchangeRate(String currencyExchangeRate) {
		this.currencyExchangeRate = currencyExchangeRate;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getOrderTime() {
		return orderTime;
	}

	public void setOrderTime(String orderTime) {
		this.orderTime = orderTime;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getPamentType() {
		return pamentType;
	}

	public void setPamentType(String pamentType) {
		this.pamentType = pamentType;
	}

	public String getSettledAmount() {
		return settledAmount;
	}

	public void setSettledAmount(String settledAmount) {
		this.settledAmount = settledAmount;
	}

	public String getTaxCharged() {
		return taxCharged;
	}

	public void setTaxCharged(String taxCharged) {
		this.taxCharged = taxCharged;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}

	public String getDatacashReferance() {
		return datacashReferance;
	}

	public void setDatacashReferance(String datacashReferance) {
		this.datacashReferance = datacashReferance;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getMerchantReference() {
		return merchantReference;
	}

	public void setMerchantReference(String merchantReference) {
		this.merchantReference = merchantReference;
	}

	public BillingAddress getBillingAddress() {
		return billingAddress;
	}

	public void setBillingAddress(BillingAddress billingAddress) {
		this.billingAddress = billingAddress;
	}

}
