/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

/**
 * Represents all the possible known client application names.
 * <p>
 * WiSH AO- Represents Web In Shops Accommodation Only. WiSH BYO- Represents Web In Shops Build Your
 * Own. B2C AO - Represents Business To Customer Accommodation Only. B2C BYO - Represents Business
 * To Customer Build Your Own. Cruise - Represents Web In Shops Cruise. BRAC - Represents BRAC
 * application.
 * </p>
 * These client application names are used to create random merchant reference id. So limit the
 * length of client application name to a maximum of ten characters.
 *
 * @author thomas.pm
 *
 * @since 1.0 API
 *
 */
public enum ClientApplication
{
   /** Accommodation Only Component for Thomson. */
   ThomsonAccommodationOnly("ThomsonAO", BookingConstants.ECOMM),

   /** Flight and Hotel package for Thomson. */
   ThomsonBuildYourOwn("ThomsonBYO", BookingConstants.ECOMM),

   /** The CRUISE Application for Thomson. */
   ThomsonCruise("ThomsonCruise", BookingConstants.ECOMM),

   /** Accommodation Only Component for Web In Shops. */
   WISHAccommodationOnly("WiSHAO", BookingConstants.CNP),

   /** Flight and Hotel Component for Web In Shops. */
   WISHBuildYorOwn("WiSHBYO", BookingConstants.CNP),

   /** The BRAC Application. */
   BRAC("BRACApplication", BookingConstants.CNP),

   /** The CRUISE Application for Web In Shops. */
   WISHCRUISE("Cruise", BookingConstants.CNP),

   /** The OLBP application. */
   WISHOLBP("OLBP", BookingConstants.CNP),

   /** The generic java application. */
   GENERICJAVA("GENERICJAVA", BookingConstants.ECOMM),

   /** The generic .NET application. */
   GENERICDOTNET("GENERICDOTNET", BookingConstants.ECOMM),

   /** The hugo build your own. */
   HUGOBuildYourOwn("HUGOBYO", BookingConstants.ECOMM),

   /** The falcon build your own. */
   FALCONBuildYourOwn("FALCONBYO", BookingConstants.ECOMM),

   /** The portland application. */
   PORTLAND("Portland", BookingConstants.ECOMM),

   /** The TFly application. */
   TFLY("TFly", BookingConstants.ECOMM),

   /** The FCFO application. */
   FCFO("FCFO", BookingConstants.ECOMM),

   /** The NEWSKIES SYS application. */
   NEWSKIES("NEWSKIES", BookingConstants.ECOMM),

   /** The NEWSKIESM SYS application. */
   NEWSKIESM("NEWSKIESM", BookingConstants.ECOMM),

   /** The Kronos Call Center SYS application. */
   KRONOSCALLCENTER("KRONOSCALLCENTER", BookingConstants.CNP),

   /** The FCAO application. */
   FCAO("FCAO", BookingConstants.ECOMM),

   /** The GREENFIELDBeach application. */
   GREENFIELDBeach("GREENFIELDBeach", BookingConstants.ECOMM),

   /** The GREENFIELDSimply application. */
   GREENFIELDSimply("GREENFIELDSimply", BookingConstants.ECOMM),

   /** The GREENFIELDCruise application. */
   GREENFIELDCruise("GREENFIELDCruise", BookingConstants.ECOMM),

   /** The WSS application. */
   WSS("WSS", BookingConstants.ECOMM),

   /** The TRACS application. */
   TRACS("Tracs", BookingConstants.CNP),

   /** The B2CShorex application. */
   B2CShoreExcursions("B2CShorex", BookingConstants.ECOMM),

   /** The B2CShorex application. */
   B2BShoreExcursions("B2BShorex", BookingConstants.CNP),

   /** The redesigned payment page for Thomson BYO. */
   ThomsonFHPI("ThomsonFHPI", BookingConstants.ECOMM),

   /** The payment page for First Choice Web Excursion. */
   FCX("FCX", BookingConstants.ECOMM),

   /** The payment page for Thomson Web Excursion. */
   THX("THX", BookingConstants.ECOMM),

   /** The payment page for FirstChoice BookFlow. */
   FIRSTCHOICE("FIRSTCHOICE", BookingConstants.ECOMM),

   /** The payment page for Thomson BookFlow. */
   THOMSON("THOMSON", BookingConstants.ECOMM),

   /** The AtcomRes application. */
   AtcomRes("AtcomRes", BookingConstants.CNP),

   /** The payment page for FirstChoice Retrieve & Pay. */
   MANAGEFIRSTCHOICE("MANAGEFIRSTCHOICE", BookingConstants.ECOMM),

   /** The payment page for Thomson Retrieve & Pay. */
   MANAGETHOMSON("MANAGETHOMSON", BookingConstants.ECOMM),

   /** The HYBRISFALCON application. */
   HYBRISFALCON("HYBRISFALCON", BookingConstants.ECOMM),

   /** The HYBRISTHCRUISE application. */
   HYBRISTHCRUISE("HYBRISTHCRUISE", BookingConstants.ECOMM),

   /** The HYBRISTHFO application. */
   HYBRISTHFO("HYBRISTHFO", BookingConstants.ECOMM),

   /** The MFLIGHTONLY application. */
   MFLIGHTONLY("MFLIGHTONLY", BookingConstants.ECOMM),

   /** The THOMSONMOBILE application. */
   THOMSONMOBILE("THOMSONMOBILE", BookingConstants.ECOMM),

   /** The FIRSTCHOICEMOBILE application. */
   FIRSTCHOICEMOBILE("FIRSTCHOICEMOBILE", BookingConstants.ECOMM),

   /** The FALCONMOBILE application. */
   FALCONMOBILE("FALCONMOBILE", BookingConstants.ECOMM),

   /** The Amend And Cancel THOMSON application. */
   ANCTHOMSON("ANCTHOMSON", BookingConstants.ECOMM),

   /** The Amend And Cancel FIRSTCHOICE application. */
   ANCFIRSTCHOICE("ANCFIRSTCHOICE", BookingConstants.ECOMM),

   /** The Amend And Cancel FO application. */
   ANCTHFO("ANCTHFO", BookingConstants.ECOMM),

   /** The Amend And Cancel Cruise application. */
   ANCTHCRUISE("ANCTHCRUISE", BookingConstants.ECOMM),

   /** The Amend And Cancel Falcon application. */
   ANCFALCON("ANCFALCON", BookingConstants.ECOMM),

   /** The Amend And Cancel FalconFO application. */
   ANCFJFO("ANCFJFO", BookingConstants.ECOMM),

   /** The HYBRISFALCONFO application. */
   HYBRISFALCONFO("HYBRISFALCONFO", BookingConstants.ECOMM),

   /** The FALCONFOMOBILE application. */
   FALCONFOMOBILE("FALCONFOMOBILE", BookingConstants.ECOMM),

   /** The CRUISEMOBILE application. */
   CRUISEMOBILE("CRUISEMOBILE", BookingConstants.ECOMM),

   /** The TUITH application. */
   TUITH("TUITH", BookingConstants.ECOMM),

   /** The TUIFC application. */
   TUIFC("TUIFC", BookingConstants.ECOMM),

   /** The TUIFALCON application. */
   TUIFALCON("TUIFALCON", BookingConstants.ECOMM),

   /** The TUITHCRUISE application. */
   TUITHCRUISE("TUITHCRUISE", BookingConstants.ECOMM),

   /** The TUITHRIVERCRUISE application. */
   TUITHRIVERCRUISE("TUITHRIVERCRUISE", BookingConstants.ECOMM),

   /** The TUITHCRUISE application. */
   TUICRUISEDEALS("TUICRUISEDEALS", BookingConstants.ECOMM),

   /** The TUITHCRUISE application. */
   TUIHMCRUISE("TUIHMCRUISE", BookingConstants.ECOMM),

   /** The TUITHSHOREX application. */
   TUITHSHOREX("TUITHSHOREX", BookingConstants.ECOMM),
   
   /** The TUITHFO application. */
   TUITHFO("TUITHFO", BookingConstants.ECOMM),
   
   /** The TUIFALCONFO application. */
   TUIFALCONFO("TUIFALCONFO", BookingConstants.ECOMM),
   
   /** The TUICS application. Thomson crystal ski brand */
   TUICS("TUICS", BookingConstants.ECOMM),
   
   /** The TUIES application. Ireland crystal ski brand*/
   TUIES("TUIES", BookingConstants.ECOMM),
   
   /** The ANCSKICS application. Thomson crystal ski MMB brand */
   ANCSKICS ("ANCSKICS", BookingConstants.ECOMM),
   
   /** The ANCSKIES application. Ireland crystal ski brand */
   ANCSKIES ("ANCSKIES", BookingConstants.ECOMM);

   /** The client application name. */
   private String clientApplicationName;

   /** The capture method for datacash transaction. */
   private String captureMethod;

   /**
    * Construct the enumeration item.
    *
    * @param clientApplicationName the clientApplicationName.
    * @param captureMethod the captureMethod.
    */
   private ClientApplication(String clientApplicationName, String captureMethod)
   {
      this.clientApplicationName = clientApplicationName;
      this.captureMethod = captureMethod;
   }

   /**
    * Retrieve the client application name.
    *
    * @return the clientApplicationName
    */
   public String getClientApplicationName()
   {
      return clientApplicationName;
   }

   /**
    * Retrieve the capture method value.
    *
    * @return the captureMethod
    */
   public String getCaptureMethod()
   {
      return captureMethod;
   }

   /**
    * Determine the client application name.
    *
    * @param requiredClientName the client name of application.
    *
    * @return the clientApplicationName.
    *
    * @throws IllegalArgumentException if the code does not relate to a known package type.
    */
   public static ClientApplication findByCode(String requiredClientName)
   {
      for (ClientApplication clientApplicationName : values())
      {
         if (clientApplicationName.getClientApplicationName().equals(requiredClientName))
         {
            return clientApplicationName;
         }
      }
      throw new IllegalArgumentException("Unknown Client Application Name:" + requiredClientName);
   }
}
