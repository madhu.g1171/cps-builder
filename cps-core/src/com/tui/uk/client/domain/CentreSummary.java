package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * Centre Summary Holds Data of each Centre in MultiCentre.
 * @author anup.g
 *
 */
public final class CentreSummary implements Serializable
{
	/** The generated serial version id.*/
	private static final long serialVersionUID = 8458103928753754166L;
    /**
     * startDay.
     */
    private int startDay;

    /**
     * endDay.
     */
    private int endDay;

    /**
     * duration.
     */
    private int duration;

    /**
     * name.
     */
    private String name;

    public CentreSummary(){
    	
    }
	public CentreSummary(int startDay, int endDay, int duration, String name) {
		super();
		this.startDay = startDay;
		this.endDay = endDay;
		this.duration = duration;
		this.name = name;
	}

	/**
     * Returns StartDay of the Centre in MultiCentre.
     *
     * @return startDay.
     */
    public int getStartDay()
    {
        return startDay;
    }

    /**
     * Sets Start Day of the Centre in MultiCentre.
     *
     * @param startDay This is start day of the centre.
     */
    public void setStartDay(int startDay)
    {
        this.startDay = startDay;
    }

    /**
     * Returns End Day of the Centre in MultiCentre.
     *
     * @return endDay.
     */
    public int getEndDay()
    {
        return endDay;
    }

    /**
     * Sets End Day.
     *
     * @param endDay This is end day of the centre.
     */
    public void setEndDay(int endDay)
    {
        this.endDay = endDay;
    }

    /**
     * Returns Duration of the Centre in MultiCentre.
     * @return duration.
     */
    public int getDuration()
    {
        return duration;
    }

    /**
     * Sets Duration of the Centre in MultiCentre.
     *
     * @param duration This is centre duration.
     */
    public void setDuration(int duration)
    {
        this.duration = duration;
    }

    /**
     * Returns Centre Name.
     *
     * @return name.
     */
    public String getName()
    {
        return name;
    }

    /**
     * Sets Centre Name.
     *
     * @param name This is centre name.
     */
    public void setName(String name)
    {
        this.name = name;
    }

}
