/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
*/
package com.tui.uk.client.domain;

import java.io.Serializable;

/**
 * This class is responsible for holding all the payment details related to cards.
 *
 * @author sindhushree.g
 *
 * @since 1.0 API
 *
 */
public final class CardType implements Serializable
{

   /** The generated serial version id. */
   private static final long serialVersionUID = -4456216871849848533L;

   /** The maximum security code length. */
   private Integer securityCodeLength;

   /** The minimum security code length. */
   private Integer minSecurityCodeLength;

   /** Is issue number required. */
   private Boolean isIssueNumberRequired;

   /** Is start date required. */
   private Boolean isStartDateRequired;

   /**
    * Constructor for creating maximum security code length.
    *
    * @param securityCodeLength the maximum security code length.
    *
    */
   public CardType(Integer securityCodeLength)
   {
      this.securityCodeLength = securityCodeLength;
   }

   /**
    * Constructor for creating minimum and maximum security code length.
    *
    * @param securityCodeLength the maximum security code length.
    * @param minSecurityCodeLength the minimum security code length.
    *
    */
   public CardType(Integer securityCodeLength, Integer minSecurityCodeLength)
   {
      this.securityCodeLength = securityCodeLength;
      this.minSecurityCodeLength = minSecurityCodeLength;
   }

   /**
    * Gets the minimum security code length.
    *
    * @return the minSecurityCodeLength
    */
   public Integer getMinSecurityCodeLength()
   {
      if (null == minSecurityCodeLength)
      {
         this.minSecurityCodeLength = securityCodeLength;
      }
      return minSecurityCodeLength;
   }

   /**
    * Gets the security code length.
    *
    * @return the securityCodeLength
    */
   public Integer getSecurityCodeLength()
   {
      return securityCodeLength;
   }

   /**
    * Gets if issue number is required or not.
    *
    * @return true if issue number is required. Otherwise, false.
    */
   public Boolean getIsIssueNumberRequired()
   {
      return isIssueNumberRequired;
   }

   /**
    * Gets if start date is required.
    *
    * @return true if start date is required. Otherwise, false.
    *
    * @deprecated As start date is not displayed for any of the card types, this method
    * is not required.
    */
   public Boolean getIsStartDateRequired()
   {
      return isStartDateRequired;
   }

   /**
    * Sets isIssueNumberRequired field.
    *
    * @param isIssueNumberRequired the isIssueNumberRequired to set.
    */
   public void setIsIssueNumberRequired(Boolean isIssueNumberRequired)
   {
      this.isIssueNumberRequired = isIssueNumberRequired;
   }

   /**
    * Sets isStartDateRequired field.
    *
    * @param isStartDateRequired the isStartDateRequired to set.
    *
    * @deprecated As start date is not displayed for any of the card types, this method
    * is not required.
    */
   public void setIsStartDateRequired(Boolean isStartDateRequired)
   {
      this.isStartDateRequired = isStartDateRequired;
   }
}