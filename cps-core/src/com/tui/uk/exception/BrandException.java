/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BrandException.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2009-09-16 12:11:00$
 *
 * $author: kumarhe$
 *
 * $Log : $
 *
 */
package com.tui.uk.exception;

/**
 * 
 * Throws an exception whenever there is a problem in reading the 
 * brand configuration file/message entries or accessing a
 * particular data in the configuration file.
 * 
 * @author kumarhe
 *
 */
@SuppressWarnings("serial")
public class BrandException extends Exception

{

   /**
    * Default constructor for FilterException class.
    */
   public BrandException()
   {
      super();
   }

   /**
    * Constructor to create the exception by specifying a custom message.
    * 
    * @param msg The custom message associated with this exception.
    */
   public BrandException(String msg)
   {
      super(msg);
   }

   /**
    * Defines the constructor for FilterException with detailed message and specified cause.
    * 
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public BrandException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Defines the constructor for FilterException with specified cause.
    * 
    * @param cause - The specified cause.
    */
   public BrandException(Throwable cause)
   {
      super(cause);
   }

}
