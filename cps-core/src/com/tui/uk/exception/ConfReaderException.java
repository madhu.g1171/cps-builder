/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ConfReaderException.java,v $
 *
 * $Revision: 1.2 $
 *
 * $Date: 2008-05-08 09:56:57 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $
 */

package com.tui.uk.exception;

/**
 * This class is an Exception class, which is thrown whenever there is a
 * problem in reading the configuration file/message entries or accessing a
 * particular data in the configuration file.
 *
 * @author Author: sindhushree.g
 */
@SuppressWarnings("serial")
public class ConfReaderException extends Exception
{
   /**
    * Default constructor for ConfReaderException class.
    */
   public ConfReaderException()
   {
      super();
   }

   /**
    * Constructor to create the exception by specifying a custom message.
    *
    * @param msg The custom message associated with this exception.
    */
   public ConfReaderException(String msg)
   {
      super(msg);
   }

   /**
    * Defines the constructor for ConfReaderException with detailed
    * message and specified cause.
    *
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public ConfReaderException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Defines the constructor for ConfReaderException with specified
    * cause.
    *
    * @param cause - The specified cause.
    */
   public ConfReaderException(Throwable cause)
   {
      super(cause);
   }
}
