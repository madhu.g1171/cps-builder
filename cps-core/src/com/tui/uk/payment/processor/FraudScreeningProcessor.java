/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.processor;

import com.tui.uk.client.domain.Money;

import java.util.Map;

/**
 * This is an interface for contacting the fraud screening service and handling its response.
 *
 * @author sindhushree.g
 *
 */
public interface FraudScreeningProcessor
{
   /**
    * This method is responsible for calling fraud screening service and handling the response
    * appropriately.
    *
    * @return false in case of Reject response, true in all other cases.
    */
   boolean processFraudScreening();

   /**
    * This method is responsible for updating the card details when authorization fails.
    */
   void updateFailureCardData();

   /**
    * This method is responsible for updating booking related information to the fraud screening
    * application once booking is completed.
    *
    * @param bookingReference the booking reference.
    * @param toalAmount the total amount.
    */
   void updateBookingDetails(String bookingReference, Money toalAmount);

   boolean processFraudScreeningForZeroDepositeDD(Map directDebitDetails);

}
