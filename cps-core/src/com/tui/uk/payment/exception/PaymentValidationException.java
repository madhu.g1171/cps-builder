/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentValidationException.java$
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: thomas.pm $
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.exception;

/**
 * PaymentValidationException is thrown whenever any payment transaction validation fails in the
 * server side.The validation message should be associated with this exception,whenever it
 * is thrown.
 *
 * @author thomas.pm
 */
@SuppressWarnings("serial")
public final class PaymentValidationException extends Exception
{

   /** The field associated with this exception. */
   private String errorFields = "";

   /** The code associated with this exception. */
   private int code;

   /**
    * Constructs PaymentValidationException with the specified validation exception message.
    *
    * @param message the detail message.
    */
   public PaymentValidationException(String message)
   {
      super(message);
   }

   /**
    * Constructs PaymentValidationException with the message and the associated field name.
    *
    * @param message the detail message.
    * @param fieldName the associated field name.
    */
   public PaymentValidationException(String message, String fieldName)
   {
      super(message);
      this.errorFields = fieldName;
   }

   /**
    * Constructs PaymentValidationException with the message and the associated field name.
    *
    * @param code the exception code.
    * @param message the exception message.
    */
   public PaymentValidationException(int code, String message)
   {
      super(message);
      this.code = code;
   }
   /**
    * Constructs PaymentValidationException with the message and the associated field name.
    *
    * @param code the exception code.
    * @param message the exception message.
    * @param fieldName the associated field name.
    */
   public PaymentValidationException(int code, String message, String fieldName)
   {
      super(message);
      this.code = code;
      this.errorFields = fieldName;
   }

   /**
    * Defines the constructor for PaymentValidationException with detailed
    * message and specified cause.
    *
    * @param code the exception code.
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public PaymentValidationException(int code, String message, Throwable cause)
   {
      super(message, cause);
      this.code = code;
   }

   /**
    * Defines the constructor for PaymentValidationException with specified
    * cause.
    *
    * @param cause - The specified cause.
    */
   public PaymentValidationException(Throwable cause)
   {
      super(cause);
   }
   /**
    * Gets the associated field name.
    *
    * @return the associated field name.
    */
   public String getErrorFields()
   {
      return errorFields;
   }

   /**
    * Returns the code of the exception.
    *
    * @return the code associated with this exception.
    */
   public int getCode()
   {
      return code;
   }
}
