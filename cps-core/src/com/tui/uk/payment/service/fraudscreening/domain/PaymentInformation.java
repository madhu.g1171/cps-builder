/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $author : jaleelakbar.m@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.fraudscreening.domain;

import java.util.ArrayList;
import java.util.List;

import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;

import edu.emory.mathcs.backport.java.util.Collections;

/**
 * This Class Represents PaymentInformation.
 * @author Jaleel
 */

public class PaymentInformation
{
   /** The fraudScreeningCardData. */
   private List<FraudScreeningCardData> fraudScreeningCardDataList =
           new ArrayList<FraudScreeningCardData>();

   /** The promoId. */
   private String promoId;

   /** The promoAmount. */
   private String promoAmount;

   /** The comma separator. */
   private static final String COMMA_SEPARATOR = ",";

    /**
    * The constructor.
    *
    * @param fraudScreeningCardData the fraudScreeningCardData.
    * @param promoId the promoId.
    * @param promoAmount the promoAmount.
    *
    *
    */
   public PaymentInformation(List<FraudScreeningCardData> fraudScreeningCardData,
                             String promoId, String promoAmount)
   {
      this.fraudScreeningCardDataList = fraudScreeningCardData;
      this.promoId = promoId;
      this.promoAmount = promoAmount;

      /*As of now first failed card information and then
       * success card information is shown in request xml. To reverse the order
       * in xml collections.reverse is used.
       */
      Collections.reverse(this.fraudScreeningCardDataList);


   }

   /**
    * Method to populate all PaymentInformation.
    *
    * @param isLogging XML is for logging or not.
    *
    * @return the PaymentInformation.
    */
   public String toString(boolean isLogging)
   {
      StringBuilder stringBuilder = new StringBuilder();
      stringBuilder.append("<paymentInformation>");
      for (FraudScreeningCardData cardData : fraudScreeningCardDataList)
      {
          stringBuilder.append(cardData.toString(isLogging));
      }
      stringBuilder.append("<promoId>").append(promoId).append("</promoId>");
      stringBuilder.append("<promoAmount>").append(promoAmount).append("</promoAmount>");
      stringBuilder.append("</paymentInformation>");

      return stringBuilder.toString();
   }

   /**
    * Purges sensitive card information.
    */
   public void purgeSensitiveCardData()
   {
      for (FraudScreeningCardData cardData : fraudScreeningCardDataList)
      {
         cardData.purgeSensitiveCardData();
      }
   }

   /**
    * Gets the necessary logging information.
    *
    * @return the log message string.
    */
   public String getPaymentLoggingInformation()
   {
      StringBuilder logMessage = new StringBuilder();
      for (FraudScreeningCardData fraudScreeningCardData : this.fraudScreeningCardDataList)
      {
         AuthInfo authInfo = fraudScreeningCardData.getAuthInfo();
         if (authInfo.getPaymentGatewayResponseCode().equalsIgnoreCase("001")
                  && !fraudScreeningCardData.isFailedCard())
         {
            logMessage.append(authInfo.getMerchantReferenceNumber());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(authInfo.getPaymentGatewayReferenceNumber());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(fraudScreeningCardData.getMaskedCardNumber());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(authInfo.getCardScheme());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(fraudScreeningCardData.getCurrency());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(fraudScreeningCardData.getCardAmount());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(authInfo.getPaymentGatewayResponseCode());
            logMessage.append(COMMA_SEPARATOR);

            logMessage.append(authInfo.getResponseMsg());
            logMessage.append(COMMA_SEPARATOR);

            break;
         }
      }
      return logMessage.toString();
   }

}
