/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CardSecurity.java,v $
 *
 * $Revision: 1.4 $
 *
 * $Date: 2008-05-14 10:19:55 $
 *
 * $Author: thomas.pm $
 *
 * $Log: not supported by cvs2svn $ Revision 1.3 2008/05/08 10:45:09
 * sindhushree.g Modified to give appropriate path for the .key files.
 *
 * Revision 1.2 2008/05/07 14:19:34 thomas.pm Added Log History
 *
 *
 *
 */
package com.tui.uk.payment.service.security;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.codec.binary.Base64;

import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;

/**
 * This class is responsible for the encryption and decryption of credit card details.
 *
 * @author thomas.pm
 *
 */
public final class CardSecurity
{

   /** Constant for private key. */
   private static final int PRIVATE = 0;

   /** Constant for public key. */
   private static final int PUBLIC = 1;

   /** Constant for getting encryption algorithm. */
   private static final String ENCRYPTION_ALGORITHM =
      ConfReader.getConfEntry("encryption.Algorithm", null);

   /**
    * Private constructor to avoid instantiation.
    */
   private CardSecurity()
   {
      // Stops instantiation of this class.
   }

   /**
    * This method is used for encrypting card details.
    *
    * @param cardDetails the card details.
    * @return encryptedString the encrypted String
    */
   public static String encrypt(char[] cardDetails)
   {
      String path = ConfReader.getConfEntry("encryption.PublicKeyPath", null);
      if (path == null)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty("public.keypath.notfound",
            MESSAGES_PROPERTY));
         throw new RuntimeException(PropertyResource.getProperty("public.keypath.notfound",
            MESSAGES_PROPERTY));
      }
      PublicKey publicKey = (PublicKey) getKey(path, PUBLIC);
      Cipher cipher;
      try
      {
         cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
         cipher.init(Cipher.ENCRYPT_MODE, publicKey);
      }
      catch (InvalidKeyException ikex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(ikex.getMessage(),
            MESSAGES_PROPERTY), ikex);
         throw new RuntimeException(ikex);
      }
      catch (NoSuchAlgorithmException nsex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(nsex.getMessage(),
            MESSAGES_PROPERTY), nsex);
         throw new RuntimeException(nsex);
      }
      catch (NoSuchPaddingException nspex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(nspex.getMessage(),
            MESSAGES_PROPERTY), nspex);
         throw new RuntimeException(nspex);
      }
      byte[] encryptedBinary;
      try
      {
         String cardValues = new String(cardDetails);
         byte[] clearText = cardValues.getBytes();
         encryptedBinary = cipher.doFinal(clearText);
      }
      catch (IllegalBlockSizeException ibex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(ibex.getMessage(),
            MESSAGES_PROPERTY), ibex);
         throw new RuntimeException(ibex);
      }
      catch (BadPaddingException bpex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(bpex.getMessage(),
            MESSAGES_PROPERTY), bpex);
         throw new RuntimeException(bpex);
      }
      byte[] encryptedText = Base64.encodeBase64(encryptedBinary);
      return new String(encryptedText);
   }

   /**
    * This method is used for decrypting card details.
    *
    * @param encryptedString the encrypted string.
    * @return clearResult the Result.
    */
   public static char[] decrypt(String encryptedString)
   {
      String path = ConfReader.getConfEntry("encryption.PrivateKeyPath", null);
      if (path == null)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty("private.keypath.notfound",
            MESSAGES_PROPERTY));
         throw new RuntimeException(PropertyResource.getProperty("private.keypath.notfound",
            MESSAGES_PROPERTY));
      }
      PrivateKey privateKey = (PrivateKey) getKey(path, PRIVATE);
      Cipher cipher;
      char[] clearResult;
      try
      {
         cipher = Cipher.getInstance(ENCRYPTION_ALGORITHM);
         cipher.init(Cipher.DECRYPT_MODE, privateKey);
      }
      catch (InvalidKeyException ikex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(ikex.getMessage(),
            MESSAGES_PROPERTY), ikex);
         throw new RuntimeException(ikex);
      }
      catch (NoSuchAlgorithmException nsaex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(nsaex.getMessage(),
            MESSAGES_PROPERTY), nsaex);
         throw new RuntimeException(nsaex);
      }
      catch (NoSuchPaddingException nspex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(nspex.getMessage(),
            MESSAGES_PROPERTY), nspex);
         throw new RuntimeException(nspex);
      }
      byte[] encryptedText = encryptedString.getBytes();
      byte[] encryptedBinary = Base64.decodeBase64(encryptedText);

      try
      {
         String clearValue = new String(cipher.doFinal(encryptedBinary));
         clearResult = clearValue.toCharArray();
      }
      catch (IllegalBlockSizeException ibex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(ibex.getMessage(),
            MESSAGES_PROPERTY), ibex);
         throw new RuntimeException(ibex);
      }
      catch (BadPaddingException bpex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(bpex.getMessage(),
            MESSAGES_PROPERTY), bpex);
         throw new RuntimeException(bpex);
      }
      return clearResult;
   }

   /**
    * This method is used for getting the key.
    *
    * @param filename the file name.
    * @param type the type.
    * @return key the Key.
    */
   private static Key getKey(String filename, int type)
   {
      File file = new File(filename);
      FileInputStream fis = null;

      try
      {
         fis = new FileInputStream(file);
      }
      catch (FileNotFoundException fnfex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(fnfex.getMessage(),
            MESSAGES_PROPERTY), fnfex);
         throw new RuntimeException(fnfex);
      }

      int size = (int) file.length();
      byte[] keydata = new byte[size];

      try
      {
         fis.read(keydata);
      }
      catch (IOException ioex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(ioex.getMessage(),
            MESSAGES_PROPERTY), ioex);
         throw new RuntimeException(ioex);
      }
      finally
      {
         if (fis != null)
         {
            try
            {
               fis.close();
            }
            catch (IOException ioe)
            {
               LogWriter.logErrorMessage(PropertyResource.getProperty(ioe.getMessage(),
                  MESSAGES_PROPERTY), ioe);
               throw new RuntimeException(ioe);
            }
         }
      }

      Key key = null;
      try
      {
         KeyFactory keyFactory = KeyFactory.getInstance(ENCRYPTION_ALGORITHM);
         switch (type)
         {
            case PRIVATE:
               PKCS8EncodedKeySpec encodedPrivateKey = new PKCS8EncodedKeySpec(keydata);
               key = keyFactory.generatePrivate(encodedPrivateKey);
               break;
            case PUBLIC:
               X509EncodedKeySpec encodedPublicKey = new X509EncodedKeySpec(keydata);
               key = keyFactory.generatePublic(encodedPublicKey);
               break;
            default:
         }
      }
      catch (NoSuchAlgorithmException nsaex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(nsaex.getMessage(),
            MESSAGES_PROPERTY), nsaex);
         throw new RuntimeException(nsaex);
      }
      catch (InvalidKeySpecException iksex)
      {
         LogWriter.logErrorMessage(PropertyResource.getProperty(iksex.getMessage(),
            MESSAGES_PROPERTY), iksex);
         throw new RuntimeException(iksex);
      }
      return key;
   }
}
