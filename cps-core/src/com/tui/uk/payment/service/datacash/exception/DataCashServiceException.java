/*
 * Copyright (C)2008 TUI UK Ltd
 * 
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 * 
 * Telephone - (024)76282828
 * 
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 * 
 * $RCSfile: DataCashServiceException.java,v $
 * 
 * $Revision: 1.4 $
 * 
 * $Date: 2008-05-08 07:26:36 $
 * 
 * $Author: sindhushree.g $
 * 
 * $Log: not supported by cvs2svn $ Revision 1.3 2008/05/07 14:24:50
 * thomas.pm Added Log History
 */
package com.tui.uk.payment.service.datacash.exception;

/**
 * This is Data Cash Service layer checked exception. This is mainly thrown
 * in case of data-cash status code 7 (failure) and in case of any bin
 * validation failure.
 * 
 * @author thomas.pm
 */
@SuppressWarnings("serial")
public final class DataCashServiceException extends Exception
{
   /** The code associated with this exception. */
   private int code;

   /**
    * Default constructor for DataCashServiceException class.
    */
   public DataCashServiceException()
   {
      super();
   }

   /**
    * Constructs with exception message.
    * 
    * @param message exception message.
    */
   public DataCashServiceException(String message)
   {
      super(message);
   }

   /**
    * Constructor to create this exception with a code and a message.
    * 
    * @param code the exception code.
    * @param message the exception message.
    */
   public DataCashServiceException(int code, String message)
   {
      super(message);
      this.code = code;
   }

   /**
    * Returns the code of the exception.
    * 
    * @return the code associated with this exception.
    */
   public int getCode()
   {
      return code;
   }

   /**
    * Defines the constructor for DataCashServiceException with detailed
    * message and specified cause.
    * 
    * @param message - The detailed exception message
    * @param cause - The specified cause for the exception
    */
   public DataCashServiceException(String message, Throwable cause)
   {
      super(message, cause);
   }

   /**
    * Defines the constructor for DataCashServiceException with specified
    * cause.
    * 
    * @param cause - The specified cause.
    */
   public DataCashServiceException(Throwable cause)
   {
      super(cause);
   }
}
