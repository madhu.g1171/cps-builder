package com.tui.uk.payment.service.datacash;

import com.datacash.util.XMLDocument;

/**
 * The query transaction response xml encapsulation.
 * 
 * @author ganapna
 * 
 */
public class QueryTransactionResponse extends Response
{

   /** ECI indicator code from Datacash returned in Query Transaction response. */
   private String eci;

   /**
    * The QueryTransactionResponse Constructor.
    * 
    * @param xmlDocument the XML Document.
    */
   public QueryTransactionResponse(XMLDocument xmlDocument)
   {
      super(xmlDocument);
      eci = xmlDocument.get(DataCashServiceConstants.ECI);
   }

   /**
    * Returns ECI from Query Transaction Response xml.
    * 
    * @return the ECI Indicator.
    */
   public String getEci()
   {
      return eci;
   }

}
