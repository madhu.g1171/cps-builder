/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AddressFinderConnectionImpl.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-06-23 08:08:25$
 *
 * $Author: bibin.j@sonata-software.com$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.io.IOException;
import java.util.Vector;

import capscan.client.McConnection;

import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.AddressFinderServiceException;

/**
  * This class performs the following action,
  * 1. gets the connection from connection factory for Capscan Address Finder Service
  * 2. searches Capscan service for the address details for given criteria
  * (houseNo/postCode) using Match Code Client API
  *
  * @author bibin.j@sonata-software.com
  */

public final class AddressFinderConnectionImpl implements AddressFinderConnection
{
   /**The constant for default connection time out. */
   private static final int DEFAULT_TIMEOUT = 15;

   /** The constant for capscan host ip. */
   private static final String CAPSCAN_HOST_IP = "capscan.HostIP";

   /** The constant for capscan connection pool. */
   private static final String CAPSCAN_CONNECTION_POOL = "capscan.ConnectionPool";

   /** The constant for capscan log. */
   private static final String CAPSCAN_LOG = "capscan.log";

   /** The constant for capscan connection timeout. */
   private static final String CAPSCAN_CONNECTION_TIMEOUT = "capscan.ConnectionTimeOut";

   /** The fields required from capscan server. */
   private Vector<String> select;

   /** The query input field names. */
   private Vector<String> where;

   /** The query input field values. */
   private Vector<String> data;

   /** The array of results returned by the server. */
   private String[] results;

   /** The math code connection object. */
   private McConnection mcConnection;

   /**
    * Gets a suitable instance of AddressFinderConnection.
    * AddressFinderServiceException is thrown when there is a failure in getting connection.
    *
    * @throws AddressFinderServiceException when connection with matchcode server fails.
    */
   public AddressFinderConnectionImpl() throws AddressFinderServiceException
   {
     getConnection();
   }

   /**
    * The method to get the results fields.
    *
    * @return select the vector.
    */
   public Vector<String> getSelect()
   {
      return select;
   }

   /**
    * The method to get the query input fields.
    *
    * @return where the vector.
    */
   public Vector<String> getWhere()
   {
      return where;
   }

   /**
    * The method to get query input field values.
    *
    * @return data the vector.
    */
   public Vector<String> getData()
   {
      return data;
   }

   /**
    * The method gets query results.
    *
    * @return results the array of results.
    */
   public String[] getResults()
   {
      return results.clone();
   }

   /**
    * Get the match code connection object.
    *
    * @return the mcConnection.
    */
   public McConnection getMcConnection()
   {
      return mcConnection;
   }

   /**
    * This method gets the connection for Capscan address finder service.
    * While fetching the connection, AddressFinderServiceException is thrown when there is a
    * failure in communication.
    *
    * @return AddressFinderConnection.
    * @throws AddressFinderServiceException when connection with matchcode server fails.
    */
   private AddressFinderConnection getConnection() throws AddressFinderServiceException
   {
      try
      {
         this.mcConnection = new McConnection(ConfReader.getConfEntry(CAPSCAN_HOST_IP, null),
            ConfReader.getConfEntry(CAPSCAN_CONNECTION_POOL, null), CAPSCAN_LOG,
               McConnection.CONNECTIONLESS);
         this.select = new Vector<String>();
         this.where = new Vector<String>();
         this.data = new Vector<String>();
      }
      catch (IOException ioe)
      {
         String message = PropertyResource.getProperty("capscan.communication.failed",
            MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(message + ioe.getMessage(), ioe);
         throw new AddressFinderServiceException(message, ioe);
      }
      return this;
   }
   /**
    * This method search the address details for given criteria(houseNo/postCode).
    *
    * @param houseNo the house number.
    * @param postCode the post code.
    * @return Address the address.
    * @throws AddressFinderServiceException when connection with matchcode server fails.
    */
   public Address search(String houseNo, String postCode) throws AddressFinderServiceException
   {
      select.add("SUBBUILDING(CA=2)");
      select.add("BUILDINGNUMBER(CA=2)");
      select.add("STREET(CA=2)");
      select.add("LOCALITY(CA=2)");
      select.add("POSTTOWN(CA=2)");
      select.add("COUNTY(FC)");
      select.add("POSTCODE(PP=16)");
      select.add("LISTCOUNT");
      select.add("AMBIGLIST(DE=':',LD='|')");
      where.add("ADDR");
      data.add(houseNo + "," + postCode);

      int connectionTimeOut = ConfReader.getIntEntry(CAPSCAN_CONNECTION_TIMEOUT, DEFAULT_TIMEOUT);
      results = mcConnection.search(
                             McConnection.SEARCH, select, where, data, connectionTimeOut);
      try
      {
         mcConnection.disconnect();
      }
      catch (IOException ioe)
      {
         LogWriter.logErrorMessage(ioe.getMessage(), ioe);
      }
      if (results != null && mcConnection.errno() == McConnection.OK)
      {
         return new Address(results);
      }
      else
      {
         String message = PropertyResource.getProperty("capscan.results.error", MESSAGES_PROPERTY);
         LogWriter.logErrorMessage(message);
         throw new AddressFinderServiceException(mcConnection.errno() + message);
      }
   }
}
