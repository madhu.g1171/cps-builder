/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AddressFinderDummyConnectionImpl.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-06-23 08:08:25$
 *
 * $author: bibin.j@sonata-software.com$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import com.tui.uk.payment.exception.AddressFinderServiceException;

/**
 * This class is used for testing purpose.
 * This class performs the following action:
 *   Searches Capscan service for the address details for given criteria (houseNo/postCode) using
 *   Match Code Client API
 *
 * @author bibin.j
 */
public final class AddressFinderDummyConnectionImpl implements AddressFinderConnection
{

   /**The constant for results. */
   private static final String[] RESUTLS =  {"", "", "" , "WIGMORE LANE", "" ,
      "LUTON, BEDS", "LU2 9TA", "2", "Without Building Name:|WIGMORE SHOPPING CENTRE:"};

   /**
    * The method to search the address details for given criteria(houseNo/postCode).
    * For testing purpose change the entry in cps.conf as
    * cps.capscan.Connection =
    *   com.tui.uk.payment.service.capscan.addressfinder.AddressFinderDummyConnectionImpl.
    *
    * @param houseNo the house number.
    * @param postCode the post code.
    * @return Address the address.
    * @throws AddressFinderServiceException when connection with matchcode server fails.
    */
   public Address search(String houseNo, String postCode) throws AddressFinderServiceException
   {
      if (houseNo != null)
      {
         return new Address(RESUTLS);
      }
      else
      {

         throw new AddressFinderServiceException(postCode);
      }

   }
}
