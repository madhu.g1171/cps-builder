/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AddressFinderService.java,v $
 *
 * $Revision: 1.0$
 *
 * $Date: 2008-06-23 08:08:25$
 *
 * $author: bibin.j$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.service.capscan.addressfinder;

import com.tui.uk.payment.exception.AddressFinderServiceException;
/**
 * This class performs the address search for a given criteria.
 *
 * @author bibin.j@sonata-software.com
 */
public final class AddressFinderService
{
/**
 * This method process the criteria(houseNo/postCode) for getting the address details.
 * AddressFinderServiceException is thrown when there is a failure in getting connection or when
 * there is failure in fetching the results.
 *
 * @param houseNo the house number.
 * @param postCode the post code.
 * @return Address the address.
 * @throws AddressFinderServiceException the address finder service exception.
 */
  public Address process(String houseNo, String postCode) throws AddressFinderServiceException
  {
     AddressFinderConnection connection = AddressFinderConnectionFactory.getConnection();
     return connection.search(houseNo, postCode);
  }

}

