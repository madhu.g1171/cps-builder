/*
* Copyright (C)2008 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   NoPaymentTransaction.java$
*
* $Revision:   $
*
* $Date:   Oct 30, 2008$
*
* Author: vijayalakshmi.d
*
*
* $Log:   $
*/
package com.tui.uk.payment.domain;

import java.math.BigDecimal;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.exception.PaymentValidationException;


/**
 * This Class Represents NoPayment Transaction with ZeroTransactionamount.
 */
public final class NoPaymentTransaction implements PaymentTransaction
{
   /** The payment type. */
   private String paymentTypeCode;

   /** The transaction amount. */
   private Money transactionAmount;
   /**
    * The Constructor.
    * @param transactionAmount the transaction amount
    * @param paymentTypeCode the payment type
    */
   public NoPaymentTransaction(String paymentTypeCode, Money transactionAmount)
   {
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount =  new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
   }

   /**
    * Gets the payment type.
    *
    * @return the payment type
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }


   /**
    * Gets the PaymentMethod associated with this Transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.NO_PAYMENT;
   }


   /**
    * Dummy method for validation.
    *
    * @throws PaymentValidationException Since, this is a dummy implementation this exception will
    * not be thrown for Nopayment transactions.
    */
   public void validate() throws PaymentValidationException
   {
      // Empty Implementation.
      // No validation required for NoPayment transaction.
   }

   /**
    * Clears sensitive data present in the transaction.
    */
   public void clearSensitiveData()
   {
      // Do nothing, empty implementation.
   }

   /**
    * Gets essential transaction data associated with NoPaymentTransaction.
    *
    * @return essentialTransactionData the essential transaction data
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      return new EssentialTransactionData(paymentTypeCode,
         getTransactionAmount() , getTransactionCharge(), getPaymentMethod());
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the ZERO transaction charge for Nopayment transaction.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
   }
   /**
    * This method sets the trackingData.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
       //TODO:need to implement this.
   }
}
