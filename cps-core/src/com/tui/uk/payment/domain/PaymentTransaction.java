/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentTransaction.java,v $
 *
 * $Revision:  $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S.j$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This interface defines the basic behavior for transactions.
 */
public interface PaymentTransaction
{

    /**
     * Gets the payment type.
     *
     * @return the payment type
     */
     String getPaymentTypeCode();


     /**
      * Gets the transaction amount.
      *
      * @return the transaction amount
      */
     Money getTransactionAmount();


     /**
      * Gets the transaction charge.
      *
      * @return the transaction charge
      */
     Money getTransactionCharge();

     /**
      * Returns PaymentMethod associated with PaymentTransaction.
      * Eg., DATACASH, PDQ, CASH etc.,
      *
      * @return the PaymentMethod
      */
     PaymentMethod getPaymentMethod();

     /**
      * Validates the payment transaction.
      *
      * @throws PaymentValidationException the payment validation exception.
      */
      void validate() throws PaymentValidationException;

      /**
       * Clears sensitive data present in the transaction.
       */
      void clearSensitiveData();

      /**
       * Returns the essential transaction data associated with PaymentTransaction.
       *
       * @return essential transaction data.
       */
      EssentialTransactionData getEssentialTransactionData();

      /**
       * This method sets the trackingData.
       * Tracking data contains token,sessionId and inventory booking reference number.
       *
       * @param trackingdata the trackingData.
       */
      void setTrackingData(String trackingdata);
}
