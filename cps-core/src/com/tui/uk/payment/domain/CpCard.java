/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: CpCard.java,v $
 *
 * $Revision:  $
 *
 * $Date:  $
 *
 * $author : thomas.pm@sonata-software.com $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;


import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;

import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class represents the Card domain object. Represents debit/credit card that is used for
 * payment. Field names in this class should match those used within the DataCash developers guide.
 *
 * @author thomas.pm@sonata-software.com
 */
public final class CpCard implements Card
{

   /** Pattern for card number. */
   private static final Pattern CARDNUMBER_PATTERN = Pattern.compile("[0-9]{13,19}+");

   /** Card number.A String of 13 to 19 digits. */
   private char[] pan;

   /** Holds the start index for masking the card number. */
   private static final int MASK_START_INDEX = 6;

   /** Holds the end index for masking the card number. */
   private static final int MASK_END_INDEX = 4;

   /**
    * The Constructor.
    *
    * @param pan the pan
    */
   public CpCard(char[] pan)
   {
      char[] panCopy = pan;
      this.pan = panCopy;
   }

   /**
    * Gets the pan.
    *
    * @return the pan
    */
   public char[] getPan()
   {
      char[] panCopy = pan;
      return panCopy;
   }

   /**
    * Gets the expire date.
    *
    * @return the expiryDate
    */
   public String getExpiryDate()
   {
      return null;
   }

   /**
    * Gets the card type.
    *
    * @return the card type
    */
   public String getCardtype()
   {
      return null;
   }

   /**
    * Gets the name on card.
    *
    * @return the nameOncard
    */
   public String getNameOncard()
   {
      return null;
   }

   /**
    * Gets the cv2.
    *
    * @return the cv2
    */
   public char[] getCv2()
   {
      return null;
   }

   /**
    * This method is used to validate card.
    *
    * @throws PaymentValidationException when validation fails.
    */
   public void validate() throws PaymentValidationException
   {
      if (StringUtils.isBlank(String.valueOf(pan)) ||  (!validateCreditCardNumber()))
      {
         populateException("payment.pan.invalid", "CardNumber",
            PaymentConstants.ERRORCODE_INVALIDCARDNO);
      }
   }

   /**
    * Method to validate Credit Card Number.
    *
    * @return boolean if it does not matches the pattern defined.
    */
   private boolean validateCreditCardNumber()
   {
      return CARDNUMBER_PATTERN.matcher(String.valueOf(pan)).matches();
   }

   /**
    * This method overwrites sensitive data.
    */
   public void purgeCardDetails()
   {
      Arrays.fill(pan, '*');
      pan = null;
   }

   /**
    * Gets masked card number.
    *
    * @return the masked card number.
    */
   public String getMaskedCardNumber()
   {
      StringBuilder maskedCardNumber =
         new StringBuilder(String.valueOf(pan));
      for (int i = MASK_START_INDEX; i < pan.length - MASK_END_INDEX; i++)
      {
         maskedCardNumber.setCharAt(i, '*');
      }
      return maskedCardNumber.toString();
   }

   /**
    * Method to populate exception.
    *
    * @param message the message code.
    * @param fieldName the associated field name.
    * @param errorCode the error code.
    *
    * @throws PaymentValidationException the Exception.
    */
   private void populateException(String message, String fieldName, int errorCode)
      throws PaymentValidationException
   {
      String errorMessage = PropertyResource.getProperty(message, MESSAGES_PROPERTY);
      LogWriter.logErrorMessage(errorMessage);
      throw new PaymentValidationException(errorCode, message, fieldName);
   }
}