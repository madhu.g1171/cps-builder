/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PostPaymentGuaranteeTransaction.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-06-17 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.EncryptedCard;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashCard;
import com.tui.uk.payment.service.security.CardSecurity;

/**
 * This class represents the PostPaymentGuaranteeTransaction. This transaction is used for
 * Hopla Post Pay accommodations. This class implements all the necessary behaviors required
 * for post payment guarantee transactions.
 *
 * @author Vinodha.S
 */
public final class PostPaymentGuaranteeTransaction implements PaymentTransaction
{
   /** The card. */
   private CnpCard card;

   /** The payment type code. */
   private String paymentTypeCode;

   /** The transaction amount. */
   private Money transactionAmount;

   /**
    * The Constructor.
    *
    * @param card the card
    * @param paymentTypeCode the payment type code
    * @param transactionAmount the transaction amount
    */
   public PostPaymentGuaranteeTransaction(CnpCard card, String paymentTypeCode,
            Money transactionAmount)
   {
      this.card = card;
      this.paymentTypeCode = paymentTypeCode;
      this.transactionAmount = transactionAmount;
   }

   /**
    * Gets the card.
    *
    * @return the card
    */
   public CnpCard getCard()
   {
      return card;
   }

   /**
    * Gets the payment type code.
    *
    * @return the payment type code
    */
   public String getPaymentTypeCode()
   {
      return paymentTypeCode;
   }

   /**
    * Gets the transaction amount.
    *
    * @return the transaction amount
    */
   public Money getTransactionAmount()
   {
      return transactionAmount;
   }

   /**
    * Gets the transaction charge.
    *
    * @return the transaction charge
    */
   public Money getTransactionCharge()
   {
      return new Money(BigDecimal.ZERO, transactionAmount.getCurrency());
   }

   /**
    * Gets the PaymentMethod associated with this transaction.
    *
    * @return the PaymentMethod
    */
   public PaymentMethod getPaymentMethod()
   {
      return PaymentMethod.POST_PAYMENT;
   }

   /**
    * This method is responsible for card validation using pattern matching and bin validation.
    *
    * @throws PaymentValidationException the PaymentValidationException
    */
   public void validate() throws PaymentValidationException
   {
      card.validate();
      //Since HOPLA also requires bin validation,
      //we are making use of DataCashCard class bin validation service.
      Map<String, String> address = new HashMap<String, String>();
      Map<String, String> cvvPolicies = new HashMap<String, String>();
      Map<String, String> addressPolicies = new HashMap<String, String>();
      Map<String, String> postCodePolicies = new HashMap<String, String>();

      DataCashCard datacashCard = new DataCashCard(card.getPan(), card.getNameOncard(),
                                     card.getExpiryDate(), card.getCv2(), card.getPostCode(),
                                        card.getCardtype(), card.getIssueNumber(), address,
                                        cvvPolicies, addressPolicies, postCodePolicies);
      datacashCard.setStartDate(card.getStartDate());
      datacashCard.validate();
   }

   /**
    * Clears sensitive data.This method is called just prior to dereferencing the object.
    * This intention is that no sensitive data resides in memory after the object has been
    * de-referenced, before it has been garbage collected. All sensitive data should be held in
    * character arrays--not Strings
    */
   public void clearSensitiveData()
   {
      card.purgeCardDetails();
   }

   /**
    * This method creates an <code>EncryptedCard</code> object by retrieving the encrypted pan
    * from <code>CardSecurity</code> class.
    *
    * @return The <code>EncryptedCard</code> object.
    */
   public EncryptedCard getEncryptedCard()
   {
      String encryptedPan;
      if (StringUtils.isBlank(ConfReader.getConfEntry("encryptedcard.value", null)))
      {
         return null;
      }
      else if (ConfReader.getBooleanEntry("encryptedcard.value", false))
      {
         encryptedPan = CardSecurity.encrypt(card.getPan());
      }
      else
      {
        encryptedPan = String.valueOf(card.getPan());
      }
      return new EncryptedCard(encryptedPan, card.getNameOncard(), card.getExpiryDate(),
               card.getPostCode(), card.getCardtype());

   }

   /**
    * Gets essential transaction data associated with post payment transaction.
    *
    * @return essentialTransactionData the essential transaction data
    */
   public EssentialTransactionData getEssentialTransactionData()
   {
      return new EssentialTransactionData(paymentTypeCode, transactionAmount,
               getTransactionCharge(), getPaymentMethod());
   }

   /**
    * This method sets the trackingData.
    * Tracking data contains token,sessionId and inventory booking reference number.
    *
    * @param trackingdata the trackingData.
    */
   public void setTrackingData(String trackingdata)
   {
       //TODO:need to implement this.
   }

}
