/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: PaymentServlet.java,v $
 *
 * $Revision: 1.5 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: veena.bn $
 *
 *
 * $Log: not supported by cvs2svn $ Revision 1.4 2008/05/14 09:57:21 veena.bn added validation
 * methods.
 */
package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.DEFAULT_FAILURE_COUNT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.MAX_FAILURE_COUNT;
import static com.tui.uk.payment.domain.PaymentConstants.HYPHEN;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH;
import static com.tui.uk.payment.domain.PaymentConstants.MAX_REFERENCE_LENGTH_ACSS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.CLIENT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DATACASH_REFERENCE_RS;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_1;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_2;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_3;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_4;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_5;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_6;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_7;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_8;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.DYN_DATA_9;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.EXPIRY_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_DATACASH_RS_KEY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_SESSION_KEY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_TRANSACTION_TYPE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HCC_URL_KEY;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HOST_NAME;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.HPS_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.MERCHANT_REFERENCE;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PAGE_SET_ID;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PASSWORD;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.RETURN_URL;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SESSION_ID;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.SETUP;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Currency;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.*;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;

import com.datacash.client.Agent;
import com.datacash.client.Amount;
import com.datacash.errors.FailureReport;
import com.datacash.logging.Logger;
import com.datacash.util.XMLDocument;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.cacheeventlistener.TransactionStatusUtil;
import com.tui.uk.cacheeventlistener.TransactionStatus;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

/**
 * This class is responsible for rendering the payment page. This class gets the token from the
 * request parameter and then gets the corresponding <code>BookingComponent</code> object from
 * <code>PaymentStore</code>. From the booking component, the application for which payment page has
 * to be rendered is identified and forwarded to appropriate page.
 *
 * @author sindhushree.g
 *
 */
@SuppressWarnings("serial")
public final class PaymentServlet extends CommonPaymentServlet
{

   /** The constant for data-cash error. */
   private static final String DATACASH_ERROR = "datacash.generic.error";

   /** The constant for next line. */
   private static final String NEXT_LINE = "\n";

   /** The constant for DD_DEPOSIT_TYPE. */
   public static final String DD_DEPOSIT_TYPE = "lowDepositPlusDD";
   
   public static final String CHECK_IN_PAYMENT= "isCheckinPayment";

   /**
    * This method will render the payment page for each client application.
    *
    * @param request a <code>HttpServletRequest</code> object that contains the request the client
    *           has made of the servlet.
    * @param response a <code>HttpServletResponse</code> object that contains the response the
    *           servlet sends to the client.
    *
    * @throws IOException if an input or output error is detected when the servlet handles the GET
    *            request.
    * @throws ServletException if the request for the GET could not be handled.
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      // The following property is being set for reading in log4j.properties file.
      System.setProperty("serverName", request.getServerName());
      String token = request.getParameter(DispatcherConstants.TOKEN);

      UUID uuid = UUID.fromString(token);
      PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
      BookingInfo bookingInfo = paymentData.getBookingInfo();
      boolean newSetupReq = bookingInfo.getNewSetupReq();
      boolean paymentFailureRedirect = false;
      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      String sessionId = request.getSession().getId();
      bookingComponent.setBookingSessionIdentifier(sessionId);
      bookingComponent.setSessionTimeOut(request.getSession().getMaxInactiveInterval());
      String sessionAlertTime = ConfReader.getConfEntry("payment.SessionAlertTimeOut", null);
      bookingComponent.setSessionTimeAlert(Integer.parseInt(sessionAlertTime));
      String isCheckinPayment = bookingComponent.getNonPaymentData().get(CHECK_IN_PAYMENT);
      LogWriter.logInfoMessage("******** isCheckinPayment = "+isCheckinPayment);
      String customIpHeader = request.getHeader("X-Forwarded-For");
      String ipAddress = StringUtils.substringBefore(customIpHeader, ",");
      updateSessionId(bookingInfo, token, sessionId, ipAddress);
  	  addSameSiteForSessionCookie(request, response);
      request.setAttribute("bookingInfo", bookingInfo);   
      if (bookingComponent.getNonPaymentData().get("csrfToken") != null)
      {

         request.getSession().setAttribute("csrfTokenVal",
            bookingComponent.getNonPaymentData().get("csrfToken"));

      }
      LogWriter.logInfoMessage("PayPal component is :-------------------------- "
         + bookingComponent.getPaypalComponent());
      if (bookingComponent.getPaypalComponent() != null)
      {
         LogWriter.logInfoMessage("Enabling PayPal option for this request is : "
            + bookingComponent.getPaypalComponent().getEnablePaypal());
         LogWriter.logInfoMessage("Activating PayPal option for this request is : "
            + bookingComponent.getPaypalComponent().isPaypalOptionActive());
      }
      // If price changes due to browser back and forward buttons
      String priceChangeUrl = priceChangeUrl(bookingComponent);
      if (StringUtils.isNotBlank(priceChangeUrl))
      {
         response.sendRedirect(priceChangeUrl);
         return;
      }

      // Update booking session identifier.
      paymentData.setBookingSessionId(getBookingSessionId(ipAddress, bookingComponent));
      request.getSession().setAttribute(DispatcherConstants.TOKEN, token);
      request.getSession().setAttribute(DispatcherConstants.BOOKING_INFO, bookingInfo);

      if (bookingInfo.getNewHoliday())
      {
         bookingInfo.reset();
      }
      else
      {
         bookingComponent.setErrorMessage(StringUtils.EMPTY);
         LogWriter.logInfoMessage(
            "Rendering payment page for the token " + token + " after the successful booking");
      }

      // To be removed once ATOL feature goes live
      // TODO : Commented till we get the these flags from Client Application
      // LogWriter.logDebugMessage("ATOL flag value for the token " + token +
      // ": "
      // + bookingComponent.getNonPaymentData().get("atol_flag"));
      // LogWriter.logDebugMessage("ATOL protected value for the token " +
      // token + ": "
      // + bookingComponent.getNonPaymentData().get("atol_protected"));

      updateTransactionData(paymentData, request, bookingInfo);

      String clientApplication = paymentData.getBookingInfo().getBookingComponent()
         .getClientApplication().getClientApplicationName();
      int failureCount = ConfReader.getIntEntry(clientApplication + "." + MAX_FAILURE_COUNT, 0);

      if (failureCount == 0)
      {
         failureCount =
            ConfReader.getIntEntry("default." + MAX_FAILURE_COUNT, DEFAULT_FAILURE_COUNT);
      }

      if (StringUtils.isNotBlank(bookingComponent.getPaymentFailureURL())
         && paymentData.getFailureCount() >= failureCount)
      {
         bookingInfo.setNewHoliday(Boolean.FALSE);
         paymentFailureRedirect = true;
      }
      String url = "";
      if (null != bookingComponent.getLockYourPriceSummary()
         && bookingComponent.getLockYourPriceSummary().isSelected())
      {
         url = getPaymentPageUrl(bookingComponent.getClientApplication(),
            bookingComponent.getLockYourPriceSummary().getClientApplication(), isCheckinPayment);
      }
      else
      {
         url = getPaymentPageUrl(bookingComponent.getClientApplication(), StringUtils.EMPTY, isCheckinPayment);
      }

      /*
       * if(!(isApplicableBrand(bookingComponent.getClientApplication().
       * getClientApplicationName())) && !getCommonPaymentPageEnabled()){
       *
       * url = getPaymentUrl(bookingComponent.getClientApplication()); } else{ url =
       * "/tuicommon/tuicommonPaymentPage.jsp"; }
       */

      // This code should be removed once sub brand is sent from WSS and BRAC.
      // This populates the card data from falcon brand for Falcon bookings in
      // WSS.
      if ("21000".equalsIgnoreCase(request.getParameter("b")))
      {
         bookingInfo.updatePaymentInfo("FALCONBYO");
      }
      RequestDispatcher requestDispatcher = request.getRequestDispatcher(url);
      if (paymentData.getPayment() == null)
      {
         new TransactionStatusUtil().updateStatus(paymentData, token,
            paymentData.getTransactionStatus(),
            TransactionStatus.findByCodeTS("REDIRECT_TO_PAYMENT_URL"));
      }
      // boolean isHccTrue = ConfReader.getBooleanEntry("tuicommon.hcc", false);
      boolean isHccTrue = ConfReader.getBooleanEntry(
         bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
     
      if (isHccTrue && !isACSSBrand(clientApplication))
      {
         if (paymentFailureRedirect
            && Objects.nonNull(bookingInfo.getBookingComponent().getPaymentFailureURL()))
         {
            Map<String, String> hcsMap = new HashMap<String, String>();
            hcsMap.put("hccSwitch", String.valueOf(isHccTrue));
            hcsMap.put("paymentFailure", String.valueOf(paymentFailureRedirect));
            bookingComponent.setHccsMap(hcsMap);
            requestDispatcher.forward(request, response);
            return;
         }
         if (newSetupReq)
         {
            bookingComponent.setHccsMap(setUpReqHCS(bookingInfo, request));
         }
         if (bookingComponent.isSetUpFails())
         {
            requestDispatcher.forward(request, response);
            return;
         }
      }
      else
      {
    	  String hccSwitch = String.valueOf(isHccTrue);
			if(bookingComponent.getHccsMap()==null){
			Map<String, String> hcsMap = new HashMap<String, String>();
			hcsMap.put("hccSwitch", hccSwitch);
			bookingComponent.setHccsMap(hcsMap);
			}
      }

      requestDispatcher.forward(request, response);
   }

   public Map<String, String> setUpReqHCS(BookingInfo bookingInfo, HttpServletRequest request)
   {

      BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      String merchantReference = getMerchantReference(bookingComponent);
      bookingComponent.setMerchantReference(merchantReference);

      String vTid = bookingComponent.getPaymentGatewayVirtualTerminalId();
      String password = ConfReader.getConfEntry(vTid, null);

      String returnUrl = getHccReturnURL(request);
      String expiryUrl = getHccExpiryURL(request);
      String pageSetId = getPageSetId(
         bookingComponent.getClientApplication().getClientApplicationName(), bookingComponent);

      Map<String, String> hcsMap = new HashMap<String, String>();
      XMLDocument xmlDocument1;
      
      String clientApplName = bookingComponent.getClientApplication().getClientApplicationName();
	  String nonHybrisBrands = ConfReader.getConfEntry("nonhybris.brands", null);
      try
      {
         xmlDocument1 = new XMLDocument();
         xmlDocument1.set(HCC_TRANSACTION_TYPE, SETUP);

         if (returnUrl != null && returnUrl != "")
         {
            xmlDocument1.set(RETURN_URL, returnUrl);
         }

         if (expiryUrl != null && expiryUrl != "")
         {
            xmlDocument1.set(EXPIRY_URL, expiryUrl);
         }

         if (pageSetId != null && pageSetId != "")
         {
            xmlDocument1.set(PAGE_SET_ID, pageSetId);
         }

         if (!StringUtils.contains(nonHybrisBrands, clientApplName)) {
 		 	setHybrisDynamicData(bookingInfo, xmlDocument1);
 		 } else {
 		 	setNonHybrisDynamicData(bookingInfo, xmlDocument1);
 		 }

         xmlDocument1.set(MERCHANT_REFERENCE, merchantReference);

         xmlDocument1.set(CLIENT, vTid);
         xmlDocument1.set(PASSWORD, password);

         LogWriter.logDebugMessage(xmlDocument1.getSanitizedDoc());
         LogWriter.logInfoMessage(logSetupTransaction(request, bookingComponent));
         XMLDocument response = null;
         try
         {
            long startTime = System.currentTimeMillis();
            response = getAgent().request(xmlDocument1, new Logger());
            long elapsedTime = System.currentTimeMillis() - startTime;
            LogWriter.logDebugMessage("Time taken for HCC setup : " + elapsedTime);
         }
         catch (FailureReport failureReport)
         {
            String errorMessage = PropertyResource.getProperty(DATACASH_ERROR, MESSAGES_PROPERTY)
               + vTid + NEXT_LINE + failureReport.getMessage();
            LogWriter.logErrorMessage(errorMessage, failureReport);
            throw new RuntimeException(errorMessage);

//				e.printStackTrace();

         }
         catch (Exception e)
         {
            LogWriter
               .logInfoMessage("Setup response having some issue. Please review the response");
            LogWriter.logErrorMessage(e.getMessage(), e);
         }

         String value = response.get(HPS_URL);
         String value1 = response.get(SESSION_ID);
         String hpsDatacashReference = response.get(DATACASH_REFERENCE_RS);
         String reason = response.get(DataCashServiceConstants.REASON);
         String status = response.get(DataCashServiceConstants.STATUS);

         // Handling -ve scenario for setUp Request
         String url = "";

         if (reason != "ACCEPTED" && !status.equals("1"))
         {
            // constructing the url for error page
            url = "/common/hccError.jsp?b=" + request.getParameter("b") + "&tomcat="
               + request.getParameter("tomcat") + "";
            bookingComponent.setSetUpFails(true);

         }
         else
         {
            bookingComponent.setSetUpFails(false);
         }

         LogWriter.logDebugMessage(response.getSanitizedDoc());

         hcsMap.put(HCC_URL_KEY, value);
         hcsMap.put(HCC_SESSION_KEY, value1);
         hcsMap.put(HCC_DATACASH_RS_KEY, hpsDatacashReference);
         hcsMap.put(DataCashServiceConstants.SETUP_FAIL_ERROR_URL, url);
         String hccSwtch = "true";
         hcsMap.put("hccSwitch", hccSwtch);
         hcsMap.put("paymentFailure", "false");
         bookingInfo.setNewSetupReq(false);
         
         LogWriter.logInfoMessage(
            DataCashServiceConstants.SETUP + ", " + request.getParameter(DispatcherConstants.TOKEN)
               + ", " + response.get(DataCashServiceConstants.SETUP_MERCHANTREFERENCE) + ", "
               + StringUtils.defaultIfEmpty(response.get(DataCashServiceConstants.REASON),
                  StringUtils.EMPTY)
               + ", " + StringUtils.defaultIfEmpty(response.get(DataCashServiceConstants.STATUS),
                  StringUtils.EMPTY));


      }
      catch (IOException e)
      {
         LogWriter.logErrorMessage(e.getMessage(), e);
      }
      catch (JDOMException e)
      {
         LogWriter.logErrorMessage(e.getMessage(), e);
      }
      return hcsMap;
   }

   private Agent getAgent()
   {
      Agent agent = new Agent();
      // This is the place u would get appropriate host
      String host = ConfReader.getConfEntry(HOST_NAME, null);

      if (StringUtils.isBlank(host))
      {
         LogWriter.logErrorMessage(
            PropertyResource.getProperty("datacash.host.notfound", MESSAGES_PROPERTY));
         throw new RuntimeException(
            PropertyResource.getProperty("datacash.host.notfound", MESSAGES_PROPERTY));
      }
      agent.setHost(host);
      agent.setTimeout(ConfReader.getIntEntry("datacash.ConnectionTimeOut", 60000));
      return agent;
   }
   
   private String logSetupTransaction(HttpServletRequest request, BookingComponent bookingComponent)
   {

      StringBuilder logSetupTrans = new StringBuilder();
      logSetupTrans.append(SETUP + ", ")
         .append(request.getParameter(DispatcherConstants.TOKEN) + ", ")
         .append(bookingComponent.getMerchantReference() + ", ")
         .append(StringUtils.defaultIfEmpty(
            getPageSetId(bookingComponent.getClientApplication().getClientApplicationName(),
               bookingComponent),
            StringUtils.EMPTY) + ", ")
         .append(bookingComponent.getPaymentGatewayVirtualTerminalId());

      return logSetupTrans.toString();

   }
   
   private void setHybrisDynamicData(BookingInfo bookingInfo, XMLDocument xmlDocument) {

		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		ContactInfo contactInfo = bookingComponent.getContactInfo();
		if(contactInfo!=null){

		if (contactInfo.getHouseDetails() != null && contactInfo.getHouseDetails() != "") {
			xmlDocument.set(DYN_DATA_1, contactInfo.getHouseDetails());
		}

		if (contactInfo.getStreetAddress1() != null && contactInfo.getStreetAddress1() != "") {
			xmlDocument.set(DYN_DATA_2, contactInfo.getStreetAddress1());
		}

		if (contactInfo.getStreetAddress2() != null && contactInfo.getStreetAddress2() != "") {
			xmlDocument.set(DYN_DATA_3, contactInfo.getStreetAddress2());
		}

		if (contactInfo.getCity() != null && contactInfo.getCity() != "") {
			xmlDocument.set(DYN_DATA_4, contactInfo.getCity());
		}

		if (contactInfo.getCountryName() != null && contactInfo.getCountryName() != "") {
			xmlDocument.set(DYN_DATA_5, contactInfo.getCountryName());
		}

		if (contactInfo.getPostCode() != null && contactInfo.getPostCode() != "" && isNotIreland(bookingComponent)) {
			xmlDocument.set(DYN_DATA_6, contactInfo.getPostCode());
		}

		if (bookingComponent.getTermsAndCondition() != null
				&& bookingComponent.getTermsAndCondition().getRelativeTAndCUrl() != null) {
			xmlDocument.set(DYN_DATA_7, bookingComponent.getTermsAndCondition().getRelativeTAndCUrl());
		}

		if (contactInfo.getCountry() != null && contactInfo.getCountry() != "") {
			xmlDocument.set(DYN_DATA_8, contactInfo.getCountry());
		}
		
		}
		if (isDDApplicableBrand(bookingComponent.getClientApplication())
				&& getDDDeposit(bookingComponent, bookingInfo) != "") {
			xmlDocument.set(DYN_DATA_9, getDDDeposit(bookingComponent, bookingInfo));
		}

	}
  
  private void setNonHybrisDynamicData(BookingInfo bookingInfo, XMLDocument xmlDocument) {

		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		Map<String, String> paymentDataMap = bookingComponent.getNonPaymentData();

		if (paymentDataMap.get("house_name") != null && paymentDataMap.get("house_name") != "") {
			xmlDocument.set(DYN_DATA_1, paymentDataMap.get("house_name"));
		}

		if (paymentDataMap.get("street_address1") != null && paymentDataMap.get("street_address1") != "") {
			xmlDocument.set(DYN_DATA_2, paymentDataMap.get("street_address1"));
		}

		if (paymentDataMap.get("street_address2") != null && paymentDataMap.get("street_address2") != "") {
			xmlDocument.set(DYN_DATA_3, paymentDataMap.get("street_address2"));
		}

		if (paymentDataMap.get("street_address3") != null && paymentDataMap.get("street_address3") != "") {
			xmlDocument.set(DYN_DATA_4, paymentDataMap.get("street_address3"));
		}

		if (paymentDataMap.get("cardBillingPostcode") != null && paymentDataMap.get("cardBillingPostcode") != "") {
			xmlDocument.set(DYN_DATA_6, paymentDataMap.get("cardBillingPostcode"));
		}

	}

   private String getHccReturnURL(HttpServletRequest request)
   {

      final String COLON = ":";
      String token = request.getParameter(DispatcherConstants.TOKEN);
      String tomIns = request.getParameter(DispatcherConstants.TOMCAT_INSTANCE);
      String brandCode = request.getParameter("b");
      // StringBuffer strBuffer = new StringBuffer(request.getScheme() + "://");
      
      StringBuffer strBuffer =
               new StringBuffer(ConfReader.getConfEntry("hcc.protocol", "https") + "://");
      String domainReturnUrl =ConfReader.getConfEntry("hcc.domain.returnurl", "");
      String serverName=(ConfReader.getBooleanEntry("hcc.hybd8.environment", false)?domainReturnUrl:request.getServerName());
      // StringBuffer strBuffer=new StringBuffer("https://");
      strBuffer.append(serverName)
         .append(Boolean.valueOf(ConfReader.getConfEntry("hcc.dev.environment", "false"))
            ? COLON + ConfReader.getConfEntry("hcc.dev.environment.port", "9090") : "")
         .append(request.getContextPath()).append("/hccprocess?b=").append(brandCode)
         .append("&token=").append(token).append("&tomcat=").append(tomIns);
      return strBuffer.toString();

   }

   private String getHccExpiryURL(HttpServletRequest request)
   {

      final String COLON = ":";
      StringBuffer strBuffer =
         new StringBuffer(ConfReader.getConfEntry("hcc.protocol", "https") + "://");
      // StringBuffer strBuffer=new StringBuffer("https://");
      strBuffer.append(request.getServerName())
         .append(Boolean.valueOf(ConfReader.getConfEntry("hcc.dev.environment", "false"))
            ? COLON + ConfReader.getConfEntry("hcc.dev.environment.port", "9090") : "")      
         .append(request.getContextPath()).append("/common/")
         .append(DispatcherConstants.HCC_EXPIRY_URL);

      return strBuffer.toString();

   }

   private String getPageSetId(String clientApp, BookingComponent bookingComponent)
   {
	   String nonHybrisBrands = ConfReader.getConfEntry("nonhybris.brands", null);

      if (null != bookingComponent.getLockYourPriceSummary()
         && bookingComponent.getLockYourPriceSummary().isSelected())
      {
         clientApp = "LYP_" + bookingComponent.getClientApplication();
      }
      if (Boolean.valueOf(bookingComponent.getNonPaymentData().get("isMarellaBooking")))
      {
         clientApp = "M_" + clientApp;
      }
      else if(null == bookingComponent.getFlightSummary())
      {
         if (!clientApp.contains("CRUISE") && (!StringUtils.contains(nonHybrisBrands, clientApp)))
         {
            clientApp = clientApp + "AO";
         }
      }
      if(isIrelandBrand(bookingComponent) && Boolean.valueOf(bookingComponent.getNonPaymentData().get("oscarEnabled")))
      { 	  
      	
          clientApp = "OSCAR_" + clientApp;         
 	  
      }

      return ConfReader.getConfEntry(clientApp + "." + DispatcherConstants.PAGESET_ID, null);
   }

   /**
    *
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @param bookingComponent the bookingComponent.
    *
    * @return merchantReference the generated unique merchant reference.
    */
   private static String getMerchantReference(BookingComponent bookingComponent)
   {
      String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
      if (clientApp.equalsIgnoreCase("NEWSKIES") || clientApp.equalsIgnoreCase("NEWSKIESM"))
      {
         return clientApp + '_'
            + bookingComponent.getNonPaymentData().get("booking_Reference_Number") + '_'
            + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
               MAX_REFERENCE_LENGTH_ACSS - (clientApp.length()
                  + bookingComponent.getNonPaymentData().get("booking_Reference_Number").length()));
      }else if(clientApp.equalsIgnoreCase("TUITHSHOREX") && ConfReader.getBooleanEntry("merchantid.enabled", false) ){
			
			if(!Objects.isNull(bookingComponent.getCruiseSummary()) && !Objects.isNull(bookingComponent.getCruiseSummary().getExcursionSummary())){
				
				String merchantId=bookingComponent.getCruiseSummary().getExcursionSummary().getMerchantID();
				if(StringUtils.isEmpty(merchantId)){
					merchantId=clientApp;
				}
				return merchantId
						+ StringUtils.left(String.valueOf(UUID.randomUUID())
								.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
								- clientApp.length());
				
				
				
			}else{
				return clientApp
				+ StringUtils.left(String.valueOf(UUID.randomUUID())
						.replaceAll(HYPHEN, ""), MAX_REFERENCE_LENGTH
						- clientApp.length());
			}
			
			
		}
      else
      {
         return clientApp
            + StringUtils.left(String.valueOf(UUID.randomUUID()).replaceAll(HYPHEN, ""),
               MAX_REFERENCE_LENGTH - clientApp.length());
      }

   }

   /**
    * @param clientApplicationName
    * @param lypClientApplicationName
    * @return
    */
   private String getPaymentPageUrl(ClientApplication clientApplicationName,
      String lypClientApplicationName, String isCheckinPayment)
   {

      String tuiApplicableBrands = ConfReader.getConfEntry("tuicommon.applicableBrands", null);
      String fcApplicableBrands = ConfReader.getConfEntry("fccommon.applicableBrands", null);
      String faeApplicableBrands = ConfReader.getConfEntry("fae.applicableBrands", null);

      if (StringUtils.isEmpty(lypClientApplicationName))
      {
         lypClientApplicationName = clientApplicationName.getClientApplicationName();
      }
      String url = null;

      if (ConfReader.getBooleanEntry("tuicommon.paymentpage", false)
         && StringUtils.contains(tuiApplicableBrands, lypClientApplicationName))
      {

         url = "/tuicommon/tuicommonPaymentPage.jsp";
      }
      else if (ConfReader.getBooleanEntry("fccommon.paymentpage", false)
         && StringUtils.contains(fcApplicableBrands, lypClientApplicationName))
      {

         url = "/fccommon/fccommonPaymentPage.jsp";
      }
      else if (ConfReader.getBooleanEntry("fae.paymentpage", false)
         && StringUtils.contains(faeApplicableBrands, lypClientApplicationName)
         && (isCheckinPayment != null && isCheckinPayment.equalsIgnoreCase("true")))
      {

         url = "/amendandcancel/faeAncPaymentPage.jsp";

      }

      else
      {
         url = getPaymentUrl(clientApplicationName);
      }

      return url;
   }

   /**
    * @param clientApplicationName
    * @return
    */
   private boolean isApplicableBrand(String clientApplicationName)
   {
      String applicableBrands = ConfReader.getConfEntry("tui.applicableBrands", null);

      return StringUtils.contains(applicableBrands, clientApplicationName);

   }

   /**
    * @return
    */
   private boolean getCommonPaymentPageEnabled()
   {

      return ConfReader.getBooleanEntry("common.paymentpage", false);
   }

   /**
    * Gets the booking session identifier required for fraud screening.
    *
    * @param ipAddress the ipAddress.
    * @param bookingComponent the booking component.
    *
    * @return the booking session identifier.
    */
   private String getBookingSessionId(String ipAddress, BookingComponent bookingComponent)
   {
      return StringUtils
         .replace(ipAddress + bookingComponent.getClientApplication().getClientApplicationName()
            + bookingComponent.getBookingSessionIdentifier(), ".", StringUtils.EMPTY);
   }

   /**
    * Updates the transaction data required for JSP.
    *
    * @param paymentData the payment data.
    * @param request the HttpServletRequest object.
    * @param bookingInfo the BookingInfo object.
    */
   private void updateTransactionData(PaymentData paymentData, HttpServletRequest request,
      BookingInfo bookingInfo)
   {
      if (paymentData.getPayment() != null
         && paymentData.getPayment().getPaymentTransactions().size() == 1)
      {
         request.setAttribute("paymentTransactions",
            paymentData.getPayment().getPaymentTransactions());
         String transactionResponseMsg;
         if (ConfReader.getConfEntry("fraudscreening.reject.code", "DECLINED")
            .equalsIgnoreCase(bookingInfo.getFraudScreeningResponse()))
         {
            transactionResponseMsg = bookingInfo.getFraudScreeningResponse();
         }
         else
         {
            transactionResponseMsg = ((DataCashPaymentTransaction) paymentData.getPayment()
               .getPaymentTransactions(PaymentMethod.DATACASH).get(0)).getResponseMsg();
         }
         request.setAttribute("transactionResponseMsg", transactionResponseMsg);
      }
   }

   /**
    * @param bookingComponent
    * @return
    */
   private boolean isNotIreland(BookingComponent bookingComponent)
   {
      String clientApp = bookingComponent.getClientApplication().getClientApplicationName();
      boolean value = true;
      if (StringUtils.equalsIgnoreCase("TUIFALCON", clientApp)
         || StringUtils.equalsIgnoreCase("TUIFALCONFO", clientApp))
      {
         value = false;
      }
      return value;
   }

   private String getDDDeposit(BookingComponent bookingComponent, BookingInfo bookingInfo)
   {
      String ddDeposit = "";
      if (bookingComponent.getDepositComponents() != null
         && isDirectDebitAvailable(bookingComponent))
      {
         DepositComponent depositComponent = bookingComponent.getDepositComponents().get(0);
         if (depositComponent.getDepositType().equalsIgnoreCase("lowDeposit"))
         {
            BigDecimal amount =
               getCalculatedDepositAmount(depositComponent.getDepositAmount().getAmount(),
                  depositComponent.getOutstandingBalance().getAmount(),
                  bookingInfo.getCalculatedDiscount().getAmount());
            ddDeposit = amount.setScale(2).toString();
         }
         else if (depositComponent.getDepositType().equalsIgnoreCase("deposit"))
         {
            BigDecimal amount = depositComponent.getDepositAmount().getAmount();
            ddDeposit = amount.setScale(2).toString();
         }
      }
      return ddDeposit;
   }

   private boolean isDirectDebitAvailable(BookingComponent bookingComponent)
   {
      boolean ddAvailable = false;
      for (DepositComponent deposit : bookingComponent.getDepositComponents())
      {
         if (DD_DEPOSIT_TYPE.equalsIgnoreCase(deposit.getDepositType()))
            ddAvailable = true;
      }
      return ddAvailable;
   }

   private BigDecimal getCalculatedDepositAmount(BigDecimal deposit, BigDecimal outstandingBal,
      BigDecimal discount)
   {
      return deposit.add(outstandingBal).subtract(discount);
   }

   private boolean isDDApplicableBrand(ClientApplication clientApplication)
   {
      List<String> applicableBrands = Arrays.asList(StringUtils.split(ConfReader.getConfEntry(
         "dd.applicableBrands",
         "TUITH,TUIFC,TUITHCRUISE,THOMSONMOBILE,TUITHRIVERCRUISE"),
         ','));
      return applicableBrands.contains(clientApplication.getClientApplicationName());
   }
   
   /**
 * @param clientApplication
 * @return
 */
private boolean isACSSBrand(String clientApplication) {
	
	return "NEWSKIES".equalsIgnoreCase(clientApplication) || "NEWSKIESM".equalsIgnoreCase(clientApplication);
}



/**
 * @param HttpServletRequest
 * @param HttpServletResponse
 * This adds Secure,HttpOnly and SameSite=none attributes to the JSESSIONID cookie  in the response Header.
 * This is introduced to make our application compliant with the Chrome's new SameSite-by-default cookie
 * behavior which by default sets the cookie samesite attribute to Lax by default if no value is set. Due to this 
 * the session cookie is not being sent in cross site request and creates a new session for it when we got the response.
 * Thus the validation fails when compared with the session in booking info.
 * Setting it to secure and samesite=none will allow the cookie to be sent in cross site request. 
 */
private void addSameSiteForSessionCookie(HttpServletRequest request, HttpServletResponse response) {
	Cookie jsessionIdCookie = getCookie(request, "JSESSIONID");
	String userAgent = request
			.getHeader(DataCashServiceConstants.BROWSER_USER_AGENT);
	if (Objects.isNull(jsessionIdCookie)) {
		if(userAgent != null && (userAgent.contains("iPhone OS 12_") || userAgent.contains("iPad; CPU OS 12_") || 
				(userAgent.contains(" OS X 10_14_") && userAgent.contains("Version/") && userAgent.contains("Safari")))){
			LogWriter.logInfoMessage("Setting sameSite JSession id(iPhone/iPad/Mac)==" + jsessionIdCookie);
			response.addHeader("Set-Cookie", "JSESSIONID=" + request.getSession().getId() + "; Secure;Path="
					+ request.getContextPath() + "; HttpOnly;");
		}else{
			LogWriter.logInfoMessage("Setting sameSite JSession id==" + jsessionIdCookie);
			response.addHeader("Set-Cookie", "JSESSIONID=" + request.getSession().getId() + "; Secure;Path="
					+ request.getContextPath() + "; HttpOnly; SameSite=none");
		}
	}
	
}




/**
 * @param request
 * @param cookieName
 * @return the cookie if the provided cookie name present in request with the
 *         path same as that of the content path else returns null.
 */
public Cookie getCookie(HttpServletRequest request, String cookieName) {
	if (request.getCookies() != null) {
		for (Cookie cookie : request.getCookies()) {
				LogWriter.logInfoMessage("Looking for CPS site Cookie Name="
						+ cookie.getName() + " Value =" + cookie.getValue()
						+ " Path =" + cookie.getPath() + " Secure = "
						+ cookie.getSecure() + " Domain = "
						+ cookie.getDomain() + " MaxAge = "
						+ cookie.getMaxAge() + " Version = "
						+ cookie.getVersion());
			if (StringUtils.equalsIgnoreCase(cookie.getName(), cookieName)
					&& StringUtils.equalsIgnoreCase(request.getContextPath(), cookie.getPath())) {
				return cookie;
			}
		}
	}
	return null;
}



public boolean isIrelandBrand(BookingComponent bookingComponent){
	String clientApp=bookingComponent.getClientApplication().getClientApplicationName();
	ArrayList<String> irelandBrands=new ArrayList<String>();
	irelandBrands.add("TUIFALCON");
	irelandBrands.add("TUIFALCONFO");
	irelandBrands.add("TUIES");
	irelandBrands.add("ANCFALCON");
	irelandBrands.add("ANCFJFO");
	irelandBrands.add("TUIFALCONAO");
	
		
	return irelandBrands.contains(clientApp);
}



}