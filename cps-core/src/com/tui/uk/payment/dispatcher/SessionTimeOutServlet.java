/*
 * Copyright (C)2006 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: SessionTimeOutServlet.java$
 *
 * $Revision: $
 *
 * $Date: Nov 7, 2013$
 *
 * Author: thyagaraju.e
 *
 *
 * $Log: $
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This class is used to reset the session on ajax call.
 *
 * @author thyagaraju.e
 *
 */
@SuppressWarnings("serial")
public class SessionTimeOutServlet extends HttpServlet
{

   /**
    * Returns the response.
    *
    * @param request the request
    * @param response the response
    * @throws ServletException the servlet exception
    * @throws IOException Signals that an I/O exception has occurred.
    * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest,
    *      javax.servlet.http.HttpServletResponse)
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      response.getWriter().write("true");
   }

   /**
    * Calls the Post method.
    *
    * @param request the request
    * @param response the response
    * @throws ServletException the servlet exception
    * @throws IOException Signals that an I/O exception has occurred.
    * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest,
    *      javax.servlet.http.HttpServletResponse)
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      doPost(request, response);
   }

}
