package com.tui.uk.payment.dispatcher;

import static com.tui.uk.config.PropertyConstants.MESSAGES_PROPERTY;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.ACCEPT_LANGUAGE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.HYPHEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.PRE_AUTHENTICATION_SESSION_TIMEOUT;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.SESSION_MAX_INTERVAL;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.THREE_D_SECURE_CONF_VALUE;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.USER_AGENT;
import static com.tui.uk.payment.service.datacash.DataCashServiceConstants.PAYPAL_HOST_NAME;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.MessageFormat;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutData;
import com.tui.uk.client.domain.PayPalSetExpressCheckoutResponse;
import com.tui.uk.config.ConfReader;
import com.tui.uk.config.PropertyResource;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.service.datacash.DataCashServiceConstantsForPayPalTransaction;
import com.tui.uk.payment.service.datacash.PayPalConstants;
import com.tui.uk.validation.constants.ValidationConstants;
import com.tui.uk.xmlrpc.handlers.DataCashServiceHandler;
import com.tui.uk.client.domain.BookingConstants;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.client.domain.Money;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.client.domain.PayPalComponent;

public class PayPalServlet extends HttpServlet {

	   /** The deposit type */
	   private static final String DEPOSIT = "depositType";


	@SuppressWarnings("unchecked")
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String redirectionURL = null;
		PayPalSetExpressCheckoutData paypalExpressCheckout = null;

		LogWriter
				.logInfoMessage("paypal servlet execution started.............................................."
						+ request.getParameterMap().get("depositType"));
		String token = request.getParameter("token");
		String tomcat = request.getParameter("tomcat");
		Map<String, String[]> parameterMap = (Map<String, String[]>) request
				.getParameterMap();
		String depositTypes[] = null;
		if(parameterMap.containsKey("depositType")){
		    depositTypes = parameterMap.get("depositType");
		}

		String depositType = null;
		String amount=null;
		if (null != depositTypes){
			depositType = depositTypes[0];
		}

		UUID uuid = UUID.fromString(token);
		try {
			PaymentData paymentData = PaymentStore.getInstance()
					.getPaymentData(uuid);
			BookingInfo bookingInfo=paymentData.getBookingInfo();
			BookingComponent bookingComponent = paymentData.getBookingInfo()
					.getBookingComponent();
			   String agreeTandC[]=parameterMap.get("agreeTermAndCondition");
		      if (agreeTandC == null)
		      {
		    	  String browserLocale = request.getHeader(ACCEPT_LANGUAGE);
		          Locale locale =
		             new Locale(StringUtils.substringBefore(browserLocale, HYPHEN),
		                StringUtils.substringAfterLast(browserLocale, HYPHEN));
		         String errorMessage =getErrorMessage("booking.termsandconditions.notchecked", locale,null);
		         LogWriter.logErrorMessage(errorMessage);
		         bookingInfo.getBookingComponent().setErrorMessage(errorMessage);
		         response.sendRedirect(request.getHeader("referer"));
		         return;
		      }

			if (isDirectDebitSelected(depositType)) {
				paymentData.getBookingInfo().getPaymentDataMap()
						.put("ddAndPayPalSelected", "true");
			}


			setDepositComponentForPartPayment(bookingComponent,parameterMap,depositType);
			if(isPayPalSelected(parameterMap.get("cardtype")[0])){
				bookingComponent.getNonPaymentData().put("cardtype", parameterMap.get("cardtype")[0]);

			}
			DataCashServiceHandler dataCashServiceHandler = new DataCashServiceHandler();
			
			if (Objects.nonNull(bookingComponent.getPaypalComponent())) {
				if ((Objects.nonNull(bookingComponent.getNonPaymentData().get("resetPayPalSetupSwitch")) && 
						StringUtils.equalsIgnoreCase("true",bookingComponent.getNonPaymentData().get("resetPayPalSetupSwitch")))){
					paypalExpressCheckout = bookingComponent
							.getPaypalComponent().getPayPalExpressCheckout();
							paypalExpressCheckout.setAmount(getAmount(depositType,bookingComponent,parameterMap.get("cardtype")[0]));
							if (Objects.isNull(bookingComponent.getPaypalComponent()
									.getPayPalExpressCheckoutResponse())) {
								String cancelURL = request.getHeader("referer");
								paypalExpressCheckout.setCancelUrl(cancelURL);
							} else {
								paypalExpressCheckout.setMerchantReference(createUniqueMerchantRef());
							}
							paypalExpressCheckout.setCardType(parameterMap.get("cardtype")[0]);
							PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse = dataCashServiceHandler
									.setUpPayPal(token);
							if(paypalExpressCheckoutResponse==null){
								bookingComponent.getPaypalComponent().setPaypalOptionActive(
										Boolean.parseBoolean("false"));
								sendAjaxResponse(response, request.getHeader("referer"));
							}
							if (isSetExpressCheckoutSuccess(paypalExpressCheckoutResponse
									.getStatus())) {

								//Once the payPal setUp API call is succesfull.
								bookingComponent.getNonPaymentData().put("payPalDefaultSelection","true");

								redirectionURL = redirectToPayPalLoginUrl(paypalExpressCheckoutResponse
										.getToken());

							} else {
								redirectionURL = request.getHeader("referer");
								bookingComponent.getPaypalComponent().setPaypalOptionActive(
										Boolean.parseBoolean("false"));
								LogWriter
										.logInfoMessage("Redirecting to payment page : "
												+ redirectionURL);
							}
						}
				else {
					if (Objects.nonNull(bookingComponent.getPaypalComponent()
							.getPayPalExpressCheckoutResponse())) {
						if(StringUtils.isNotEmpty(bookingComponent
								.getPaypalComponent()
								.getPayPalExpressCheckoutResponse().getToken())){
							redirectionURL = redirectToPayPalLoginUrl(bookingComponent
									.getPaypalComponent()
									.getPayPalExpressCheckoutResponse().getToken());
						}
						else{
							redirectionURL=request.getHeader("referer");
						}

					} else {
						amount=getAmount(depositType,bookingComponent,parameterMap.get("cardtype")[0]);
						paypalExpressCheckout = bookingComponent
								.getPaypalComponent().getPayPalExpressCheckout();
						paypalExpressCheckout.setAmount(amount);
						String cancelURL = request.getHeader("referer");
						paypalExpressCheckout.setCancelUrl(cancelURL);
						paypalExpressCheckout.setCardType(parameterMap.get("cardtype")[0]);
						PayPalSetExpressCheckoutResponse paypalExpressCheckoutResponse = dataCashServiceHandler
								.setUpPayPal(token);
						if(paypalExpressCheckoutResponse==null){
							bookingComponent.getPaypalComponent().setPaypalOptionActive(
									Boolean.parseBoolean("false"));
							sendAjaxResponse(response, request.getHeader("referer"));
						}
						if (isSetExpressCheckoutSuccess(paypalExpressCheckoutResponse
								.getStatus())) {

							//Once the payPal setUp API call is succesfull.
							bookingComponent.getNonPaymentData().put("payPalDefaultSelection","true");

							redirectionURL = redirectToPayPalLoginUrl(paypalExpressCheckoutResponse
									.getToken());

						} else {
							redirectionURL = request.getHeader("referer");
							bookingComponent.getPaypalComponent().setPaypalOptionActive(
									Boolean.parseBoolean("false"));
							LogWriter
							.logInfoMessage("Redirecting to payment page : "
									+ redirectionURL);
						}

					}
				}
				if ("local".equals(tomcat)) {
					response.setContentType("text/plain");
					response.setCharacterEncoding("UTF-8");
					response.sendRedirect(redirectionURL);
				}

				sendAjaxResponse(response, redirectionURL);

			}
		} catch (Exception ex) {
			LogWriter.logErrorMessage(ex.getMessage());

		}

	}

	/**
	 * @param bookingComponent
	 * @param parameterMap
	 */
	private void setDepositComponentForPartPayment(
			BookingComponent bookingComponent,
			Map<String, String[]> requestParameterMap,String depositType) {

	      Currency GBP = Currency.getInstance(Locale.UK);
	      Currency EUR = Currency.getInstance("EUR");

	    if (!bookingComponent.getClientApplication().getClientApplicationName().equals("ANCTHFO") || !bookingComponent.getClientApplication().getClientApplicationName().equals("ANCFJFO"))
	      {
	         if(depositType != null)
	         {
	            if (depositType.equalsIgnoreCase("PART_PAYMENT"))
	            {
	               for (DepositComponent depositComponent : bookingComponent.getDepositComponents())
	               {
	                  if (depositComponent.getDepositType().equalsIgnoreCase("PART_PAYMENT"))
	                  {
	                     BigDecimal PART_PAYMENT = new BigDecimal(requestParameterMap.get("part")[0]);

	                     if (bookingComponent.getClientApplication().getClientApplicationName()
	                        .equals("ANCFALCON"))
	                     {
	                        depositComponent.setDepositAmount(new Money(PART_PAYMENT, EUR));
	                     }
	                     else
	                     {
	                        depositComponent.setDepositAmount(new Money(PART_PAYMENT, GBP));
	                     }
	                     if (PART_PAYMENT.compareTo(BigDecimal.ZERO) == -1)
	                     {
	                        throw new RuntimeException(
	                           "invalid part payment amount   :   "+PART_PAYMENT);
	                     }
	                  }
	               }
	            }
	         }
	      }


	}

	/**
	 * @param cardtype
	 * @return
	 */
	private boolean isPayPalSelected(String cardtype) {
		if(StringUtils.isEmpty(cardtype)){
			return false;
		}
		else
		{
			return PayPalConstants.PAYPAL.equalsIgnoreCase(cardtype) || PayPalConstants.PAYPAL_CREDIT.equalsIgnoreCase(cardtype) ;
		}

	}

	/**
	 * @param cardtype
	 * @return
	 */
	private boolean isPayPalCredit(String cardtype) {
		
			return  PayPalConstants.PAYPAL_CREDIT.equalsIgnoreCase(cardtype) ;
		}
	/**
	 * @param depositType
	 * @param bookingComponent
	 * @return
	 */
	private String getAmount(String depositType,
			BookingComponent bookingComponent ,String cardType) {
/*		String amount=null;
		List<DepositComponent> depositComponents=bookingComponent.getDepositComponents();
		amount = String.valueOf(bookingComponent.getTotalAmount().getAmount().doubleValue());
		for(DepositComponent depositComponent:depositComponents){
			if(StringUtils.equalsIgnoreCase(depositComponent.getDepositType(), depositType)){
				amount = String.valueOf(depositComponent.getDepositAmount().getAmount().doubleValue());
				break;
			}
		}
		if(isAncFullcost(bookingComponent,depositType)){
			amount=String.valueOf(bookingComponent.getPayableAmount().getAmount().doubleValue());
		}

		return amount;*/


		 Money transactionAmount = null;

	      String clientApp = null;
	      if (bookingComponent.getClientApplication() != null)
	      {
	         clientApp = bookingComponent.getClientApplication().getClientApplicationName();
	      }
	      bookingComponent.getNonPaymentData().put(BookingConstants.DEPOSIT_TYPE, depositType);

	      if ((StringUtils.equalsIgnoreCase(depositType, DispatcherConstants.FULL_COST) || isPayPalCredit(cardType)) && !clientApp.contains("ANC"))
	      {
	         transactionAmount = bookingComponent.getPayableAmount();
	      }
	      else
	      {
	    	   if(depositType==null && ("ANCTHFO".equals(clientApp) || "ANCFJFO".equals(clientApp)) ){


	            	 if ((bookingComponent.getAmendmentCharge() != null) && (bookingComponent.getAmendmentCharge().getAmount().doubleValue() > 0.00))
		               {
		                  transactionAmount = bookingComponent.getAmendmentCharge();
		               } else if(bookingComponent.getPayableAmount() != null && bookingComponent.getPayableAmount().getAmount().doubleValue()>0.00 ){
		                       transactionAmount =bookingComponent.getPayableAmount();

		                   }
	            	 return String.valueOf(transactionAmount.getAmount().doubleValue());

	  	      }

	         if ((clientApp != null)
	            && (!StringUtils.equalsIgnoreCase(clientApp, "ANCTHFO"))
	            && ((StringUtils.equalsIgnoreCase(clientApp, "ANCTHOMSON"))
	               || (StringUtils.equalsIgnoreCase(clientApp, "ANCFIRSTCHOICE"))
	               || (StringUtils.equalsIgnoreCase(clientApp, "ANCFJFO"))
	               || (StringUtils.equalsIgnoreCase(clientApp, "ANCTHCRUISE")) || (StringUtils
	                  .equalsIgnoreCase(clientApp, "ANCFALCON"))))
	         {
	            Money amendmentChargeMoney = bookingComponent.getAmendmentCharge();
	            BigDecimal amendmentChargeBD = null;
	            if (StringUtils.equalsIgnoreCase(depositType, "onlyAmendCharge"))
	            {
	               if (amendmentChargeMoney != null)
	               {
	                  amendmentChargeBD = amendmentChargeMoney.getAmount();
	               }
	               if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
	               {
	                  transactionAmount = amendmentChargeMoney;
	               }
	            }
	            else if (depositType != null && depositType.equalsIgnoreCase("fullCost"))
                {
                    if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
                    {
                       if (bookingComponent.getPayableAmount() != null)
                       {
                          transactionAmount = bookingComponent.getAmendmentCharge().add(bookingComponent.getPayableAmount());
                       }
                       else
                       {
                    	  transactionAmount = bookingComponent.getAmendmentCharge();
                       }
                    }
                    else
                    {
                       transactionAmount=bookingComponent.getPayableAmount();
                    }
                 }
	            else
	            {
	               List<DepositComponent> depositComponents = bookingComponent.getDepositComponents();
	               for (DepositComponent depositComponent : depositComponents)
	               {
	                  if (StringUtils.equalsIgnoreCase(depositComponent.getDepositType(), depositType))
	                  {
	                     transactionAmount = depositComponent.getDepositAmount();
	                 /*    if (amendmentChargeMoney != null)
	                     {
	                        amendmentChargeBD = amendmentChargeMoney.getAmount();
	                     }
	                     if ((amendmentChargeBD != null) && (amendmentChargeBD.doubleValue() > 0.00))
	                     {
	                        if (transactionAmount != null)
	                        {
	                           transactionAmount = transactionAmount.add(amendmentChargeMoney);
	                        }
	                     }*/
	                  }
	               }
	            }


	            if(depositType==null && transactionAmount==null){

	            	transactionAmount = bookingComponent.getAmendmentCharge();
	            }


	         }
	         else
	         {
	            List<DepositComponent> depositComponents = bookingComponent.getDepositComponents();
	            for (DepositComponent depositComponent : depositComponents)
	            {
	               if (StringUtils.equalsIgnoreCase(depositComponent.getDepositType(), depositType))
	               {
	                  transactionAmount = depositComponent.getDepositAmount();
	               }
	            }
	         }
	      }
	      if(transactionAmount==null){
	    	  transactionAmount=bookingComponent.getTotalAmount();
	      }
	      return String.valueOf(transactionAmount.getAmount().doubleValue());





























	}

	/**
	 * @param palpalExpressCheckoutResponse
	 */
	private String redirectToPayPalLoginUrl(final String paypalToken) {
		LogWriter
				.logInfoMessage("paypal service execution completed successfully..........................");

		String redirectionURL = ConfReader.getConfEntry(
				DataCashServiceConstantsForPayPalTransaction.PAYPAL_LOGIN_URL,
				null);
		redirectionURL = redirectionURL + paypalToken;
		LogWriter.logInfoMessage("Redirecting to paypal login page : "
				+ redirectionURL);
		return redirectionURL;
	}

	private boolean isSetExpressCheckoutSuccess(String statusCode) {

		return "1".equals(statusCode);
	}

	private boolean isDirectDebitSelected(String depositType) {

		return "lowDepositPlusDD".equalsIgnoreCase(depositType);
	}

	private void sendAjaxResponse(HttpServletResponse response, String output)
			throws IOException {
		response.setContentType("text/plain");
		response.setCharacterEncoding("UTF-8");
		response.sendRedirect(output);
	}

	private boolean isAncFullcost(BookingComponent bookingComponent,String depositType){
     String ancBrands=ConfReader.getConfEntry("manage.ancbrands", "ANCTHOMSON,ANCFIRSTCHOICE,ANCTHFO,ANCTHCRUISE,ANCFALCON,ANCFJFO");

     return ancBrands.contains(bookingComponent.getClientApplication().getClientApplicationName())&&depositType.equalsIgnoreCase("fullcost");

	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}


	protected final String getErrorMessage(String key, Locale locale, String defaultKey)
	   {
	      String[] keyData = StringUtils.split(key, '|');
	      String message = PropertyResource.getProperty(keyData[0], MESSAGES_PROPERTY, locale);
	      if (StringUtils.isNotBlank(message))
	      {
	         message = MessageFormat.format(message, (Object[]) keyData);
	      }
	      else
	      {
	         if (StringUtils.isNotBlank(defaultKey)
	            && StringUtils.equals(defaultKey, "payment.default.datacash.error"))
	         {
	            keyData[0] = "payment.default.datacash.error";

	         }
	         else
	         {
	            keyData[0] =
	            		"nonpaymentdatavalidation.default."
	                  + StringUtils.substringAfter(StringUtils.substringAfter(keyData[0],
	                     ValidationConstants.NON_PAYMENT_DATA_VALIDATION), ".");
	         }
	         message = PropertyResource.getProperty(keyData[0], MESSAGES_PROPERTY, locale);
	      }
	      return message;
	   }

	  /**
	    * Creates the unique merchant ref.
	    *
	    * @return the string
	    */
	   private String createUniqueMerchantRef()
	   {
	      return String.format(String.format("%016d", System.currentTimeMillis()));
	   }
}
