package com.tui.uk.payment.dispatcher;

import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.DepositComponent;
import com.tui.uk.config.ConfReader;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;


@SuppressWarnings("serial")
public class DepositTypeFinderServlet extends HttpServlet {
	 /**
	  * the deposit Type parameter name
	  */
	 private static final String DEPOSIT_TYPE = "depositType";



	 /**
	    * Receives an HTTP POST request and handles the request. The HTTP POST method uses the BIN to
	    * find the card type based on the card number.
	    *
	    * @param request the HttpServletRequest object that contains the request the client has made of
	    *           the servlet.
	    * @param response the HttpServletResponse object that contains the response the servlet sends to
	    *           the client.
	    *
	    * @throws IOException the IOException thrown if an input or output error is detected when the
	    *            servlet handles the request
	    * @throws ServletException the ServletException thrown if the request for the POST could not be
	    *            handled.
	    */
	 protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String token = request.getParameter(TOKEN);
		PaymentData paymentData = PaymentStore.getInstance().getPaymentData(UUID.fromString(token));
		BookingInfo bookingInfo = paymentData.getBookingInfo();
		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		Map<String, String> hccCapturedData = new HashMap<String, String>();

		String clientApplicaton = bookingComponent.getClientApplication().getClientApplicationName();

		String prefix = PaymentConstants.PAYMENT + 0;
		String depositType = request.getParameter(DEPOSIT_TYPE);
		String deposit = request.getParameter("deposit");

		if (clientApplicaton.equals("AtcomRes")) {			
			
			findPaymentAmt(bookingInfo, depositType, hccCapturedData, prefix);

			String amountValue = "";
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(amountValue);

		} else {
			List<DepositComponent> depositComponentList = bookingComponent.getDepositComponents();

			boolean isHccTrue = ConfReader.getBooleanEntry(
					bookingComponent.getClientApplication().getClientApplicationName() + ".hcc.switch", false);
			if (isHccTrue && depositType.equals("fullCost")) {
				hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT,
						bookingComponent.getPayableAmount().getAmount().toString());
				hccCapturedData.put("CURRENCY", bookingComponent.getPayableAmount().getCurrency().toString());
				hccCapturedData.put(DEPOSIT_TYPE, depositType);
				bookingComponent.setHccCapturedData(hccCapturedData);
			} else if (isHccTrue && depositType.equals("PART_PAYMENT")) {
				hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT, deposit);
				hccCapturedData.put("CURRENCY", bookingComponent.getPayableAmount().getCurrency().toString());
				hccCapturedData.put(DEPOSIT_TYPE, depositType);
				bookingComponent.setHccCapturedData(hccCapturedData);
			} else {
				for (DepositComponent depositComponent : depositComponentList) {

					if (depositComponent.getDepositType().equals(depositType)) {
						hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT,
								depositComponent.getDepositAmount().getAmount().toString());
						hccCapturedData.put("CURRENCY", depositComponent.getDepositAmount().getCurrency().toString());
						hccCapturedData.put(DEPOSIT_TYPE, depositType);
						bookingComponent.setHccCapturedData(hccCapturedData);

					}
				}
			}

			String selectedDepositType = "";
			response.setContentType("text/plain");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(selectedDepositType);

		}

	}
	 
	private void findPaymentAmt(BookingInfo bookingInfo, String despositType, Map<String, String> hccCapturedData,
			String prefix) {


		BookingComponent bookingComponent = bookingInfo.getBookingComponent();
		String currency = bookingComponent.getTotalAmount().getCurrency().toString();
		
		if (despositType.equalsIgnoreCase("fullCost")) {
			hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT,
					bookingComponent.getTotalAmount().getAmount().toString());
			hccCapturedData.put("CURRENCY", currency);

		} else if (despositType.equalsIgnoreCase("highBalance")) {

			hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT,
					bookingComponent.getNonPaymentData().get("deposit_high_amount"));
			hccCapturedData.put("CURRENCY", currency);

		} else if (despositType.equalsIgnoreCase("lowDeposit")) {

			hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT,
					bookingComponent.getNonPaymentData().get("deposit_low_amount"));
			hccCapturedData.put("CURRENCY", currency);
		} else {

			hccCapturedData.put(prefix + PaymentConstants.TRANSACTION_AMOUNT, despositType);
			hccCapturedData.put("CURRENCY", currency);
			
		}
		bookingComponent.setHccCapturedData(hccCapturedData);

		LogWriter.logDebugMessage("************ DynamciMap " + hccCapturedData);

	}



	  /**
	    * The doGet method. Receives an HTTP GET request and handles the request by calling the doPost
	    * method.
	    *
	    * @param request the HttpServletRequest object that contains the request the client has made of
	    *           the servlet.
	    * @param response the HttpServletResponse object that contains the response the servlet sends to
	    *           the client.
	    *
	    * @throws IOException the IO exception thrown if an input or output error is detected when the
	    *            Servlet handles the request.
	    * @throws ServletException the ServletException thrown if the request could not be handled.
	    */
	 protected void doGet(HttpServletRequest request, HttpServletResponse response)
		      throws ServletException, IOException {
		      doPost(request, response);
		   }

}
