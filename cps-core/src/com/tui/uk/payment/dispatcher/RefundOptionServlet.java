/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: RefundOptionServlet.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-11-07 08:25:26 $
 *
 * $Author: vinothkumar.k $
 *
 *
 * $Log: $.
 *
 */
package com.tui.uk.payment.dispatcher;

import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.RefundDetails;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.payment.domain.PaymentStore;
import static com.tui.uk.payment.domain.PaymentConstants.TRANSACTION_INDEX;
import static com.tui.uk.payment.dispatcher.DispatcherConstants.TOKEN;

/**
 * This class reads the transaction index and creates the
 * HTML elements and sent them back to the caller.
 *
 * @author vinothkumar.k
 *
 */
@SuppressWarnings("serial")
public final class RefundOptionServlet extends HttpServlet
{
   /**
    * Receives an HTTP POST request and handles the request.
    * The HTTP POST method calls the receives the transaction number and then based
    * on the index creates the refund options.
    *
    * @param request the HttpServletRequest object that contains the request the client has
    * made of the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet
    * sends to the client.
    *
    * @throws IOException the IOException thrown if an input or output error is detected when the
    * servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the POST could
    * not be handled.
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      final String transactionIndex = (String) request.getParameter(TRANSACTION_INDEX);
      final String token = request.getParameter(TOKEN);

      final BookingInfo bookingInfo = PaymentStore.getInstance()
            .getPaymentData(UUID.fromString(token)).getBookingInfo();
      final BookingComponent bookingComponent = bookingInfo.getBookingComponent();
      final List<RefundDetails> refundDetails = bookingComponent.getRefundDetails();
      final int length = refundDetails.size();
      StringBuilder refundOptionString = new StringBuilder();

      StringBuilder refundOpt = new StringBuilder();
      final Format formatter = new SimpleDateFormat("'on'  dd/MM/yy  'at' HH:mm:ss aaa");
      for (int index = 0; index < length; index++)
      {
         refundOptionString.append("<div class='cnpsize'><span class='alignThis'id='radioSpan");
         refundOptionString.append(index);
         refundOptionString.append("'><input type='radio' name='refund_");
         refundOptionString.append(transactionIndex);
         refundOptionString.append("' value='");
         refundOptionString.append(refundDetails.get(index).getTransactionAmount().getAmount());
         refundOptionString.append("' onclick='validateRadioChecking(");
         refundOptionString.append(transactionIndex);
         refundOptionString.append(", ");
         refundOptionString.append(refundDetails.get(index).getDatacashReference());
         refundOptionString.append(")'/></span><span class='alignThis marginLeft30px'>Txn");
         refundOptionString.append(index);
         refundOptionString.append("</span><span class='alignThis marginLeft5px'>");
         if (refundDetails.get(index).getTransactionAmount()
                  .getCurrency().toString().equalsIgnoreCase("EUR"))
         {
            refundOptionString.append("&euro;");
         }
         else
         {
            refundOptionString.append("&pound;");
         }
         refundOptionString.append(refundDetails.get(index).getTransactionAmount().getAmount());
         refundOptionString.append("</span><span class='alignThis marginLeft30px'>");
         refundOptionString.append(formatter.format(refundDetails.get(index).getTransactionDate()));
         refundOptionString.append("</span></div><br clear='all'/>");
      }
      refundOpt.append(length);
      refundOpt.append("~");
      refundOpt.append(refundOptionString);
      response.setContentType("text/html;charset=UTF-8");
      response.getWriter().write(refundOpt.toString());
   }

   /**
    * The doGet method.
    * Receives an HTTP GET request and handles the request by calling the doPost method.
    *
    * @param request the HttpServletRequest object that contains the request the client has
    * made of the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet
    * sends to the client.
    *
    * @throws IOException the IO exception thrown if an input or output error is detected when the
    * Servlet handles the request.
    * @throws ServletException the ServletException thrown if the request could not be handled.
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      doPost(request, response);
   }

}
