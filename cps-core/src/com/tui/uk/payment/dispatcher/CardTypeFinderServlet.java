/*
 * Copyright (C)2013 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park, Coventry, United Kingdom CV4
 * 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any actual or intended
 * publication of this source code.
 *
 * $RCSfile: AddressFinderServlet.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2008-05-15 08:25:26 $
 *
 * $Author: pushparaja.g@sonata-software.com $
 *
 *
 * $Log: $.
 */

package com.tui.uk.payment.dispatcher;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jdom.JDOMException;

import com.datacash.client.CardDetails;
import com.datacash.util.CardInfo;
import com.datacash.util.XMLDocument;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.service.datacash.DataCashServiceConstants;

/**
 * The CardTypeFinderServlet class. This servlet finds the card type entered by the user and returns
 * it to the UI.
 *
 * @author pushparaja.g@sonata-software.com
 */
@SuppressWarnings("serial")
public class CardTypeFinderServlet extends HttpServlet
{
   
   /** The brand parameter name. */
   private static final String CARD_NUMBER_PARAM = "cardNumber";
   
   /** The brand parameter name. */
   private static final String CREDIT_CARD_CONF_LOCATION = "CreditCard.location";   
   
   /** The brand parameter name. */
   private static final String NOT_AVAILABLE = "NA";
   
   /**
    * Receives an HTTP POST request and handles the request. The HTTP POST method uses the BIN to
    * find the card type based on the card number.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    *
    * @throws IOException the IOException thrown if an input or output error is detected when the
    *            servlet handles the request
    * @throws ServletException the ServletException thrown if the request for the POST could not be
    *            handled.
    */
   protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      String cardNumber = request.getParameter(CARD_NUMBER_PARAM);
      String thomsonCardRange = ConfReader.getConfEntry("ThomsonCreditcard.BINRange", null);
      String[] thomsonCardNumberList = null;
      if (null != thomsonCardRange && StringUtils.isNotEmpty(thomsonCardRange))
      {
         thomsonCardNumberList = thomsonCardRange.split(",");
      }
      String cardType = StringUtils.EMPTY;
      if (StringUtils.isNotEmpty(cardNumber))
      {
         CardInfo cardInfo = new CardInfo(ConfReader.getConfEntry(CREDIT_CARD_CONF_LOCATION, null));
         try
         {
            XMLDocument xmlDoc = new XMLDocument();
            CardDetails cardDetails = new CardDetails();
            cardDetails.put(DataCashServiceConstants.PAN, cardNumber);
            xmlDoc.set(cardDetails);
            xmlDoc.setCardInfo(cardInfo);
            cardInfo.setStrict(true);
            cardType = cardInfo.getScheme();
            if (null != thomsonCardNumberList && StringUtils.isNotEmpty(thomsonCardRange))
            {
               for (String thomsonCardNumber : thomsonCardNumberList)
               {
                  if (cardNumber.startsWith(thomsonCardNumber))
                  {
                     cardType = "Thomson Credit Card";
                     break;
                  }
               }
            }
         }
         catch (IOException ioe)
         {
            LogWriter.logInfoMessage("Exception occured while finding the card type",
               CardTypeFinderServlet.class.getName());
         }
         catch (JDOMException jdome)
         {
            LogWriter.logInfoMessage("Exception occured while finding the card type",
               CardTypeFinderServlet.class.getName());
         }
         if (StringUtils.isEmpty(cardType))
         {
            cardType = NOT_AVAILABLE;
         }
      }
      response.setContentType("text/plain");
      response.setCharacterEncoding("UTF-8");
      response.getWriter().write(cardType);
   }

   /**
    * The doGet method. Receives an HTTP GET request and handles the request by calling the doPost
    * method.
    *
    * @param request the HttpServletRequest object that contains the request the client has made of
    *           the servlet.
    * @param response the HttpServletResponse object that contains the response the servlet sends to
    *           the client.
    *
    * @throws IOException the IO exception thrown if an input or output error is detected when the
    *            Servlet handles the request.
    * @throws ServletException the ServletException thrown if the request could not be handled.
    */
   protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException
   {
      doPost(request, response);
   }

}
