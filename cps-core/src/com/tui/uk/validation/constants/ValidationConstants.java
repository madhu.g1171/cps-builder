/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ValidationConstants.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-06-25 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.validation.constants;


/**
 * The class ValidationConstants.
 * This contains the constants used in non payment data validation.
 *
 * @author roopesh.s
 *
 */
public final class ValidationConstants
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private ValidationConstants()
   {
      // Do nothing.
   }

   /** The constant for PERSONALDETAILS. */
   public static final String PERSONALDETAILS = "personaldetails_";

   /** The constant for AGE_FOR_VALIDATION. */
   public static final String AGE_FOR_VALIDATION = "_age_for_validation";

   /** The constant for AGECLASSIFICATION. */
   public static final String AGECLASSIFICATION = "_ageClassification";

   /** The constant for AGEBETWEEN_65_TO_75. */
   public static final String AGEBETWEEN_65_TO_75 = "ageBetween65To75";

   /** The constant for AGEBETWEEN_65_TO_75. */
   public static final String AGEBETWEEN_76_TO_84 = "ageBetween76To84";

   /** The constant for DURATION. */
   public static final String DURATION = "duration";

   /** The constant for ARRIVALDATE. */
   public static final String ARRIVALDATE = "arrivalDate";

   /** The constant for NOERROR. */
   public static final String NOERROR = "NoError";

   /** The constant for DAY. */
   public static final String DAY = "_day";

   /** The constant for MONTH. */
   public static final String MONTH = "_month";

   /** The constant for YEAR. */
   public static final String YEAR = "_year";

   /** The constant for CHILD. */
   public static final String CHILD = "CHILD";

   /** The constant for CHILD_CODE. */
   public static final String CHILD_CODE = "CHD";

   /** The constant for INFANT_CODE. */
   public static final String INFANT_CODE = "INF";

   /** The constant for UNDERSCORE. */
   public static final char UNDERSCORE = '_';

   /** The constant for SENIOR. */
   public static final String SENIOR = "senior";

   /** The constant for SENIOR2. */
   public static final String SENIOR2 = "senior2";

   /** The constant for SENIOR_COUNT. */
   public static final String SENIOR_COUNT = "SENIOR_COUNT";

   /** The constant for SENIOR2_COUNT. */
   public static final String SENIOR2_COUNT = "SENIOR2_COUNT";

   /** The constant for MAX_CHILD_AGE. */
   public static final String MAX_CHILD_AGE = "maxChildAge";

   /** The constant for MAX_INFANT_AGE. */
   public static final String MAX_INFANT_AGE = "maxInfantAge";

   /** The constant for MAX_AGE. */
   public static final String MAX_AGE = "_maxAge";
   
   /** The constant for MIN_AGE. */
   public static final String MIN_AGE = "_minAge";
   
   /** The constant for TRAVEL_WITH. */
   public static final String TRAVEL_WITH = "_travelWith";
   
   /** The constant for CHILD_MEALS_SELECTED. */
   public static final String CHILD_MEALS_SELECTED  = "_child_meals_selected";
   
   /** The constant for PASSENGER. */
   public static final String PASSENGER = "passenger_";
   
   /** The constant for PASSENGER_INF_COUNT. */ 
   public static final String PASSENGER_INF_COUNT = "passenger_INF_count";
   
   /** The constant for PASSENGER_CHD_COUNT. */
   public static final String PASSENGER_CHD_COUNT = "passenger_CHD_count";
   
   /** The constant for CHILD_AGE. */
   public static final String CHILD_AGE = "_childAge";
   
   /** The constant for CHILD_FORE_NAME. */
   public static final String CHILD_FORE_NAME = "_childForeName";
   
   /** The constant for CHILD_LAST_NAME. */ 
   public static final String CHILD_LAST_NAME = "_childLastName";
   
   /** The constant for TRAVEL_WITH_VALIDATION. */
   public static final String TRAVEL_WITH_VALIDATION = "travelWith";
   
   /** The constant for CHILD_MEALS_VALIDATION. */
   public static final String CHILD_MEALS_VALIDATION = "childMeals";
   
   /** The NON_PAYMENT_DATA_VALIDATION constant. */
   public static final String NON_PAYMENT_DATA_VALIDATION = "nonpaymentdatavalidation.";
   
   /** The constant for DOB_AFTER_TODAY. */
   public static final String DOB_AFTER_TODAY = ".age.dobaftertoday";

   /** The constant for INVALID_CHILD_COUNT. */
   public static final String INVALID_CHILD_COUNT = ".age.invalidchildcount";

   /** The constant for MAXAGENOTAVAILABLE. */
   public static final String MAX_AGE_NOT_AVAILABLE = ".age.maxagenotavailable";
   
   /** The constant for holding format of date of birth  .*/
   public static final String DATE_FORMAT = "dd/MM/yyyy";
   
   /** The delimiter for date of birth  .*/
   public static final String DELIMITER = "/";
   
   /** The constant for POST_CODE. */
   public static final String POST_CODE = "postCode";

   /** The constant for COUNTRY. */
   public static final String COUNTRY = "country";
}
