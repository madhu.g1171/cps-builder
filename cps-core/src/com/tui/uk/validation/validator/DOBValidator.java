/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DOBValidator.java,v $
 *
 * $Revision: 1.0 $
 *
 * $Date: 2009-06-22 08:25:26 $
 *
 * $Author: roopesh.s@sonata-software.com $
 *
 *
 * $Log: $.
 */
package com.tui.uk.validation.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.validation.constants.ValidationConstants;

/**
 * This class validates the dob fields.
 *
 * @author roopesh.s
 *
 */
public final class DOBValidator
{
   /**
    * Private constructor to stop instantiation of this class.
    */
   private DOBValidator()
   {
      // Do nothing.
   }

   /**
    * This method is validate child or infant age, enter in payment page.
    *
    * @param nonPaymentDetails the non payment data map.
    * @param ageKey the age key name.
    * @param clientApplication the client application object.
    *
    * @return string error message.
    */
   public static String validateDOB(Map<String, String> nonPaymentDetails, String ageKey,
      ClientApplication clientApplication)
   {
      String errorMessage = ValidationConstants.NOERROR; 
      String underscore = String.valueOf(ValidationConstants.UNDERSCORE);
      String passengerIndex = StringUtils.substringBetween(ageKey, underscore, underscore);
      
      Date dateOfBirth = buildDateOfBirth(nonPaymentDetails, passengerIndex); 

      if (dateOfBirth != null)
      {
         int age = Integer.parseInt(nonPaymentDetails.get(ageKey));
         String ageClassification =
            nonPaymentDetails.get(ValidationConstants.PERSONALDETAILS + passengerIndex
               + ValidationConstants.AGECLASSIFICATION);
         String maxAge =
            nonPaymentDetails.get(ValidationConstants.PERSONALDETAILS + passengerIndex
               + ValidationConstants.MAX_AGE);

         String minAge =
            nonPaymentDetails.get(ValidationConstants.PERSONALDETAILS + passengerIndex
               + ValidationConstants.MIN_AGE);

         String duration = nonPaymentDetails.get(ValidationConstants.DURATION);
         String arrivalDate = nonPaymentDetails.get(ValidationConstants.ARRIVALDATE);
         int ageAsPerRule = getAgeInYears(arrivalDate, Integer.parseInt(duration), dateOfBirth);
         
         errorMessage = validate(dateOfBirth, clientApplication.getClientApplicationName(),
            ageClassification, age, ageAsPerRule, maxAge, minAge);
      }
      return errorMessage;
   }
   
   
   /**
    * This method is responsible for DOB validation.
    * 
    * @param dateOfBirth the passenger date of birth.
    * @param clientApplication the client application.
    * @param ageClassification the ageClassification.
    * @param age the passenger age.
    * @param ageAsPerRule the passenger age.
    * @param maxAge the child or infant max age.
    * @param minAge the child or infant min age.
    * 
    * @return string error message.
    */
   private static String validate(Date dateOfBirth, String clientApplication,
      String ageClassification, int age, int ageAsPerRule, String maxAge, String minAge)
   {
      Date currentDate = new Date();

      if (dateOfBirth.after(currentDate))
      {
         return ValidationConstants.NON_PAYMENT_DATA_VALIDATION + clientApplication
            + ValidationConstants.DOB_AFTER_TODAY;
      }

      // For children dob entered should match the age entered in
      // search panel.
      if (ageClassification.equals(ValidationConstants.CHILD_CODE) && age != ageAsPerRule)
      {
         return ValidationConstants.NON_PAYMENT_DATA_VALIDATION + clientApplication
            + ValidationConstants.INVALID_CHILD_COUNT;
      }

      if (maxAge == null)
      {
         return ValidationConstants.NON_PAYMENT_DATA_VALIDATION + clientApplication
            + ValidationConstants.MAX_AGE_NOT_AVAILABLE;
      }

      return checkInvalidChild(clientApplication, ageClassification, ageAsPerRule, maxAge, minAge);
   }
   
   /**
    * This method is responsible for DOB validation.
    * 
    * @param clientApplication the client application.
    * @param ageClassification the ageClassification.
    * @param ageAsPerRule the passenger age.
    * @param maxAge the child or infant max age.
    * @param minAge the child or infant min age.
    * 
    * @return string error message.
    */
   
   private static String checkInvalidChild(String clientApplication, String ageClassification,
      int ageAsPerRule, String maxAge, String minAge)
   {
      // check for infant, as per Business on checkout
      // date infant completes 2 yrs
      // he is considered as child.

      if (ageClassification.equals(ValidationConstants.INFANT_CODE)
         && (ageAsPerRule >= Integer.parseInt(maxAge)))
      {
         return ValidationConstants.NON_PAYMENT_DATA_VALIDATION + clientApplication
            + ValidationConstants.INVALID_CHILD_COUNT;
      }

      // check for child, if checkout date child completes child max age
      // he is considered as adult.
      if (ageClassification.equals(ValidationConstants.CHILD_CODE)
         && ageAsPerRule > Integer.parseInt(maxAge))
      {
         return ValidationConstants.NON_PAYMENT_DATA_VALIDATION + clientApplication
            + ValidationConstants.INVALID_CHILD_COUNT;
      }

      if (ageClassification.equals(ValidationConstants.CHILD_CODE)
         && (ageAsPerRule < Integer.parseInt(minAge)))
      {
         return ValidationConstants.NON_PAYMENT_DATA_VALIDATION + clientApplication
            + ValidationConstants.INVALID_CHILD_COUNT;
      }

      return ValidationConstants.NOERROR;
   }
   
   /**
    * This method builds the date of birth from non payment data.
    *
    * @param nonPaymentDetails the non payment data map.
    * @param passengerIndex the passenger index.
    *
    * @return dateOfBirth passenger date of birth.
    */
   private static Date buildDateOfBirth(Map<String, String> nonPaymentDetails, 
      String passengerIndex)      
   {
      Date dateOfBirth = null;

      String day = ValidationConstants.PERSONALDETAILS + passengerIndex + ValidationConstants.DAY;
      String month =
         ValidationConstants.PERSONALDETAILS + passengerIndex + ValidationConstants.MONTH;
      String year = ValidationConstants.PERSONALDETAILS + passengerIndex + ValidationConstants.YEAR;

      if (nonPaymentDetails.containsKey(day) && nonPaymentDetails.containsKey(month)
         && nonPaymentDetails.containsKey(year))
      {
         StringBuilder dob = new StringBuilder();
         dob.append(nonPaymentDetails.get(day))
               .append(ValidationConstants.DELIMITER)
                  .append(nonPaymentDetails.get(month))
                     .append(ValidationConstants.DELIMITER)
                        .append(nonPaymentDetails.get(year));

         try
         {
            SimpleDateFormat simpleDateFormat =
               new SimpleDateFormat(ValidationConstants.DATE_FORMAT);
            dateOfBirth = simpleDateFormat.parse(dob.toString());
         }
         catch (ParseException parseException)
         {
            throw new RuntimeException("dob is not correct", parseException);
         }
      }
      return dateOfBirth;
   }

   /**
    * This method is responsible for getting age, it calculate age with
    * arrival date and duration.
    *
    * @param flightArrivalDate flight arrival date.
    * @param duration holiday duration.
    * @param dob passenger date of birth.
    *
    * @return age passenger age.
    */
   private static int getAgeInYears(String flightArrivalDate, int duration, Date dob)      
   {
      int ageInYears = 0;

      Date arrivalDateTime;
      try
      {
         SimpleDateFormat simpleDateFormat = new SimpleDateFormat(ValidationConstants.DATE_FORMAT);
         arrivalDateTime = simpleDateFormat.parse(flightArrivalDate);
      }
      catch (ParseException parseException)
      {
         throw new RuntimeException("dateOfBirth or arrivalDate is not correct",
            parseException);
      }

      Calendar dobCalendarObject = Calendar.getInstance();
      if (dob != null)
      {
         // setting dob to calendar object
         dobCalendarObject.setTime(dob);
      }
      Calendar arrivalDateCalendarObject = Calendar.getInstance();
      
      // setting arrival date to calendar object
      arrivalDateCalendarObject.setTime(arrivalDateTime);

      // adding duration to arrival date
      if (duration != 0)
      {
         arrivalDateCalendarObject.add(Calendar.DATE, duration);
      }

      // counting age
      while (true)
      {
         dobCalendarObject.add(Calendar.YEAR, 1);

         if (dobCalendarObject.after(arrivalDateCalendarObject))
         {
            break;
         }
         
         // if dob before arrival date increasing count by one.
         ageInYears++;
      }

      return ageInYears;
   }

}
