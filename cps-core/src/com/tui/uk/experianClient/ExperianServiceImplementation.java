package com.tui.uk.experianClient;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.lang.Long;
import java.lang.Integer;

import javax.xml.soap.SOAPException;

import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;
import com.tui.uk.payment.domain.PaymentStore;

import org.apache.commons.lang.StringUtils;

public class ExperianServiceImplementation {

	public Logger log;

	public ExperianServiceImplementation() {
		log = LogWriter
				.getLogger(ExperianServiceImplementation.class.getName());

	}

	public Map<String, String> branchDataResponseManager(
			Map<String, String> addressDetails) {
		Map<String, String> branchDataResponseManagerMap = new HashMap<String, String>();
		String addressLine1 = null;
		String addressLine2 = null;
		String addressLine3 = null;
		String addressLine4 = null;
		String addressLine5 = null;
		String postOrZipCode = null;
		String institutionName = null;
		String branchName = null;
		String telephoneNumber = null;

		addressLine1 = addressDetails.get("1");
		addressLine2 = addressDetails.get("2");
		addressLine3 = addressDetails.get("3");
		addressLine4 = addressDetails.get("4");
		addressLine5 = addressDetails.get("5");
		postOrZipCode = addressDetails.get("postOrZipCode");
		institutionName = addressDetails.get("institutionName");
		branchName = addressDetails.get("branchName");
		telephoneNumber = addressDetails.get("telephoneNumber");

		branchDataResponseManagerMap.put("addressLine1", addressLine1);
		branchDataResponseManagerMap.put("addressLine2", addressLine2);
		branchDataResponseManagerMap.put("addressLine3", addressLine3);
		branchDataResponseManagerMap.put("addressLine4", addressLine4);
		branchDataResponseManagerMap.put("addressLine5", addressLine5);
		branchDataResponseManagerMap.put("postOrZipCode", postOrZipCode);
		branchDataResponseManagerMap.put("institutionName", institutionName);
		branchDataResponseManagerMap.put("branchName", branchName);
		branchDataResponseManagerMap.put("telephoneNumber", telephoneNumber);

		return branchDataResponseManagerMap;

	}

	public Map<String, String> validateAccountDetails(String countryCode,
			Map<String, String> validateDetails) throws ClassNotFoundException,
			IOException, SOAPException, ExperianServiceException {
		LogWriter.logMethodStart("validateAccountDetails");

		log.logInfoMessage("Parameters for validateAccountDetails are : countryCode"
				+ countryCode + " , " + validateDetails);

		String coditionCodes = ConfReader.getConfEntry("experian.warningCodes",
				"");
		Map<String, String> experianValidationResults = new HashMap<String, String>();
		Map<String, String> validationResults = new HashMap<String, String>();
		ValidateServiceRequestProcessor validateServiceRequestProcessor = new ValidateServiceRequestProcessor(
				validateDetails);
		GetBranchDataRequestProcessor getBranchDataRequestProcessor = null;
		experianValidationResults = validateServiceRequestProcessor
				.getValidationResult();

		Map<String, String> tempMap = new HashMap<String, String>();

		String error = experianValidationResults.get("failure");
		String success = experianValidationResults.get("success");
		if (error != null) {
			log.logInfoMessage("Experian response for account"
					+ validateDetails + "is " + experianValidationResults);
			validationResults.put("validationStatus", "failure");
			validationResults.put("dataAccessKey", "");
			validationResults.put("addressLine1", "");
			validationResults.put("addressLine2", "");
			validationResults.put("addressLine3", "");
			validationResults.put("addressLine4", "");
			validationResults.put("addressLine5", "");
			validationResults.put("postOrZipCode", "");
			validationResults.put("institutionName", "");
			validationResults.put("branchName", "");
			validationResults.put("telephoneNumber", "");

		} else if (success != null) {
			log.logInfoMessage("Experian response for account"
					+ validateDetails + "is " + experianValidationResults);
			validationResults.put("validationStatus", "success");
			validationResults.put("dataAccessKey",
					experianValidationResults.get("dataAccessKey"));
			if (experianValidationResults.get("dataAccessKey") != null) {
				getBranchDataRequestProcessor = new GetBranchDataRequestProcessor();
				Map<String, String> addressDetails = getBranchDataRequestProcessor
						.getBranchData(experianValidationResults
								.get("dataAccessKey"));

				tempMap = branchDataResponseManager(addressDetails);
				for (Map.Entry e : tempMap.entrySet())
					if (!validationResults.containsKey(e.getKey()))
						validationResults.put(e.getKey().toString(), e.getValue() != null ? e.getValue().toString() : "");

			} else {

				validationResults.put("addressLine1", "");
				validationResults.put("addressLine2", "");
				validationResults.put("addressLine3", "");
				validationResults.put("addressLine4", "");
				validationResults.put("addressLine5", "");
				validationResults.put("postOrZipCode", "");
				validationResults.put("institutionName", "");
				validationResults.put("branchName", "");
				validationResults.put("telephoneNumber", "");

			}
		}

		else if (StringUtils.contains(coditionCodes,
				experianValidationResults.get("validationConditionCode"))) {
			String warningCode = experianValidationResults
					.get("validationConditionCode");

			log.logInfoMessage("Experian response for account"
					+ validateDetails + "is" + warningCode);
			validationResults.put("validationStatus", "failure");
			validationResults.put("dataAccessKey", "");
			validationResults.put("addressLine1", "");
			validationResults.put("addressLine2", "");
			validationResults.put("addressLine3", "");
			validationResults.put("addressLine4", "");
			validationResults.put("addressLine5", "");
			validationResults.put("postOrZipCode", "");
			validationResults.put("institutionName", "");
			validationResults.put("branchName", "");
			validationResults.put("telephoneNumber", "");

		} else {
			log.logInfoMessage("Experian response for account "
					+ validateDetails + "is" + experianValidationResults);
			validationResults.put("validationStatus", "success");
			validationResults.put("dataAccessKey",
					experianValidationResults.get("dataAccessKey"));
			if (experianValidationResults.get("dataAccessKey") != null) {
				getBranchDataRequestProcessor = new GetBranchDataRequestProcessor();
				Map<String, String> addressDetails = getBranchDataRequestProcessor
						.getBranchData(experianValidationResults
								.get("dataAccessKey"));

				tempMap = branchDataResponseManager(addressDetails);
				for (Map.Entry e : tempMap.entrySet())
					if (!validationResults.containsKey(e.getKey()))
						validationResults.put(e.getKey().toString(), e.getValue() != null ? e.getValue().toString() : "");

			} else {

				validationResults.put("addressLine1", "");
				validationResults.put("addressLine2", "");
				validationResults.put("addressLine3", "");
				validationResults.put("addressLine4", "");
				validationResults.put("addressLine5", "");
				validationResults.put("postOrZipCode", "");
				validationResults.put("institutionName", "");
				validationResults.put("branchName", "");
				validationResults.put("telephoneNumber", "");

			}
		}

		log.logInfoMessage("cps to hybris/atcom response for" + validateDetails
				+ "is" + experianValidationResults);
		return validationResults;
	}

	public Map<String, String> verifyAccountDetails(String countryCode,
			Map<String, String> VerifyDetails) throws SOAPException,
			IOException, ExperianServiceException {
		log = LogWriter
				.getLogger(ExperianServiceImplementation.class.getName());
		//TODO: need to refactor the old logging code
		/*
		 * log.logInfoMessage("Parameters for verifyAccountDetails are : countryCode = "
		 * + countryCode + " ," + VerifyDetails);
		 */
		//TODO: Refactoring till here.
		
		// implementation for hiding customer personal details in log
		int startInd = 0;
		String accountNumber = "";
		String method = (String) VerifyDetails.get("method");
		String address_refinement_required = (String) VerifyDetails.get("address_refinement_required");
		String merchantreference = (String) VerifyDetails.get("merchantreference");
		String sortCode = (String) VerifyDetails.get("sortCode");
		String accnValue = (String) VerifyDetails.get("accountNumber");
		try {
			accountNumber = maskString(accnValue, startInd, accnValue.length() - 4, '*');
		} catch (Exception e) {
			// TODO: handle exception
			log.logErrorMessage("Error in Masking ", e);
		}
		log.logInfoMessage(
				"Parameters for verifying the account are:  countryCode = " + countryCode + " , method = " + method
						+ " , address_refinement_required = " + address_refinement_required + " , merchantreference = "
						+ merchantreference + " , accountNumber = " + accountNumber + " , sortCode = " + sortCode);
		//====================================================
		
		VerifyServiceProcessor verifyServiceProcessor = new VerifyServiceProcessor(
				VerifyDetails);
		Map<String, String> results = verifyServiceProcessor.getVerifyResult();
		HashMap<String, String> verifyDetails = new HashMap<String, String>();
		String verificationStatus = "true";
		String verifyWarningCodes = ConfReader.getConfEntry(
				"experian.verifyWarningCodes", "");
		String accountVerificationStatus = (String) results
				.get("accountVerificationStatus");
		String bacsCode = (String) results.get("bacsCode");
		String errors = (String) results.get("error");
		String warnings = (String) results.get("warning");
		String errorOutput = new String("");

		String dataAccessKey = (String) results.get("dataAccessKey");
		String personalDetailsScore = (String) results
				.get("personalDetailsScore");
		String addressDetailsScore = (String) results
				.get("addressDetailsScore");
		String reason = "";

		String experianScore = ConfReader.getConfEntry("experian.pascore", "");

		verifyDetails.put("accountVerificationStatus",
				accountVerificationStatus);
		verifyDetails.put("personalDetailsScore", personalDetailsScore);
		verifyDetails.put("addressDetailsScore", addressDetailsScore);

		if (!StringUtils.equalsIgnoreCase("match", accountVerificationStatus)) {
			verificationStatus = "false";
			reason = "Account Matching ,";

		}
		if (bacsCode != null) {
			verificationStatus = "false";
			verifyDetails.put("bacsCode", bacsCode);
			errorOutput += bacsCode;
			reason += "bacsCode , ";

		}
		if (errors != null) {
			verificationStatus = "false";
			verifyDetails.put("errors", errors);
			errorOutput += errors;
			reason += "errors ,";
		}
		if (warnings != null) {
			String warningMessages[] = warnings.split(" ");
			String code = null;

			for (int i = 0; i < warningMessages.length; i++) {
				code = warningMessages[i];
			}

			if (verifyWarningCodes.contains(code)) {
				verificationStatus = "false";
				errorOutput += " " + code;
				reason += "warnings , ";
			}

			verifyDetails.put("warnings", warnings);

		}

		if (personalDetailsScore != null) {

			if (Integer.parseInt(personalDetailsScore) < Integer
					.parseInt(experianScore)) {

				errorOutput += " pds";

				verificationStatus = "false";
				reason += "Personal Detail Score , ";

			}
		}

		if (addressDetailsScore != null) {

			if (Integer.parseInt(addressDetailsScore) < Integer
					.parseInt(experianScore)) {
				errorOutput += " ads";

				verificationStatus = "false";
				reason += "Address Details Score ,";
			}
		}
		if (errors == null)
			verifyDetails.put("dataAccessKey", dataAccessKey);
		if (!StringUtils.isEmpty(errorOutput)) {
			errorOutput = errorOutput.trim();
			errorOutput = errorOutput.replaceAll(" ", ",");
			verifyDetails.put("errors", errorOutput);
		}

		verifyDetails.put("reason", reason);
		verifyDetails.put("verificationStatus", verificationStatus);
		verifyDetails.put("accountSetupDateScore", "-1");

		LogWriter
				.logInfoMessage("Experian Service Response to Hybris/atcore is : "
						+ verifyDetails);
		return verifyDetails;
	}
	
	 /**
     * This method is responsible for masking
     */
    private String maskString(String strText, int start, int end, char maskChar) 
	        throws Exception{
	        
	        if(strText == null || strText.equals(""))
	            return "";
	        
	        if(start < 0)
	            start = 0;
	        
	        if( end > strText.length() )
	            end = strText.length();
	            
	        if(start > end)
	            throw new Exception("End index cannot be greater than start index");
	        
	        int maskLength = end - start;
	        
	        if(maskLength == 0)
	            return strText;
	        
	        StringBuilder sbMaskString = new StringBuilder(maskLength);
	        
	        for(int i = 0; i < maskLength; i++){
	            sbMaskString.append(maskChar);
	        }
	        
	        return strText.substring(0, start) 
	                + sbMaskString.toString() 
	                + strText.substring(start + maskLength);
	    }

}
