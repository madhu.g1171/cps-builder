package com.tui.uk.experianClient;


import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.soap.*;
import com.experianpayments.bankwizard.common.xsd._2009._09.Condition;
import com.experianpayments.bankwizard.common.xsd._2009._09.ConditionSeverity;
import com.experianpayments.bankwizard.common.xsd._2009._09.Conditions;
import com.experianpayments.bankwizard.xsd._2009._07.ValidateResponse;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

public class ValidateServiceResponsePopulator {

	public ValidateServiceResponsePopulator() {

	}

	public Map<String, String> getValidationResponse(SOAPMessage soapMessage) {
		Map<String, String> validateResponseMaP = new HashMap<String, String>();
		Logger logger = LogWriter
				.getLogger(ValidateServiceResponsePopulator.class.getName());
		ValidateResponse validateResponse = null;

		try {
			final Unmarshaller unmarshaller = JAXBContext.newInstance(
					ValidateResponse.class).createUnmarshaller();
			validateResponse = (ValidateResponse) unmarshaller
					.unmarshal(soapMessage.getSOAPBody().getFirstChild());
			soapMessage.saveChanges();

			String dataAccessKey = validateResponse.getDataAccessKey();
			validateResponseMaP = new HashMap<String, String>();

			Conditions condition = validateResponse.getConditions();
			if (condition != null) {
				List<Condition> lscondititon = condition.getCondition();
				Iterator<Condition> iterator = lscondititon.iterator();
				while (iterator.hasNext()) {

					Condition conditionIterator = iterator.next();

					ConditionSeverity cv = ConditionSeverity.ERROR;
					if (conditionIterator.getSeverity().value()
							.equals(cv.value())) {
						validateResponseMaP
								.put("failure",
										"error is reported and validation request has to be rejected");

						return validateResponseMaP;
					}
					validateResponseMaP.put(conditionIterator.getSeverity()
							.value(), conditionIterator.getValue());
					String validationConditionCode = conditionIterator
							.getCode().toString();
					validateResponseMaP.put("validationConditionCode",
							validationConditionCode);
				}
			}
			if (condition == null) {
				validateResponseMaP.put("success", "success");
			}

			validateResponseMaP.put("dataAccessKey", dataAccessKey);

		} catch (JAXBException e) {
		    logger.logErrorMessage(e.getMessage(), e);


		} catch (SOAPException e) {
		    logger.logErrorMessage(e.getMessage(), e);


		}

		return validateResponseMaP;

	}

}
