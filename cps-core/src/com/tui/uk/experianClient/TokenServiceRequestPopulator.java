package com.tui.uk.experianClient;

import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPBodyElement;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;

import org.w3c.dom.CDATASection;
import org.w3c.dom.Document;

import com.experian.uk.wasp.STS;
import com.tui.uk.log.LogWriter;
import com.tui.uk.log.Logger;

/**
 * makes a token request to experian and get a token as String.
 */

public class TokenServiceRequestPopulator {



	/**
	 * creates the request Soap envelope.
	 */

	public static void createSoapEnvelope(final SOAPMessage soapMessage)
			throws SOAPException {
	     Logger logger = LogWriter.getLogger(TokenServiceRequestPopulator.class
	            .getName());;
		SOAPPart soapPart = soapMessage.getSOAPPart();

		String myNamespace = "wasp";
		String myNamespaceURI = "http://www.uk.experian.com/WASP/";

		// SOAP Envelope
		SOAPEnvelope envelope = soapPart.getEnvelope();
		envelope.addNamespaceDeclaration(myNamespace, myNamespaceURI);

		STS reqsts = new com.experian.uk.wasp.ObjectFactory().createSTS();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			doc = factory.newDocumentBuilder().newDocument();
		} catch (ParserConfigurationException e) {
		    logger.logInfoMessage("Error occured in parsing the document builder"+e);
		}
		String dirtext = "<![CDATA[<WASPAuthenticationRequest><ApplicationName>TUI</ApplicationName><AuthenticationLevel>CertificateAuthentication</AuthenticationLevel><AuthenticationParameters/></WASPAuthenticationRequest>]]>";
		CDATASection cdata = doc.createCDATASection(dirtext);
		reqsts.setAuthenticationBlock(cdata.toString());

		SOAPBody soapBody = envelope.getBody();
		QName qname = new QName("http://www.uk.experian.com/WASP/", "STS", "");
		// SOAPElement soapElement = soapBody.addChildElement("STS", "",
		// "http://www.uk.experian.com/WASP/");

		SOAPBodyElement bodyElement = soapBody.addBodyElement(qname);
		SOAPElement element = bodyElement
				.addChildElement("authenticationBlock");
		element.addTextNode(dirtext);

	}

}
