/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BillingAddress.java$
 *
 * $Revision: $
 *
 * $Date: Oct 16, 2010$
 *
 * Author: bibin.j
 *
 * $Log: $
 */
package com.tui.uk.domain;

/**
 * Represents address object.
 *
 * @author bibin.j@sonata-software.com
 */
public final class BillingAddress
{
   /** The street address one.Will hold house no/name. */
   private String streetAddress1;

   /** The street address two.Will hold remaining address details. */
   private String streetAddress2;

   /** The street address three.Will hold town/city. */
   private String streetAddress3;

   /** The street address four.Will hold county. */
   private String streetAddress4;

   /** The selected country. */
   private String country;

   /** The selected country. */
   private String countryCode;

  /**
   * THe constructor.
   *
   * @param streetAddress1 the streetAddress1.
   * @param streetAddress2 the streetAddress2.
   */
   public BillingAddress(String streetAddress1, String streetAddress2)
   {
      this.streetAddress1 = streetAddress1;
      this.streetAddress2 = streetAddress2;
   }

   /**
    * Gets the streetAddress1.
    *
    * @return the streetAddress1
    */
   public String getStreetAddress1()
   {
      return streetAddress1;
   }

   /**
    * Gets the streetAddress2.
    *
    * @return the streetAddress2
    */
   public String getStreetAddress2()
   {
      return streetAddress2;
   }

   /**
    * Gets the streetAddress3.
    *
    * @return the streetAddress3
    */
   public String getStreetAddress3()
   {
      return streetAddress3;
   }

   /**
    * Sets the streetAddress3.
    *
    * @param streetAddress the streetAddress3 to set
    */
   public void setStreetAddress3(String streetAddress)
   {
      streetAddress3 = streetAddress;
   }

   /**
    * Gets the streetAddress4.
    *
    * @return the streetAddress4
    */
   public String getStreetAddress4()
   {
      return streetAddress4;
   }

   /**
    * Sets the streetAddress4.
    *
    * @param streetAddress the streetAddress4 to set
    */
   public void setStreetAddress4(String streetAddress)
   {
      streetAddress4 = streetAddress;
   }

   /**
    * Sets the country.
    *
    * @return the country
    */
   public String getCountry()
   {
      return country;
   }

   /**
    * Gets the country.
    *
    * @param country the country to set
    */
   public void setCountry(String country)
   {
      this.country = country;
   }

   /**
    * Sets the country code.
    *
    * @return the country code
    */
   public String getCountryCode()
   {
      return countryCode;
   }

   /**
    * Gets the country code.
    *
    * @param countryCode the country code
    */
   public void setCountryCode(String countryCode)
   {
      this.countryCode = countryCode;
   }

}
