package com.tui.uk.domain;

import com.tui.uk.client.domain.Money;
import com.tui.uk.domain.PayDayRange;

import java.io.Serializable;
import java.util.Date;

public class DDPaymentSheduleComponent implements Serializable{

	private String paymentScheduleId;

	private Money amount;

	private Money lastAmount;

	private String payCount;

	private Date firstInstallmentMonth;

	private String ddSetUpURL;

	public String getDdSetUpURL() {
        return ddSetUpURL;
    }

    public void setDdSetUpURL(String ddSetUpURL) {
        this.ddSetUpURL = ddSetUpURL;
    }

    public Date getFirstInstallmentMonth() {
		return firstInstallmentMonth;
	}

	public void setFirstInstallmentMonth(Date firstInstallmentMonth) {
		this.firstInstallmentMonth = firstInstallmentMonth;
	}

	private String defaultSchedule;

	private PayDayRange payDayRange;


	public String getPaymentScheduleId() {
		return paymentScheduleId;
	}

	public void setPaymentScheduleId(String paymentScheduleId) {
		this.paymentScheduleId = paymentScheduleId;
	}

	public Money getAmount() {
		return amount;
	}

	public void setAmount(Money amount) {
		this.amount = amount;
	}

	public Money getLastAmount() {
		return lastAmount;
	}

	public void setLastAmount(Money lastAmount) {
		this.lastAmount = lastAmount;
	}

	public String getPayCount() {
		return payCount;
	}

	public void setPayCount(String payCount) {
		this.payCount = payCount;
	}



	public String getDefaultSchedule() {
		return defaultSchedule;
	}

	public void setDefaultSchedule(String defaultSchedule) {
		this.defaultSchedule = defaultSchedule;
	}

	public PayDayRange getPayDayRange() {
		return payDayRange;
	}

	public void setPayDayRange(PayDayRange payDayRange) {
		this.payDayRange = payDayRange;
	}




}
