
package com.experianpayments.bankwizardcard.xsd._2010._09;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardcard.xsd._2012._05.MatchCardHolderResponse;
import com.experianpayments.bankwizardcard.xsd._2012._05.VelocityResponse;
import com.experianpayments.bankwizardcard.xsd._2012._05.VerifyAvsCvvVelocityResponse;


/**
 * <p>Java class for GenericResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenericResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ID" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}guid"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenericResponse", propOrder = {
    "id"
})
@XmlSeeAlso({
    MatchDebitCardDetailsResponse.class,
    ValidateCardDetailsResponse.class,
    VerifyAvsCvvResponse.class,
    VerifyAvsCvvVelocityResponse.class,
    MatchCardHolderResponse.class,
    VelocityResponse.class
})
public abstract class GenericResponse {

    @XmlElement(name = "ID", required = true)
    protected String id;

    /**
     * Gets the value of the id property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getID() {
        return id;
    }

    /**
     * Sets the value of the id property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setID(String value) {
        this.id = value;
    }

}
