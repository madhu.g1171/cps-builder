
package com.experianpayments.bankwizardcard.xsd._2010._09;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * 
 *         The results of the response are contained in the Bits element.
 *         If validation successul, this is OK, otherwise this is set to the parameters that 
 *         failed validation.
 *         * CardType is the type of card, for example, Credit, Charge, Debit and so on.
 *         * SchemeName is the scheme, for example Mastercard, Visa and so on
 *         * IssuerName is the issuer of the card, for example, Santander, RBS and so on.
 *       
 * 
 * <p>Java class for CardValidationResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CardValidationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CardCondition" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}CardCondition" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="CardType" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}TypeOfCard"/>
 *         &lt;element name="SubType" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}SubTypeOfCard"/>
 *         &lt;element name="SchemeName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="IssuerName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CardValidationResponse", propOrder = {
    "cardCondition",
    "cardType",
    "subType",
    "schemeName",
    "issuerName"
})
public class CardValidationResponse {

    @XmlElement(name = "CardCondition")
    protected List<CardCondition> cardCondition;
    @XmlElement(name = "CardType", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected TypeOfCard cardType;
    @XmlElement(name = "SubType", required = true, nillable = true)
    @XmlSchemaType(name = "string")
    protected SubTypeOfCard subType;
    @XmlElement(name = "SchemeName", required = true, nillable = true)
    protected String schemeName;
    @XmlElement(name = "IssuerName", required = true, nillable = true)
    protected String issuerName;

    /**
     * Gets the value of the cardCondition property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cardCondition property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCardCondition().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link CardCondition }
     * 
     * 
     */
    public List<CardCondition> getCardCondition() {
        if (cardCondition == null) {
            cardCondition = new ArrayList<CardCondition>();
        }
        return this.cardCondition;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link TypeOfCard }
     *     
     */
    public TypeOfCard getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeOfCard }
     *     
     */
    public void setCardType(TypeOfCard value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the subType property.
     * 
     * @return
     *     possible object is
     *     {@link SubTypeOfCard }
     *     
     */
    public SubTypeOfCard getSubType() {
        return subType;
    }

    /**
     * Sets the value of the subType property.
     * 
     * @param value
     *     allowed object is
     *     {@link SubTypeOfCard }
     *     
     */
    public void setSubType(SubTypeOfCard value) {
        this.subType = value;
    }

    /**
     * Gets the value of the schemeName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemeName() {
        return schemeName;
    }

    /**
     * Sets the value of the schemeName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemeName(String value) {
        this.schemeName = value;
    }

    /**
     * Gets the value of the issuerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIssuerName() {
        return issuerName;
    }

    /**
     * Sets the value of the issuerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIssuerName(String value) {
        this.issuerName = value;
    }

}
