
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ReportSummaryItemList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReportSummaryItemList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DatablockSummary" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}ReportSummaryItemType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReportSummaryItemList", propOrder = {
    "datablockSummary"
})
public class ReportSummaryItemList {

    @XmlElement(name = "DatablockSummary")
    protected List<ReportSummaryItemType> datablockSummary;

    /**
     * Gets the value of the datablockSummary property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the datablockSummary property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDatablockSummary().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ReportSummaryItemType }
     * 
     * 
     */
    public List<ReportSummaryItemType> getDatablockSummary() {
        if (datablockSummary == null) {
            datablockSummary = new ArrayList<ReportSummaryItemType>();
        }
        return this.datablockSummary;
    }

}
