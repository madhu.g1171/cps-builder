
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResidentialCommercial.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResidentialCommercial">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="R"/>
 *     &lt;enumeration value="C"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ResidentialCommercial")
@XmlEnum
public enum ResidentialCommercial {


    /**
     * Residential
     * 
     */
    R,

    /**
     * Commercial
     * 
     */
    C;

    public String value() {
        return name();
    }

    public static ResidentialCommercial fromValue(String v) {
        return valueOf(v);
    }

}
