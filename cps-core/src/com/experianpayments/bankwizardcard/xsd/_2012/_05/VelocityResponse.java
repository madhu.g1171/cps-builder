
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardcard.xsd._2010._09.GenericCodeMessage;
import com.experianpayments.bankwizardcard.xsd._2010._09.GenericResponse;


/**
 * 
 *         Result of the velocity check.
 *       
 * 
 * <p>Java class for VelocityResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VelocityResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericResponse">
 *       &lt;sequence>
 *         &lt;element name="DecisionMatrix" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DecisionMatrixType" minOccurs="0"/>
 *         &lt;element name="Datablocks" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}Datablocks" minOccurs="0"/>
 *         &lt;element name="ExceptionMessage" type="{http://experianpayments.com/bankwizardcard/xsd/2010/09}GenericCodeMessage" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VelocityResponse", propOrder = {
    "decisionMatrix",
    "datablocks",
    "exceptionMessage"
})
public class VelocityResponse
    extends GenericResponse
{

    @XmlElementRef(name = "DecisionMatrix", namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", type = JAXBElement.class, required = false)
    protected JAXBElement<DecisionMatrixType> decisionMatrix;
    @XmlElementRef(name = "Datablocks", namespace = "http://experianpayments.com/bankwizardcard/xsd/2012/05", type = JAXBElement.class, required = false)
    protected JAXBElement<Datablocks> datablocks;
    @XmlElement(name = "ExceptionMessage")
    protected List<GenericCodeMessage> exceptionMessage;

    /**
     * Gets the value of the decisionMatrix property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link DecisionMatrixType }{@code >}
     *     
     */
    public JAXBElement<DecisionMatrixType> getDecisionMatrix() {
        return decisionMatrix;
    }

    /**
     * Sets the value of the decisionMatrix property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link DecisionMatrixType }{@code >}
     *     
     */
    public void setDecisionMatrix(JAXBElement<DecisionMatrixType> value) {
        this.decisionMatrix = value;
    }

    /**
     * Gets the value of the datablocks property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link Datablocks }{@code >}
     *     
     */
    public JAXBElement<Datablocks> getDatablocks() {
        return datablocks;
    }

    /**
     * Sets the value of the datablocks property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link Datablocks }{@code >}
     *     
     */
    public void setDatablocks(JAXBElement<Datablocks> value) {
        this.datablocks = value;
    }

    /**
     * Gets the value of the exceptionMessage property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the exceptionMessage property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExceptionMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link GenericCodeMessage }
     * 
     * 
     */
    public List<GenericCodeMessage> getExceptionMessage() {
        if (exceptionMessage == null) {
            exceptionMessage = new ArrayList<GenericCodeMessage>();
        }
        return this.exceptionMessage;
    }

}
