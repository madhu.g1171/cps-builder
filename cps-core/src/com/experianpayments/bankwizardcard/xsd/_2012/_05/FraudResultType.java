
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FraudResultType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FraudResultType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Summary" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DatablockSummaryType" minOccurs="0"/>
 *         &lt;element name="FraudRecord" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}FraudRecordType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attGroup ref="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DataBlockResultTypeGroup"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FraudResultType", propOrder = {
    "summary",
    "fraudRecord"
})
public class FraudResultType {

    @XmlElement(name = "Summary")
    protected DatablockSummaryType summary;
    @XmlElement(name = "FraudRecord")
    protected List<FraudRecordType> fraudRecord;
    @XmlAttribute(name = "Type")
    protected String type;

    /**
     * Gets the value of the summary property.
     * 
     * @return
     *     possible object is
     *     {@link DatablockSummaryType }
     *     
     */
    public DatablockSummaryType getSummary() {
        return summary;
    }

    /**
     * Sets the value of the summary property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatablockSummaryType }
     *     
     */
    public void setSummary(DatablockSummaryType value) {
        this.summary = value;
    }

    /**
     * Gets the value of the fraudRecord property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fraudRecord property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFraudRecord().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FraudRecordType }
     * 
     * 
     */
    public List<FraudRecordType> getFraudRecord() {
        if (fraudRecord == null) {
            fraudRecord = new ArrayList<FraudRecordType>();
        }
        return this.fraudRecord;
    }

    /**
     * Gets the value of the type property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getType() {
        return type;
    }

    /**
     * Sets the value of the type property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setType(String value) {
        this.type = value;
    }

}
