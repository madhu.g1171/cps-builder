
package com.experianpayments.bankwizardcard.xsd._2012._05;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DecisionReasonList complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DecisionReasonList">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DecisionReason" type="{http://experianpayments.com/bankwizardcard/xsd/2012/05}DecisionReasonType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="tmp" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DecisionReasonList", propOrder = {
    "decisionReason"
})
public class DecisionReasonList {

    @XmlElement(name = "DecisionReason")
    protected List<DecisionReasonType> decisionReason;
    @XmlAttribute(name = "tmp")
    protected String tmp;

    /**
     * Gets the value of the decisionReason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the decisionReason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDecisionReason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DecisionReasonType }
     * 
     * 
     */
    public List<DecisionReasonType> getDecisionReason() {
        if (decisionReason == null) {
            decisionReason = new ArrayList<DecisionReasonType>();
        }
        return this.decisionReason;
    }

    /**
     * Gets the value of the tmp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTmp() {
        return tmp;
    }

    /**
     * Sets the value of the tmp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTmp(String value) {
        this.tmp = value;
    }

}
