
package com.experianpayments.bankwizardcard.xsd._2012._05;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResidentialStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResidentialStatus">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="M"/>
 *     &lt;enumeration value="T"/>
 *     &lt;enumeration value="H"/>
 *     &lt;enumeration value="L"/>
 *     &lt;enumeration value="O"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "ResidentialStatus")
@XmlEnum
public enum ResidentialStatus {


    /**
     * Mortgage
     * 
     */
    M,

    /**
     * Tenant
     * 
     */
    T,

    /**
     * Homeowner
     * 
     */
    H,

    /**
     * Living with partner
     * 
     */
    L,

    /**
     * Other
     * 
     */
    O;

    public String value() {
        return name();
    }

    public static ResidentialStatus fromValue(String v) {
        return valueOf(v);
    }

}
