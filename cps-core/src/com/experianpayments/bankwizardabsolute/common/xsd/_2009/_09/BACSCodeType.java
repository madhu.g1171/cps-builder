
package com.experianpayments.bankwizardabsolute.common.xsd._2009._09;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         BACS code element.
 *       
 * 
 * <p>Java class for BACSCodeType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BACSCodeType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09>BACSdescription">
 *       &lt;attribute name="code" use="required" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}BACScode" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BACSCodeType", propOrder = {
    "value"
})
public class BACSCodeType {

    @XmlValue
    protected BACSdescription value;
    @XmlAttribute(name = "code", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String code;

    /**
     * 
     *         BACS code descriptions.
     *       
     * 
     * @return
     *     possible object is
     *     {@link BACSdescription }
     *     
     */
    public BACSdescription getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link BACSdescription }
     *     
     */
    public void setValue(BACSdescription value) {
        this.value = value;
    }

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

}
