
package com.experianpayments.bankwizard.wsdl.bankwizardservice_v1_1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.experianpayments.bankwizard.xsd._2009._07.DataAccessKeyType;
import com.experianpayments.bankwizard.xsd._2011._04.AccountInformation;
import com.experianpayments.bankwizard.xsd._2011._04.BranchSearch;
import com.experianpayments.bankwizard.xsd._2011._04.BranchSearchResults;
import com.experianpayments.bankwizard.xsd._2011._04.GetInputWithSearchRequest;
import com.experianpayments.bankwizard.xsd._2011._04.GetInputWithSearchResponse;
import com.experianpayments.bankwizard.xsd._2011._04.GetLookupIbanInputRequest;
import com.experianpayments.bankwizard.xsd._2011._04.GetLookupIbanInputResponse;
import com.experianpayments.bankwizard.xsd._2011._04.GlobalSearchRequest;
import com.experianpayments.bankwizard.xsd._2011._04.GlobalSearchResponse;
import com.experianpayments.bankwizard.xsd._2011._04.LookupIbanRequest;
import com.experianpayments.bankwizard.xsd._2011._04.LookupIbanResponse;
import com.experianpayments.bankwizard.xsd._2011._04.SwiftSearch;
import com.experianpayments.bankwizard.xsd._2011._04.SwiftSearchResults;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.experianpayments.bankwizard.wsdl.bankwizardservice_v1_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SwiftSearchResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "SwiftSearchResponse");
    private final static QName _GetLookupIbanInputResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "GetLookupIbanInputResponse");
    private final static QName _GetInputWithSearchRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "GetInputWithSearchRequest");
    private final static QName _GetLookupIbanInputRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "GetLookupIbanInputRequest");
    private final static QName _LookupIbanRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "LookupIbanRequest");
    private final static QName _BranchSearchResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "BranchSearchResponse");
    private final static QName _GlobalSearchResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "GlobalSearchResponse");
    private final static QName _DataAccessKey_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "DataAccessKey");
    private final static QName _GetInputWithSearchResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "GetInputWithSearchResponse");
    private final static QName _SwiftSearchRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "SwiftSearchRequest");
    private final static QName _LookupIbanResponse_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "LookupIbanResponse");
    private final static QName _AccountInformation_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "AccountInformation");
    private final static QName _BranchSearchRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "BranchSearchRequest");
    private final static QName _GlobalSearchRequest_QNAME = new QName("http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", "GlobalSearchRequest");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.experianpayments.bankwizard.wsdl.bankwizardservice_v1_1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SwiftSearchResults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "SwiftSearchResponse")
    public JAXBElement<SwiftSearchResults> createSwiftSearchResponse(SwiftSearchResults value) {
        return new JAXBElement<SwiftSearchResults>(_SwiftSearchResponse_QNAME, SwiftSearchResults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLookupIbanInputResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "GetLookupIbanInputResponse")
    public JAXBElement<GetLookupIbanInputResponse> createGetLookupIbanInputResponse(GetLookupIbanInputResponse value) {
        return new JAXBElement<GetLookupIbanInputResponse>(_GetLookupIbanInputResponse_QNAME, GetLookupIbanInputResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInputWithSearchRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "GetInputWithSearchRequest")
    public JAXBElement<GetInputWithSearchRequest> createGetInputWithSearchRequest(GetInputWithSearchRequest value) {
        return new JAXBElement<GetInputWithSearchRequest>(_GetInputWithSearchRequest_QNAME, GetInputWithSearchRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetLookupIbanInputRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "GetLookupIbanInputRequest")
    public JAXBElement<GetLookupIbanInputRequest> createGetLookupIbanInputRequest(GetLookupIbanInputRequest value) {
        return new JAXBElement<GetLookupIbanInputRequest>(_GetLookupIbanInputRequest_QNAME, GetLookupIbanInputRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupIbanRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "LookupIbanRequest")
    public JAXBElement<LookupIbanRequest> createLookupIbanRequest(LookupIbanRequest value) {
        return new JAXBElement<LookupIbanRequest>(_LookupIbanRequest_QNAME, LookupIbanRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BranchSearchResults }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "BranchSearchResponse")
    public JAXBElement<BranchSearchResults> createBranchSearchResponse(BranchSearchResults value) {
        return new JAXBElement<BranchSearchResults>(_BranchSearchResponse_QNAME, BranchSearchResults.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlobalSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "GlobalSearchResponse")
    public JAXBElement<GlobalSearchResponse> createGlobalSearchResponse(GlobalSearchResponse value) {
        return new JAXBElement<GlobalSearchResponse>(_GlobalSearchResponse_QNAME, GlobalSearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataAccessKeyType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "DataAccessKey")
    public JAXBElement<DataAccessKeyType> createDataAccessKey(DataAccessKeyType value) {
        return new JAXBElement<DataAccessKeyType>(_DataAccessKey_QNAME, DataAccessKeyType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetInputWithSearchResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "GetInputWithSearchResponse")
    public JAXBElement<GetInputWithSearchResponse> createGetInputWithSearchResponse(GetInputWithSearchResponse value) {
        return new JAXBElement<GetInputWithSearchResponse>(_GetInputWithSearchResponse_QNAME, GetInputWithSearchResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SwiftSearch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "SwiftSearchRequest")
    public JAXBElement<SwiftSearch> createSwiftSearchRequest(SwiftSearch value) {
        return new JAXBElement<SwiftSearch>(_SwiftSearchRequest_QNAME, SwiftSearch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupIbanResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "LookupIbanResponse")
    public JAXBElement<LookupIbanResponse> createLookupIbanResponse(LookupIbanResponse value) {
        return new JAXBElement<LookupIbanResponse>(_LookupIbanResponse_QNAME, LookupIbanResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "AccountInformation")
    public JAXBElement<AccountInformation> createAccountInformation(AccountInformation value) {
        return new JAXBElement<AccountInformation>(_AccountInformation_QNAME, AccountInformation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BranchSearch }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "BranchSearchRequest")
    public JAXBElement<BranchSearch> createBranchSearchRequest(BranchSearch value) {
        return new JAXBElement<BranchSearch>(_BranchSearchRequest_QNAME, BranchSearch.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GlobalSearchRequest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://experianpayments.com/bankwizard/wsdl/BankWizardService-v1-1", name = "GlobalSearchRequest")
    public JAXBElement<GlobalSearchRequest> createGlobalSearchRequest(GlobalSearchRequest value) {
        return new JAXBElement<GlobalSearchRequest>(_GlobalSearchRequest_QNAME, GlobalSearchRequest.class, null, value);
    }

}
