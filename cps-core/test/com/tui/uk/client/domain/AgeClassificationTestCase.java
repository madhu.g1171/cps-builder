/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: AgeClassificationTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-07-17 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import junit.framework.TestCase;

/**
 * This Class tests the AgeClassification class.
 */
public class AgeClassificationTestCase extends TestCase
{

   /**
    * Test for age classification for the code passed .
    */
   public void testValidAgeClassificationCode()
   {

      try
      {
         AgeClassification.findByCode("INF");
      }
      catch (IllegalArgumentException dse)
      {
         fail("Unexpected exception while validating" + dse.getMessage());
      }
   }

   /**
    * Test for age classification for the code passed .
    */
   public void testInvalidAgeClassificationCode()
   {
      try
      {
         AgeClassification.findByCode("ABC");
         fail("Exception while validating the code");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successfull.", true);
      }
   }

}
