/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: ClientURLComponentTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2010-08-02 $
 *
 * $Author: roopesh.s $
 *
 * $Log: $
 */
package com.tui.uk.client.domain;

import junit.framework.TestCase;

/**
 * This Class tests the ClientURLComponent class.
 */
public class ClientURLComponentTestCase extends TestCase
{

   /** To test the setter and getter method. */
   public void testClientURLComponent()
   {
      ClientURLComponent clientURLComponent = new ClientURLComponent();
      clientURLComponent.setLogOutURL("logOutURL");
      clientURLComponent.setHomePageURL("homePageURL");
      clientURLComponent.setSearchResultsURL("searchResultsURL");
      clientURLComponent.setAccomDetailsURL("accomDetailsURL");
      clientURLComponent.setFlightDetailsURL("flightDetailsURL");
      clientURLComponent.setHolidayOptionsURL("holidayOptionsURL");

      assertEquals(clientURLComponent.getLogOutURL(), "logOutURL");
      assertEquals(clientURLComponent.getHomePageURL(), "homePageURL");
      assertEquals(clientURLComponent.getSearchResultsURL(), "searchResultsURL");
      assertEquals(clientURLComponent.getAccomDetailsURL(), "accomDetailsURL");
      assertEquals(clientURLComponent.getFlightDetailsURL(), "flightDetailsURL");
      assertEquals(clientURLComponent.getHolidayOptionsURL(), "holidayOptionsURL");

   }

}
