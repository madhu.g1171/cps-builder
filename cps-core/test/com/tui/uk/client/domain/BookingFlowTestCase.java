/*
* Copyright (C)2006 TUI UK Ltd
*
* TUI UK Ltd,
* Columbus House,
* Westwood Way,
* Westwood Business Park,
* Coventry,
* United Kingdom
* CV4 8TT
*
* Telephone - (024)76282828
*
* All rights reserved - The copyright notice above does not evidence
* any actual or intended publication of this source code.
*
* $RCSfile:   BookingFlowTestCase.java$
*
* $Revision:   $
*
* $Date:   Dec 22, 2009$
*
* Author: veena.bn
*
*
* $Log:   $
*/
package com.tui.uk.client.domain;

import junit.framework.TestCase;

/**
 * Testcase for BookingFlow enumeration.
 * @author veena.bn
 *
 */
public class BookingFlowTestCase extends TestCase
{
   /**
    * Test for valid bookingFlow.
    */
   public void testValidBookingFlow()
   {
      try
      {
         BookingFlow.findByBookFlowCode("CANCEL");
      }
      catch (IllegalArgumentException dse)
      {
         fail("Unexpected exception while validating the bookingFlow" + dse.getMessage());
      }

   }

   /**
    * Test for invalid bookingFlow.
    */
   public void testInvalidBookingFlow()
   {
      try
      {
         BookingFlow.findByBookFlowCode("ABC");
      }
      catch (IllegalArgumentException dse)
      {
         assertTrue("Testcase was successful.", true);
      }

   }
}
