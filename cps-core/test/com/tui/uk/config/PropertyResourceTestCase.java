/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Jul 27, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.config;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import junit.framework.TestCase;

/**
 * This class tests PropertyConstants class constructor.
 *
 * @author sindhushree.g
 *
 */
public class PropertyResourceTestCase extends TestCase
{

   /**
    * Test case to check if constructor is called.
    */
   @SuppressWarnings("unchecked")
   public void testConstructor()
   {
      try
      {
         Constructor[] cons = PropertyResource.class.getDeclaredConstructors();
         cons[0].setAccessible(true);
         assertNotNull((PropertyResource) cons[0].newInstance());
      }
      catch (IllegalAccessException iae)
      {
         fail(iae.getMessage());
      }
      catch (InvocationTargetException ite)
      {
         fail(ite.getMessage());
      }
      catch (InstantiationException ise)
      {
         fail(ise.getMessage());
      }
   }

}
