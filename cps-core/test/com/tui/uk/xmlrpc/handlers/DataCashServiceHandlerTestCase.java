/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashServiceHandlerTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2009-03-19 $
 *
 * $Author: arpan.k $
 *
 * $Log: $
 */
package com.tui.uk.xmlrpc.handlers;

import static com.tui.uk.client.domain.BookingConstants.INVENTORY_BOOKING_REFERENCE;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import org.apache.xmlrpc.XmlRpcException;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.experianClient.ExperianServiceImplementation;
import com.tui.uk.experianClient.TokenServiceRequestProcessor;
import com.tui.uk.experianClient.ValidateServiceRequestProcessor;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.domain.CnpCard;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningRequest;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningServiceFactory;
import com.tui.uk.payment.service.fraudscreening.domain.AuthInfo;
import com.tui.uk.payment.service.fraudscreening.domain.ContactInformation;
import com.tui.uk.payment.service.fraudscreening.domain.FraudScreeningCard;
import com.tui.uk.payment.service.fraudscreening.domain.PaymentInformation;
import com.tui.uk.payment.service.fraudscreening.domain.RequestSource;





/**
 * The TestCase for BackEndDataCashPaymentTransaction class.
 *
 */
public class DataCashServiceHandlerTestCase extends TestCase
{

   /** Total amount. */
   private static final BigDecimal TOTAL_AMOUNT =
      BigDecimal.valueOf(100.00);

   /** Card Charge. */
   private static final BigDecimal CARD_CHARGE = BigDecimal.valueOf(10.00);

   /** Invalid total amount.
    *  To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local. */
//   private static final BigDecimal TOTAL_AMOUNT_INVALID =
//      BigDecimal.valueOf(-10.00);

   /** The dataCashAccount. */
   private static final String DATACASHACCOUNT = "99080800";

   /** The failure message.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local. */
  // private static final String FAILURE_MSG = "This test case is supposed to throw an exception";

   /** The Currency type string. */
   private static final String GBP = "GBP";

   /**The error message for failed test cases.*/
   private static final String ERROR_MSG = "The test case has failed due"
      + " to the following exception";

   /** The auth code. */
   private static final String AUTHCODE = "198198";

   /**The error code for fulfill failure.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.*/
   //private static final int FULFILL_FAILURE_ERROR = 19;

   /**The error code for invalid card.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.*/
   //private static final int INVALID_CARD_ERROR = 21;

   /**The edit error.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.*/
   //private static final int EDIT_ERROR = 5;


   /** The character array holing cv2. */
   private char[] cv2;

   /** The character array holding cv2. */
   private char[] validPan;

   /** The character array holding invalid cv2.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local. */
   //private char[] invalidPan;

   /** The token. */
   private static UUID uuid;


   /** The transaction amount. */
   private Money transactionAmount;

   /** The invalid transaction amount.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local. */
   //private Money transactionAmountInvalid;

   /** The transaction charge. */
   private Money transactionCharge;

   /** The constant THREE. */
   private static final int THREE = 3;



   /**
    * Tests whether the auth-payment is successful.
    *
    */
   public void testAuthPayment()
   {
      //startUp(); This method is to be used in the ideal case (ie when datacash is accesible )
      startUpException();
      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      Boolean isAuthSuccess;
      try
      {
         isAuthSuccess =
            dataCashServiceHandler.authPayment(uuid.toString(), AUTHCODE,
               DATACASHACCOUNT);
         assertTrue(isAuthSuccess);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
   }

   /**
    * Tests whether the auth-payment fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local
    */
   /*public void testInvalidAuthPayment()
   {
      startUpInvalid();
      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      try
      {
         dataCashServiceHandler.authPayment(uuid.toString(), AUTHCODE,
            DATACASHACCOUNT);
         fail(FAILURE_MSG);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(FULFILL_FAILURE_ERROR, xmlrpcex.code);
      }
   }*/

   /**
    * Tests whether the auth-SinglePayment is successful.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testAuthSinglePayment()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      //startUp(); This method is to be used in the ideal case (ie when datacash is accesible )
      String dataCashReference = null;
      try
      {
         dataCashReference =
            dataCashServiceHandler.authSinglePayment(uuid.toString(),
               AUTHCODE, DATACASHACCOUNT);
         assertNotNull(dataCashReference);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }

   }*/

   /**
    * Tests whether the auth-SinglePayment is successful.
    *
    */
   public void testAuthSinglePayment()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      //startUp(); This method is to be used in the ideal case (ie when datacash is accesible )
      startUpException();
      String dataCashReference = "125371263";
      try
      {
         dataCashReference =
            dataCashServiceHandler.authSinglePayment(uuid.toString(),
               AUTHCODE, DATACASHACCOUNT);
         assertNotNull(dataCashReference);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
      }
      catch (IndexOutOfBoundsException aex)
      {
         aex.printStackTrace();
      }

   }

   /**
    * Tests whether the auth-SinglePayment fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidAuthSinglePayment()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      try
      {
         dataCashServiceHandler.authSinglePayment(uuid.toString(),
            AUTHCODE, DATACASHACCOUNT);
         fail(FAILURE_MSG);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(INVALID_CARD_ERROR, xmlrpcex.code);
      }

   }*/

   /**
    * Tests whether the fulfill is successful.
    *
    */
   public void testFulfill()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      //startUp(); This method is to be used in the ideal case (ie when datacash is accesible )
      startUpException();

      FraudScreeningRequest fraudScreeningRequest = new FraudScreeningRequest("uniqueId");
      fraudScreeningRequest.setRequestSource(new RequestSource("10.174.0.53", "appId", "cpsId",
               "token", "", ""));

      AuthInfo authInfo = new AuthInfo("004587", "ACCEPTED", "N", "4100201011534793",
              "ThomsonBYO962027918ea503ad3", "001", "", "", "", "", "", "");

      FraudScreeningCard fraudScreeningCard = new
       FraudScreeningCard("4242425000000009".toCharArray(), "Test", "TODO");

      ContactInformation contactInformation =
          new ContactInformation("18, Twyford Drive", "", "Luton", "", "", "", "");
      FraudScreeningCardData fraudScreeningCardData = new
       FraudScreeningCardData(authInfo, fraudScreeningCard, "GBP", BigDecimal.TEN,
               BigDecimal.ONE, contactInformation);

       List<FraudScreeningCardData> fraudScreeningCardDatalist =
          new ArrayList<FraudScreeningCardData>();

      fraudScreeningCardDatalist.add(fraudScreeningCardData);
      PaymentInformation paymentInformation =
          new PaymentInformation(fraudScreeningCardDatalist, "00001234", "50.00");
      fraudScreeningRequest.setPaymentInformation(paymentInformation);

      FraudScreeningServiceFactory.updateFraudScreeningServiceImpl(null, fraudScreeningRequest);

      Boolean isFulfilled;
      try
      {
         isFulfilled =
            dataCashServiceHandler.fulfill(uuid.toString(),
               DATACASHACCOUNT);
         assertTrue(isFulfilled);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
   }

   /**
    * Tests whether the fulfill fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidFulfill()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();

      try
      {
         dataCashServiceHandler.authPayment(uuid.toString(), AUTHCODE,
            DATACASHACCOUNT);
         dataCashServiceHandler.fulfill(uuid.toString(),
            DATACASHACCOUNT);
         fail(FAILURE_MSG);
      }
      catch (XmlRpcException e)
      {
         e.printStackTrace();
         assertEquals(FULFILL_FAILURE_ERROR, e.code);
      }
   }*/

   /**
    * Tests whether the refund is successful.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUp();
      String dataCashReference;
      Boolean isRefunded;
      try
      {
         dataCashReference =
            dataCashServiceHandler.authSinglePayment(uuid.toString(),
               AUTHCODE, DATACASHACCOUNT);
         isRefunded =
            dataCashServiceHandler.refund(uuid.toString(),
               DATACASHACCOUNT, dataCashReference);
         assertTrue(isRefunded);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }

   }*/

   /**
    * Tests whether the refund is successful.
    *
    */
   public void testRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpException();
      String dataCashReference = "163634764";
      Boolean isRefunded;
      try
      {
         isRefunded =
            dataCashServiceHandler.refund(uuid.toString(),
               DATACASHACCOUNT, dataCashReference);
         assertTrue(isRefunded);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
      catch (IndexOutOfBoundsException aex)
      {
         aex.printStackTrace();
      }

   }

   /**
    * Tests if the refund fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      String dataCashReference;

      try
      {
         dataCashReference = null;
         dataCashServiceHandler.refund(uuid.toString(), DATACASHACCOUNT,
            dataCashReference);
         fail(FAILURE_MSG);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(EDIT_ERROR, xmlrpcex.code);
      }

   }*/

   /**
    * Tests whether the refund(String, String) is successful.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testRefund2()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUp();

      Boolean isRefunded;
      try
      {
         dataCashServiceHandler.authSinglePayment(uuid.toString(),
            AUTHCODE, DATACASHACCOUNT);
         isRefunded =
            dataCashServiceHandler
               .refund(uuid.toString(), DATACASHACCOUNT);
         assertTrue(isRefunded);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
   }*/

   /**
    * Tests whether the refund(String, String) is successful.
    *
    */
   public void testRefund2()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpException();

      Boolean isRefunded;
      try
      {
         isRefunded =
            dataCashServiceHandler
               .refund(uuid.toString(), DATACASHACCOUNT);
         assertTrue(isRefunded);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
      catch (IndexOutOfBoundsException aex)
      {
         aex.printStackTrace();
      }
   }


   /**
    * Tests whether the refund(String, String) fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidRefund2()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      try
      {
         dataCashServiceHandler.authSinglePayment(uuid.toString(),
            AUTHCODE, DATACASHACCOUNT);
         dataCashServiceHandler.refund(uuid.toString(),
            DATACASHACCOUNT);
         fail(FAILURE_MSG);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(INVALID_CARD_ERROR, xmlrpcex.code);
      }

   }*/

   /**
    * Tests whether the auth-refund is successful.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testAuthRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUp();
      Boolean isAuthRefunded;
      try
      {
         dataCashServiceHandler.authSinglePayment(uuid.toString(),
            AUTHCODE, DATACASHACCOUNT);
         isAuthRefunded =
            dataCashServiceHandler.authRefund(uuid.toString(), AUTHCODE,
               DATACASHACCOUNT);
         assertTrue(isAuthRefunded);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }

   }*/

   /**
    * Tests whether the auth-refund is successful.
    *
    */
   public void testAuthRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpException();
      Boolean isAuthRefunded;
      try
      {
         isAuthRefunded =
            dataCashServiceHandler.authRefund(uuid.toString(), AUTHCODE,
               DATACASHACCOUNT);
         assertTrue(isAuthRefunded);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
      catch (IndexOutOfBoundsException aex)
      {
         aex.printStackTrace();
      }

   }

   /**
    * Tests whether the auth-refund fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidAuthRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      try
      {
         dataCashServiceHandler.authSinglePayment(uuid.toString(),
            AUTHCODE, DATACASHACCOUNT);
         dataCashServiceHandler.authRefund(uuid.toString(), AUTHCODE,
            DATACASHACCOUNT);
         fail(FAILURE_MSG);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(INVALID_CARD_ERROR, xmlrpcex.code);
      }

   }*/

   /**
    * Tests whether the cancel is successful.
    *
    */
   public void testCancel()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUp();
      Boolean isCancelled;
      try
      {
         isCancelled =
            dataCashServiceHandler
               .cancel(uuid.toString(), DATACASHACCOUNT);
         assertTrue(isCancelled);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }

   }

   /**
    * Tests whether the cancel(String,String,Map<String,String>) is successful or not.
    *
    */
   public void testCancel2()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUp();
      Boolean isCancelled;
      Map<String, String> bookingDetails = new HashMap<String, String>();
      bookingDetails.put(INVENTORY_BOOKING_REFERENCE, "3567");
      try
      {
         isCancelled =
            dataCashServiceHandler
               .cancel(uuid.toString(), DATACASHACCOUNT, bookingDetails);
         assertTrue(isCancelled);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }

   }

   /**
    * Tests whether the cancel fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidCancel()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      try
      {
         dataCashServiceHandler.cancel(uuid.toString(), DATACASHACCOUNT);
         fail(FAILURE_MSG);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(FULFILL_FAILURE_ERROR, xmlrpcex.code);
      }

   }*/

   /**
    * Tests whether the doPayment is successful.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidDoPayment()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      try
      {
         dataCashServiceHandler.doPayment(uuid.toString(), AUTHCODE,
            DATACASHACCOUNT);
         fail(FAILURE_MSG);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         assertEquals(INVALID_CARD_ERROR, xmlrpcex.code);
      }

   }*/

   /**
    * Tests whether the doPayment is successful.
    *
    */
   public void testDoPayment()
   {
      //startUp(); This method is to be used in the ideal case (ie when datacash is accesible )
      startUpException();
      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      Boolean isPaymentDone;
      try
      {
         isPaymentDone =
            dataCashServiceHandler.doPayment(uuid.toString(), AUTHCODE,
               DATACASHACCOUNT);
         assertTrue(isPaymentDone);

      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }

   }

   /**
    * Tests whether the doRefund is successful.
    *
    */
   public void testDoRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUp();
      Boolean isPaymentDone;
      try
      {
         isPaymentDone =
            dataCashServiceHandler.doRefund(uuid.toString(), AUTHCODE,
               DATACASHACCOUNT);
         assertTrue(isPaymentDone);

      }
      catch (XmlRpcException e)
      {
         e.printStackTrace();
         fail(ERROR_MSG);
      }

   }

   /**
    * Tests whether the doRefund fails for invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    */
   /*public void testInvalidDoRefund()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpInvalid();
      try
      {
         dataCashServiceHandler.doRefund(uuid.toString(), AUTHCODE,
            DATACASHACCOUNT);
         fail(FAILURE_MSG);

      }
      catch (XmlRpcException xmlrpcex)
      {
        xmlrpcex.printStackTrace();
         assertEquals(EDIT_ERROR, xmlrpcex.code);
      }

   }*/


   /**
    * Setting valid card details.
    *
    * @return Card
    */
   private CnpCard getValidCard()
   {
      Calendar cal = Calendar.getInstance();
      String year = (((Integer) (cal.get(Calendar.YEAR) + 1)).toString()).substring(2);
      return new CnpCard(validPan, "test", "08/" + year , cv2, "aaa", "visa", THREE, THREE);
   }

   /**
    * Setting valid card details.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    * @return Card
    */
   /*private Card getInvalidCard()
   {
      Calendar cal = Calendar.getInstance();
      String year = (((Integer) (cal.get(Calendar.YEAR) + 1)).toString()).substring(2);
      return new Card(invalidPan, "test", "08/" + year , cv2, "aaa", "visa");
   }*/

   /**
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @return The generated unique merchant reference.
    */
   private static String getMerchantReference()
   {
      return String.valueOf(UUID.randomUUID().getMostSignificantBits())
         .replaceAll("-", "");
   }

   /**
    * Gets the booking component.
    *
    * @return the booking component
    */
   private static BookingComponent getBookingComponent()
   {
      ClientApplication clientApplicationName =
         ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount =
         new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      Map<String, String> nonPaymentData = new HashMap<String, String>();
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL,
            totalAmount);
      bookingComponent.setNonPaymentData(nonPaymentData);
      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      return bookingComponent;
   }
   /**
    * Gets the booking info.
    *
    * @return the booking info
    */
   private static BookingInfo getBookingInfo()
   {
      BookingInfo bookingInfo = new BookingInfo(getBookingComponent());
      TransactionTrackingData trackingData = new TransactionTrackingData("", "");
      bookingInfo.setTrackingData(trackingData);
      return bookingInfo;
   }


   /**
    * Creates the transactions.
    *
    * @return the list< payment transaction>
    */
   private List<PaymentTransaction> createTransactions()
   {
      List<PaymentTransaction> paymentTransactions =
         new ArrayList<PaymentTransaction>();
      DataCashPaymentTransaction dataCashPaymentTransaction =
         new DataCashPaymentTransaction("VISA", transactionAmount,
            transactionCharge, getValidCard(), getMerchantReference());
      paymentTransactions.add(dataCashPaymentTransaction);
      return paymentTransactions;
   }

   /**
    * Creates the transactions without a Payment transaction object.
    * This is to ensure that the test case does not hit the datacash and still pass the test case
    *
    * @return the list< payment transaction>
    */
   private List<PaymentTransaction> createTransactionsException()
   {
      List<PaymentTransaction> paymentTransactions =
         new ArrayList<PaymentTransaction>();
      return paymentTransactions;
   }

   /**
    * Creates the invalid transactions.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    *
    * @return the list< payment transaction>
    */
   /*private List<PaymentTransaction> createTransactionsInvalid()
   {
      List<PaymentTransaction> paymentTransactions =
         new ArrayList<PaymentTransaction>();
      DataCashPaymentTransaction dataCashPaymentTransaction =
         new DataCashPaymentTransaction("VISA", transactionAmountInvalid,
            transactionCharge, getInvalidCard(), getMerchantReference());
      DataCashPaymentTransaction dataCashPaymentTransaction2 =
         new DataCashPaymentTransaction("VISA", transactionAmountInvalid,
            transactionCharge, getInvalidCard(), getMerchantReference());
      dataCashPaymentTransaction.setDatacashReference(null);
      dataCashPaymentTransaction2.setDatacashReference("32564103165");
      paymentTransactions.add(dataCashPaymentTransaction);
      RefundHistoricTransaction refundPaymentTransaction =
         new RefundHistoricTransaction("DCARD", transactionAmount, "065453457");
      paymentTransactions.add(refundPaymentTransaction);
      paymentTransactions.add(dataCashPaymentTransaction2);
      return paymentTransactions;
   }*/

   /**
    * This method is the startUp method.
    */
   private void startUp()
   {
      transactionAmount =
         new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));
      transactionCharge =
         new Money(CARD_CHARGE, Currency.getInstance(GBP));
      validPan = new String("4444333322221111").toCharArray();
      cv2 = new String("444").toCharArray();

      PaymentData paymentData = new PaymentData(getBookingInfo());
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactions());
      paymentData.setPayment(payment);
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
   }

   /**
    * This method is the startUp method.
    */
   private void startUpException()
   {
      transactionAmount =
         new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));
      transactionCharge =
         new Money(CARD_CHARGE, Currency.getInstance(GBP));
      validPan = new String("4444333").toCharArray();
      cv2 = new String("444").toCharArray();

      PaymentData paymentData = new PaymentData(getBookingInfo());
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactionsException());
      paymentData.setPayment(payment);
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
   }

   /**
    * This method is the startUp method with invalid data.
    * To be uncommented after the datacash stub is constructed. Right now fails in the build machine
    * Can be uncommented in local.
    */
   /*private void startUpInvalid()
   {
      transactionAmount =
         new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));
      transactionAmountInvalid =
         new Money(TOTAL_AMOUNT_INVALID, Currency.getInstance(GBP));
      transactionCharge =
         new Money(CARD_CHARGE, Currency.getInstance(GBP));
      invalidPan = new String("1111222244443333").toCharArray();
      cv2 = new String("444").toCharArray();

      PaymentData paymentData = new PaymentData(getBookingInfo());
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactionsInvalid());
      paymentData.setPayment(payment);
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
   }*/

   /**
    * This method tests preRegister refund.
    *//*
   public void testPreRegisteredRefund()
   {
      startUp();
      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      String dataCashReference;
      try
      {
         dataCashReference = dataCashServiceHandler.authSinglePayment(
                     uuid.toString(), "", "99491100");
         dataCashServiceHandler.fulfill(uuid.toString(), "99491100");
         PaymentData paymentData = PaymentStore.getInstance().getPaymentData(uuid);
         DataCashHistoricTransaction dataCashHistoricTransaction =
                 new DataCashHistoricTransaction("VisaElectron", transactionAmount,
                      dataCashReference, getMerchantReferenceForHistoricTransaction(
                         getBookingInfo().getBookingComponent().getClientApplication(),
                                   dataCashReference));
         dataCashHistoricTransaction.setPaymentMethod(PaymentMethod.REFUND_CARD);
         paymentData.getPayment().addPaymentTransaction(dataCashHistoricTransaction);
         dataCashServiceHandler.preRegisteredRefund(uuid.toString(),
                    "99639121", dataCashReference, null);
         assertTrue(Boolean.TRUE);
      }
      catch (XmlRpcException e)
      {
         // TODO Auto-generated catch block
         e.printStackTrace();
         fail("failed");
      }
   }
*/
   /**
    * Tests whether the preRegisterRefund is successful.
    *
    */
   public void testPreRegisterRefund1()
   {

      DataCashServiceHandler dataCashServiceHandler =
         new DataCashServiceHandler();
      startUpException();
      String dataCashReference = "163634764";
      Boolean isRefunded;
      try
      {
         isRefunded =
            dataCashServiceHandler.preRegisteredRefund(uuid.toString(),
               DATACASHACCOUNT, dataCashReference, null);
         assertTrue(isRefunded);
      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
      catch (IndexOutOfBoundsException aex)
      {
         aex.printStackTrace();
      }

   }


   /**
    * Tests whether the DDSetUpMandate is successful.
    *
    */
  public void testDDSetUpMandate()throws Exception{

	   DataCashServiceHandler dataCashServiceHandler =
		         new DataCashServiceHandler();

      HashMap<String,String> applicationData=new HashMap<String,String>();
      applicationData.put("cardtype","debit");
      applicationData.put("payment_0_type","DIRECTDEBIT");
            Object directDebitDetails[]={"826300","80000990","SREENIVASULU KAMINI","setup","I-1003600"};
            Map<String,String> ddSetUpStatus = null;

      try
      {
    	  ddSetUpStatus=dataCashServiceHandler.ddSetUpMandate("GBP","GB","GB","THOMSON","99010328",applicationData,directDebitDetails);


    	  LogWriter.logInfoMessage("======================="+ddSetUpStatus+"=====================");

    	  assertNotNull(ddSetUpStatus);

           String datacashReferenceNumber=(String)ddSetUpStatus.get("datacashReferenceNumber");
           String method=(String)ddSetUpStatus.get("method");
           String stage=(String)ddSetUpStatus.get("stage");
           String startDate=(String)ddSetUpStatus.get("startDate");
           String merchantReferenceNumber=(String)ddSetUpStatus.get("merchantReferenceNumber");
           String mode=(String)ddSetUpStatus.get("mode");
           String reason=(String)ddSetUpStatus.get("reason");
           String status=(String)ddSetUpStatus.get("status");
           LogWriter.logInfoMessage("======================="+datacashReferenceNumber+"=====================");

           assertNotNull(datacashReferenceNumber);
           assertNotNull(method);
           assertNotNull(stage);
           assertNotNull(startDate);
           assertNotNull(merchantReferenceNumber);
           assertNotNull(mode);
           assertNotNull(reason);
           assertNotNull(status);


      }
      catch (XmlRpcException xmlrpcex)
      {
         xmlrpcex.printStackTrace();
         fail(ERROR_MSG);
      }
      catch (IndexOutOfBoundsException aex)
      {
         aex.printStackTrace();
      }

   }









  /**
   * Tests whether the SubmitddDrawdownBatch is successful.
   *
   */
  public void testSubmitddDrawdownBatch()
  {

     DataCashServiceHandler dataCashServiceHandler =
        new DataCashServiceHandler();

     HashMap<String,String> applicationData=new HashMap<String,String>();
     applicationData.put("cardtype","debit");
     applicationData.put("payment_0_type","DIRECTDEBIT");

     Object o1[]={"4500187297","500.50","","01","drawdown","I-1003200"};
     Object o2[]={"4500678839","600.50","","01","drawdown","I-1003393"};

     Object drawdownDetails[]={o1,o2};
     Map<String,String> ddSubmitDrawdownStatus = null;


     try
     {
    	 ddSubmitDrawdownStatus=dataCashServiceHandler.submitddDrawdownBatch("GBP","GB","THOMSON","99010328",applicationData,drawdownDetails);
         assertNotNull(ddSubmitDrawdownStatus);

         String datacashReferenceNumber=(String)ddSubmitDrawdownStatus.get("datacashReferenceNumber");
      //   String information=(String)ddSubmitDrawdownStatus.get("information");
         String merchantReferenceNumber=(String)ddSubmitDrawdownStatus.get("merchantReferenceNumber");
     //    String mode=(String)ddSubmitDrawdownStatus.get("mode");
         String reason=(String)ddSubmitDrawdownStatus.get("reason");
         String status=(String)ddSubmitDrawdownStatus.get("status");
         String time=(String)ddSubmitDrawdownStatus.get("time");

         assertNotNull(datacashReferenceNumber);
  //       assertNotNull(information);
         assertNotNull(merchantReferenceNumber);
  //       assertNotNull(mode);
         assertNotNull(reason);
         assertNotNull(status);
         assertNotNull(time);

     }
     catch (XmlRpcException xmlrpcex)
     {
        xmlrpcex.printStackTrace();
        fail(ERROR_MSG);
     }
     catch (IndexOutOfBoundsException aex)
     {
        aex.printStackTrace();
     }

  }













  /**
   * Tests whether the GetddDrawdownBatchStatus is successful.
   *
   */
  public void testGetddDrawdownBatchStatus()
  {

     DataCashServiceHandler dataCashServiceHandler =
        new DataCashServiceHandler();

     HashMap<String,String> applicationData=new HashMap<String,String>();
     applicationData.put("cardtype","debit");
     applicationData.put("payment_0_type","DIRECTDEBIT");

     HashMap<String,String> getDrawdownStatus=new HashMap<String,String>();
     getDrawdownStatus.put("datacashReferenceNumber","4800204501605323");
     getDrawdownStatus.put("method","query");
     getDrawdownStatus.put("merchantReferenceNumber","batchref20170913201638");

     Object[] drawdownStatus=null;



     try
     {
    	 drawdownStatus=dataCashServiceHandler.getddDrawdownBatchStatus("GBP","GB","THOMSON","99010328",applicationData,getDrawdownStatus);
    	 assertNotNull(drawdownStatus);

     }
     catch (XmlRpcException xmlrpcex)
     {
        xmlrpcex.printStackTrace();
        fail(ERROR_MSG);
     }
     catch (IndexOutOfBoundsException aex)
     {
        aex.printStackTrace();
     }

  }








  /**
   * Tests whether the SubmitCancelMandateBatch is successful.
   *
   */
  public void testSubmitCancelMandateBatch()
  {

     DataCashServiceHandler dataCashServiceHandler =
        new DataCashServiceHandler();

     Map<String,String> applicationData=new HashMap<String,String>();
     applicationData.put("cardtype","debit");
     applicationData.put("payment_0_type","DIRECTDEBIT");


     Object o1[]={"DATACASH@123","cancel","5555555"};
     Object o2[]={"DATACASH@456","cancel","7777777"};
     Object cancelMandateDetails[]={o1,o2};
     Map<String,String> cancelBatchStatus=new HashMap<String,String>();

     try
     {


    	 cancelBatchStatus=dataCashServiceHandler.submitCancelMandateBatch("GBP","GB","THOMSON","99010328",applicationData,cancelMandateDetails);
   	     assertNotNull(cancelBatchStatus);



   	  String datacashReferenceNumber=(String)cancelBatchStatus.get("datacashReferenceNumber");
 //     String information=(String)cancelBatchStatus.get("information");
      String merchantReferenceNumber=(String)cancelBatchStatus.get("merchantReferenceNumber");
      String mode=(String)cancelBatchStatus.get("mode");
      String reason=(String)cancelBatchStatus.get("reason");
      String status=(String)cancelBatchStatus.get("status");
      String time=(String)cancelBatchStatus.get("time");

      assertNotNull(datacashReferenceNumber);
   //   assertNotNull(information);
      assertNotNull(merchantReferenceNumber);
      assertNotNull(mode);
      assertNotNull(reason);
      assertNotNull(status);
      assertNotNull(time);







     }
     catch (XmlRpcException xmlrpcex)
     {
        xmlrpcex.printStackTrace();
        fail(ERROR_MSG);
     }
     catch (IndexOutOfBoundsException aex)
     {
        aex.printStackTrace();
     }

  }









  /**
   * Tests whether the GetCancelMandateBatchStatus is successful.
   *
   */
  public void testGetCancelMandateBatchStatus()
  {

     DataCashServiceHandler dataCashServiceHandler =
        new DataCashServiceHandler();

     HashMap<String,String> applicationData=new HashMap<String,String>();
     applicationData.put("cardtype","debit");
     applicationData.put("payment_0_type","DIRECTDEBIT");

     HashMap<String,String> cancelMandateDetails=new HashMap<String,String>();
     cancelMandateDetails.put("directDebitReferenceNumber","4200204500894987");
     cancelMandateDetails.put("method","query");
     cancelMandateDetails.put("merchantReferenceNumber","batchref20170913201638");

     Object[] cancelBatchStatus=null;



     try
     {


      	 cancelBatchStatus=dataCashServiceHandler.getCancelMandateBatchStatus("GBP","GB","THOMSON","99010328",applicationData,cancelMandateDetails);
    	 assertNotNull(cancelBatchStatus);


     }
     catch (XmlRpcException xmlrpcex)
     {
        xmlrpcex.printStackTrace();
        fail(ERROR_MSG);
     }
     catch (IndexOutOfBoundsException aex)
     {
        aex.printStackTrace();
     }

  }




	public void testGetToken(){
		TokenServiceRequestProcessor tokenServiceRequestProcessor=new TokenServiceRequestProcessor();
		String token=tokenServiceRequestProcessor.getToken();
		assertNotNull(token);

	}

	public void testValidateAccountDetails()throws Exception{
		Map<String,String> validateDetails=new HashMap<String,String>();
		Map<String,String> validateAccountDetailsResponse=new HashMap<String,String>();
		  validateDetails.put("sortCode","000004");
          validateDetails.put("accountNumber","12345677");

          ExperianServiceImplementation experianServiceImplementation=new ExperianServiceImplementation();
		    try{
    	   validateAccountDetailsResponse=experianServiceImplementation.validateAccountDetails("GB", validateDetails);

    	   assertNotNull(validateAccountDetailsResponse);


       }
       catch(IOException e){

    	   e.printStackTrace();

       }


	}


	public void testVerifyAccountDetails()throws Exception{
		Map<String,String> verifyAccountDetails=new HashMap<String,String>();
		Map<String,String> verifyAccountDetailsResponse=new HashMap<String,String>();


		verifyAccountDetails.put("sortCode","000004");
		verifyAccountDetails.put("accountNumber","12345677");
		verifyAccountDetails.put("firstName","Ashely");
		verifyAccountDetails.put("lastName","Marma");
		verifyAccountDetails.put("dob","1968-04-10");
		verifyAccountDetails.put("houseNumber","1");
		verifyAccountDetails.put("street","Wydeville Manor Road");
		verifyAccountDetails.put("postCode","SE12 0ES");
		verifyAccountDetails.put("houseName","Abbey Lodge");
		verifyAccountDetails.put("flat",null);


          ExperianServiceImplementation experianServiceImplementation=new ExperianServiceImplementation();
		    try{
		    	verifyAccountDetailsResponse=experianServiceImplementation.verifyAccountDetails("GB", verifyAccountDetails);

    	   assertNotNull(verifyAccountDetailsResponse);


       }
       catch(IOException e){

    	   e.printStackTrace();

       }


	}









}

