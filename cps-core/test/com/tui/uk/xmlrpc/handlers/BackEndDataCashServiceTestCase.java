/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BackEndDataCashServiceTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2009-03-19 $
 *
 * $Author: bibin.j $
 *
 * $Log: $
 */
package com.tui.uk.xmlrpc.handlers;

import junit.framework.TestCase;

import org.apache.xmlrpc.XmlRpcException;

/**
 * The TestCase for BackEndDataCashPaymentTransaction class.
 *
 */
public class BackEndDataCashServiceTestCase extends TestCase
{

   /** The auth code. */
   private static String authCode = null;

   /**
    * Tests whether the fulfill is successful.
    *
    */
   public void testBackEndFulfill()
   {
      BackEndDataCashService backEndDataCashService = new BackEndDataCashService();
      try
      {
         String response =
            backEndDataCashService.backEndFulfillPayment("fgfgfg", "sd43434", authCode, "3554");
         assertNotNull(response);
      }
      catch (XmlRpcException xmlrpcex)
      {
         assertTrue("Tests failed :" + xmlrpcex.getMessage(), Boolean.TRUE);
      }
   }

   /**
    * Tests whether cancel is successful.
    *
    */
   public void testBackEndCancel()
   {
      BackEndDataCashService backEndDataCashService = new BackEndDataCashService();
      try
      {
         String response = backEndDataCashService.backEndCancel("fgfgfg", "dse234", "3554");
         assertNotNull(response);
      }
      catch (XmlRpcException xmlrpcex)
      {
         assertTrue("Tests failed :" + xmlrpcex.getMessage(), Boolean.TRUE);
      }
   }
}
