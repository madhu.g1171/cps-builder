/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PurgeCardDetailsClient.java,v $
 *
 * $Revision: 1.1 $
 *
 * $Date: 2008-05-14 12:56:12 $
 *
 * $Author: thomas.pm $
 *
 * $Log: not supported by cvs2svn $
 */

package com.tui.uk.xmlrpc.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.Vector;

import junit.framework.TestCase;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;
import org.apache.xmlrpc.client.XmlRpcCommonsTransportFactory;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;
import org.xml.sax.SAXException;

import com.meterware.httpunit.PostMethodWebRequest;
import com.meterware.httpunit.WebRequest;
import com.meterware.httpunit.WebResponse;
import com.meterware.servletunit.ServletRunner;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.PostPaymentServlet;

/**
 * This class is responsible to test the card encryption service for hopla.
 *
 * @author thomas.pm
 */
public class PurgeCardDetailsClient extends TestCase
{

   /** The location of our server. */
   private static final String SERVER_URL =
      "http://localhost:8080/cps/xmlrpc";

   /** The location of our server. */
   private static final String TOKEN_SERVER_URL =
      "http://localhost:8080/tokenServlet";

   /** The constant field TIME_OUT. */
   private static final int TIME_OUT = 60 * 1000;

   /** The <code>XmlRpcClientConfigImpl</code> object. */
   private XmlRpcClientConfigImpl config;

   /** The web server. */
   private WebServer server;

   /** The transaction token. */
   private static String token;

   /** The constant for port. */
   private static final int PORT = 8080;

   /**
    * Sets the initial config data.
    */
   public void setUp()
   {
      if (config == null)
      {
         config = new XmlRpcClientConfigImpl();
         try
         {
            config.setServerURL(new URL(SERVER_URL));
         }
         catch (java.net.MalformedURLException e)
         {
            LogWriter.logInfoMessage("Caught MalformedURLException\n");

            e.printStackTrace();
         }
         config.setEnabledForExtensions(true);
         config.setConnectionTimeout(TIME_OUT);
         config.setReplyTimeout(TIME_OUT);
      }
      try
      {
         // Invoke me as <http://localhost:8080/RPC2>.
         server = new WebServer(PORT);
         XmlRpcServer xmlRpcServer = server.getXmlRpcServer();
         PropertyHandlerMapping propertyHandlerMapping =
            new PropertyHandlerMapping();
         URL url = this.getClass().getResource("XmlRpcServlet.properties");
         propertyHandlerMapping.load(Thread.currentThread()
            .getContextClassLoader(), url);
         xmlRpcServer.setHandlerMapping(propertyHandlerMapping);
         XmlRpcServerConfigImpl serverConfig =
            (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
         serverConfig.setEnabledForExtensions(true);
         serverConfig.setContentLengthOptional(false);
         server.start();
         LogWriter.logInfoMessage("Server XML-RPC is ready");
      }
      catch (XmlRpcException exception)
      {
         System.err.println("JavaServer: " + exception.toString());
      }
      catch (IOException ioe)
      {
         System.err.println("JavaServer: " + ioe.toString());
      }
   }

   /**
    * Sets the final functionality.
    */
   public void tearDown()
   {
      server.shutdown();
   }

   /**
    * Test case to test card encryption service.
    */
   @SuppressWarnings("unchecked")
   public void testPurgeCardDetails()
   {
      try
      {
         paymentServlet();
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();
         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         client.setTransportFactory(new XmlRpcCommonsTransportFactory(
            client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute(
               "purgeCardDetailsHandler.purgeCardDetails", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println("JavaClient: XML-RPC Fault # "
            + Integer.toString(exception.code) + ": "
            + exception.toString());
         fail("Exception occured");
      }
   }

   /**
    * This method sets a token for the transaction.
    */
   @SuppressWarnings("unchecked")
   public void setTransactionToken()
   {
      try
      {
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();
         // Build our parameter list.
         // Set the BookingComponent
         BookingComponent component = getBookingComponent();
         Object[] params = new Object[] {component};
         client.setTransportFactory(new XmlRpcCommonsTransportFactory(
            client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         token =
            (String) client.execute(
               "tokenGenerationHandler.generateToken", params);
         assertTrue(token.length() > 0);
      }
      catch (XmlRpcException exception)
      {
         System.err.println("JavaClient: XML-RPC Fault # "
            + Integer.toString(exception.code) + ": "
            + exception.toString());
      }
   }

   /**
    * Payment servlet.
    */
   public void paymentServlet()
   {
      ServletRunner sr = new ServletRunner();
      sr.registerServlet("tokenServlet", PostPaymentServlet.class.getName());
      WebRequest req = new PostMethodWebRequest(TOKEN_SERVER_URL);
      req.setParameter("bookingBalanceForToday", "100");
      req.setParameter("bookingPayableToday", "40");
      req.setParameter("bookingPayable", "60");
      req.setParameter("bookingPaidBelow", "40");
      req.setParameter("bookingPayableBalanceOwing", "60");
      req.setParameter("allocationCount", "1");
      req.setParameter("payment_1_paymentMethod", "SE");
      req.setParameter("payment_1_cardCharges", "1.10");
      req.setParameter("payment_1_cardNumber", "4444333322221111");
      req.setParameter("payment_1_expiryDate", "10/10");
      req.setParameter("payment_1_securityCode", "123");
      req.setParameter("payment_1_total", "10.50");
      req.setParameter("payment_1_amountPaid", "11.60");
      req.setParameter("payment_1_cardType", "Visa");
      req.setParameter("payment_1_startDate", "06/08");
      req.setParameter("payment_1_nameOnCard", "veena");
      req.setParameter("payment_1_issueNumber", "123456");
      req.setParameter("payment_1_postCode", "123456");
      setTransactionToken();
      req.setParameter("token", token);
      try
      {
         WebResponse response = sr.newClient().getResponse(req);
         assertNotNull("No response received", response);
      }
      catch (SAXException saxe)
      {
         fail("SAXException" + saxe.getMessage());
         saxe.printStackTrace();
      }
      catch (MalformedURLException mue)
      {
         fail("MalformedURLException" + mue.getMessage());
         mue.printStackTrace();
      }
      catch (IOException ioe)
      {
         fail("IOException" + ioe.getMessage());
         ioe.printStackTrace();
      }
   }

   /**
    * Gets the booking component.
    *
    * @return the booking component
    */
   public BookingComponent getBookingComponent()
   {
      ClientApplication clientApplicationName = null;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance("GBP"));
      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL,
            totalAmount);
      Map<String, String> countryMap = new HashMap<String, String>();
      countryMap.put("GB", "United Kingdom");
      bookingComponent.setCountryMap(countryMap);
      return bookingComponent;
   }
}
