/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: DataCashServiceClient.java,v $
 *
 * $Revision: 1.2 $
 *
 * $Date: 2008-05-13 09:41:02 $
 *
 * $Author: sindhushree.g $
 *
 * $Log: not supported by cvs2svn $
 * Revision 1.1  2008/05/13 08:34:30  sindhushree.g
 * Added/Modified to add XMLRPC server support.
 *
*/

package com.tui.uk.xmlrpc.client;

import com.tui.uk.log.LogWriter;

import junit.framework.TestCase;

/**
 * This class is responsible to test the services offered by the datacash module.
 *
 * @author sindhushree.g
 *
 */
public class DataCashServiceClient extends TestCase
{

//   /** The location of our server. */
//   private static final String SERVER_URL =
//      "http://localhost:8080/cps/xmlrpc";
//
//   /** The location of our server. */
//   private static final String TOKEN_SERVER_URL =
//      "http://localhost:8080/paymentServlet";
//
//   /** The constant field TIME_OUT. */
//   private static final int TIME_OUT = 60 * 1000;
//
//   /** The Port of the web server. */
//   private static final int PORT = 8080;
//
//   /** The <code>XmlRpcClientConfigImpl</code> object. */
//   private XmlRpcClientConfigImpl config;
//
//   /** The transaction token. */
//   private static String token;
//
//   /** The web server for this testcase. */
//   private WebServer server;
//
//   /** The amount to reverse. */
//   private static final BigDecimal REVERSE_AMOUNT = new BigDecimal(5.0);
//
//   /** The client account. */
//   private static final String CLIENT = "99080800";
//
//   /** The auth code. */
//   private static final String AUTH_CODE = "123";
//
//   /** The failed message. */
//   private static final String FAIL_MESSAGE = "JavaClient: XML-RPC Fault.";

   /**
    * Sets the initial config data.
    */
   @Override
   public void setUp()
   {
      /*if (config == null)
      {
         config = new XmlRpcClientConfigImpl();
         try
         {
            config.setServerURL(new URL(SERVER_URL));
         }
         catch (java.net.MalformedURLException e)
         {
            LogWriter.logInfoMessage("Caught MalformedURLException\n");
            e.printStackTrace();
         }
         config.setEnabledForExtensions(true);
         config.setConnectionTimeout(TIME_OUT);
         config.setReplyTimeout(TIME_OUT);
      }
      try
      {
          // Invoke me as <http://localhost:8080/RPC2>.
          server = new WebServer(PORT);
          XmlRpcServer xmlRpcServer = server.getXmlRpcServer();
          PropertyHandlerMapping propertyHandlerMapping = new PropertyHandlerMapping();
          URL url = this.getClass().getResource("XmlRpcServlet.properties");
          propertyHandlerMapping.load(Thread.currentThread().getContextClassLoader(), url);
          xmlRpcServer.setHandlerMapping(propertyHandlerMapping);
          XmlRpcServerConfigImpl serverConfig =
             (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
          serverConfig.setEnabledForExtensions(true);
          serverConfig.setContentLengthOptional(false);

          server.start();
          LogWriter.logInfoMessage("Server XML-RPC is ready");

      }
      catch (XmlRpcException exception)
      {
          System.err.println("JavaServer: " + exception.toString());
      }
      catch (IOException ioe)
      {
          System.err.println("JavaServer: " + ioe.toString());
      }
      if (token == null)
      {
         //setTransactionToken();
      }*/
   }

   /**
    * This method is called after the end of each test cases.
    */
   @Override
   public void tearDown()
   {
       /*server.shutdown();*/
   }

   /**
    * Test case to test auth datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testAuthPayment()
   {
      /*try
      {
          setPaymentData();
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         params.addElement(AUTH_CODE);
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.authPayment", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }

   /**
    * Test case to test fulfill datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testFulFill()
   {
      /*try
      {
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.fulfill", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }

   /**
    * Test case to test reverse datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testReverse()
   {
      /*try
      {
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         Money money = new Money(REVERSE_AMOUNT, Currency.getInstance("GBP"));
         params.add(money);
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.reverse", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }

   /**
    * Test case to test auth datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testAuthRefund()
   {
      /*try
      {
          setPaymentData();
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         params.addElement(AUTH_CODE);
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.authRefund", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }

   /**
    * Test case to test cancel datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testCancel()
   {
      /*try
      {
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.cancel", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }

   /**
    * Test case to test auth datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testdoPayment()
   {
      /*try
      {
          setPaymentData();
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         params.addElement(AUTH_CODE);
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.doPayment", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }


   /**
    * Test case to test auth datacash service.
    */
   @SuppressWarnings("unchecked")
   public void testdoRefund()
   {
      /*try
      {
          setPaymentData();
         // Create an object to represent our server.
         XmlRpcClient client = new XmlRpcClient();

         // Build our parameter list.
         Vector params = new Vector();
         params.addElement(UUID.fromString(token));
         params.addElement(AUTH_CODE);
         params.add(CLIENT);

         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
         // set configuration
         client.setConfig(config);
         // Call the server, and get our result.
         Boolean result =
            (Boolean) client.execute("datacash.doRefund", params);
         assertTrue(result);
      }
      catch (XmlRpcException exception)
      {
         System.err.println(exception.toString());
         fail(FAIL_MESSAGE);
      }*/
   }

//   /**
//    * This method sets a token for the transaction.
//    */
//   @SuppressWarnings("unchecked")
//   private void setTransactionToken()
//   {
//      try
//      {
//         // Create an object to represent our server.
//         XmlRpcClient client = new XmlRpcClient();
//
//         // Build our parameter list.
//         //Set the BookingComponent
//         BookingComponent component = null;
//         Object[] params = new Object[]{component};
//         client.setTransportFactory(new XmlRpcCommonsTransportFactory(client));
//         // set configuration
//         client.setConfig(config);
//
//         // Call the server, and get our result.
//         token = (String) client.execute("tokenGenerationHandler.generateToken", params);
//
//         assertTrue(token.length() > 0);
//      }
//      catch (XmlRpcException exception)
//      {
//         System.err.println(exception.toString());
//      }
//   }

//   /**
//    * Sets the PaymentData object.
//    */
//   private void setPaymentData()
//   {
//      ServletRunner sr = new ServletRunner();
//      sr.registerServlet("paymentServlet", PaymentServlet.class.getName());
//      ServletUnitClient sc = sr.newClient();
//      WebRequest req = new PostMethodWebRequest(TOKEN_SERVER_URL);
//      req.setParameter("allocationCount", "1");
//      req.setParameter("payment_1_paymentMethod", "CX");
//      req.setParameter("payment_1_cardCharges", "1.10");
//      req.setParameter("payment_1_cardNumber", "4444333322221111");
//      req.setParameter("payment_1_expiryDate", "10/10");
//      req.setParameter("payment_1_securityCode", "123");
//      req.setParameter("payment_1_total", "10.50");
//      req.setParameter("payment_1_amountPaid", "11.60");
//      req.setParameter("token", token);
//      try
//      {
//         WebResponse response = sc.getResponse(req);
//         assertNotNull("No response received", response);
//      }
//      catch (MalformedURLException mue)
//      {
//         fail("MalformedURLException:" + mue.getMessage());
//      }
//      catch (IOException ioe)
//      {
//         fail("IOException:" + ioe.getMessage());
//      }
//      catch (SAXException se)
//      {
//         fail("SAXException:" + se.getMessage());
//      }
//   }
}
