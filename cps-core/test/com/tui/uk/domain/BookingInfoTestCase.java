/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingInfoTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2010-08-09 $
 *
 * $Author: roopesh.s $
 *
 * $Log: $
 */
package com.tui.uk.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;

import junit.framework.TestCase;

import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.ClientApplication;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.client.domain.PriceComponent;
import com.tui.uk.config.ConfReader;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.utility.TestDataSetup;

/**
 * This is the test case for BookingInfo Class.
 */
public class BookingInfoTestCase extends TestCase
{

   /** Transaction amount. */
   private static final double TRANSACTION_AMOUNT = 5000.00;

   /** Card charge. */
   private static final BigDecimal CARD_CHARGE = new BigDecimal("95.00");

   /** Set the value for No Of Transactions. */
   private static final int NO_OF_TRANSACTIONS = 3;

   /** Instance Variable for the Booking Component Class. */
   private BookingInfo bookingInfo = null;

   /** The priceComponent List holing the Price Components. */
   private List<PriceComponent> priceComponents = null;

   /** Set the value for type of Currency. */
   private String currency = "GBP";

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   protected void setUp() throws Exception
   {
      LogWriter.logInfoMessage("Test Case Started");
      ClientApplication clientApplicationName = ClientApplication.ThomsonBuildYourOwn;
      String clientDomainURL = "test";
      Money totalAmount = new Money(new BigDecimal("10"), Currency.getInstance(currency));

      BookingComponent bookingComponent =
         new BookingComponent(clientApplicationName, clientDomainURL, totalAmount);
      bookingInfo = new BookingInfo(bookingComponent);

      // creating priceComponents for setPriceComponents
      priceComponents = new ArrayList<PriceComponent>();
      PriceComponent priceComponent =
         new PriceComponent("Flight Meals", new Money(new BigDecimal("99"), Currency
            .getInstance(currency)));
      priceComponents.add(priceComponent);

      /** Setting the Price Components */
      bookingComponent.setPriceComponents(priceComponents);

      /** Setting the values for Calculated Total Amount */
      bookingInfo.setCalculatedTotalAmount(new Money(new BigDecimal("900"), Currency
         .getInstance(currency)));

      /** Setting the Payable Amount */
      bookingComponent.setPayableAmount(new Money(new BigDecimal("1200"), Currency
         .getInstance(currency)));

      /** Setting the Calculated Payable Amount */
      bookingInfo.setCalculatedPayableAmount(totalAmount);

      /** Setting the value for Number Of Transactions */
      bookingComponent.setNoOfTransactions(NO_OF_TRANSACTIONS);
   }

   /**
    * Ensure that the Discount Component is correct.
    */
   public void testUpdateDiscountComponent()
   {
      Money money = new Money(new BigDecimal("1000.75"), Currency.getInstance(currency));
      bookingInfo.updateDiscountComponent(money, DispatcherConstants.PRICE_BEAT);
      List<PriceComponent> retpriceComponents =
         bookingInfo.getBookingComponent().getPriceComponents();
      assertNotNull("RetpriceComponents list should not be null", retpriceComponents);
      assertEquals(2, retpriceComponents.size());
   }

   /**
    * Ensure that the Calculated Discount Percentage is correct.
    */
   public void testCalculateDiscountPercentage()
   {
      Money totalDiscountMoney =
         new Money(new BigDecimal("1100.75"), Currency.getInstance(currency));
      bookingInfo.calculateDiscountPercentage(totalDiscountMoney);
   }

   /**
    * Test for the Updated Discount Amount.
    */
   public void testUpdateDiscount()
   {
      Money discountAmount = new Money(new BigDecimal("150"), Currency.getInstance(currency));
      bookingInfo.updateDiscount(discountAmount, "Flight Meals", DispatcherConstants.FULL_BALANCE);
   }

   /**
    * Test to check if the expiry year list length is proper.
    */
   public void testExpiryYearList()
   {
      assertEquals(bookingInfo.getExpiryYearList().size(), ConfReader.getIntEntry(
         "expiry.year.count", 0));
   }

   /**
    * Test to check if the start year list length is proper.
    */
   public void testStartYearList()
   {
      assertEquals(bookingInfo.getStartYearList().size(), ConfReader.getIntEntry(
         "start.year.count", 0));
   }

   /**
    * Test case to test if the number of card details displayed is equal to the number of cards sent
    * by the client.
    */
   /*public void testPercentCardDetails()
   {
      bookingInfo = TestDataSetup.getDotNetBookingInfo(TRANSACTION_AMOUNT);
      assertEquals(bookingInfo.getAllowedCards().size(), bookingInfo.getBookingComponent()
         .getPaymentType().get("CNP").size());
   }*/

   /**
    * Test case to test if the number of card details displayed is equal to the number of cards sent
    * by the client.
    */
   /*public void testFlatCardDetails()
   {
      bookingInfo = TestDataSetup.getDotNetBookingInfo(TRANSACTION_AMOUNT);
      Map<String, Object[]> allowedCardsMap = new HashMap<String, Object[]>();
      Object[] cardCharges = {CARD_CHARGE_PERCENT, CARD_CHARGE_MIN, CARD_CHARGE_MAX, "VISA"};
      allowedCardsMap.put("VISA", cardCharges);
      // bookingInfo.setAllowedCards(allowedCardsMap);
      assertEquals(bookingInfo.getAllowedCards().size(), bookingInfo.getBookingComponent()
         .getPaymentType().get("CNP").size());
   }*/

   /**
    * Test case to check if maximum card charge is used properly.
    */
   public void testMaxUpdateAllowedCardCharges()
   {
      bookingInfo = TestDataSetup.getDotNetBookingInfo(TRANSACTION_AMOUNT);
      bookingInfo.getUpdatedCardCharge("VISA", bookingInfo.getCalculatedPayableAmount());
      assertEquals(CARD_CHARGE, bookingInfo.getCalculatedCardCharge().getAmount());
   }

   /**
    * Test case to test getPayDescription method.
    */
   public void testGetPayDescription()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.getPayDescription();
   }

   /**
    * Test case to test getThreeDEnabledLogos method.
    */
   public void testGetThreeDEnabledLogos()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.getThreeDEnabledLogos();
   }

   /**
    * Test case to test reset method.
    */
   public void testReset()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.setCalculatedPayableAmount(null);
      bookingInfoLocal.setCalculatedTotalAmount(null);
      bookingInfoLocal.reset();
   }

   /**
    * Test case to test resetPayableAmount method.
    */
   public void testResetPayableAmount()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.resetPayableAmount();
   }

   /**
    * Test case to test setPaymentDataMap method.
    */
   public void testSetPaymentDataMap()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.setPaymentDataMap(new HashMap<String, String>());
   }

   /**
    * Test case to test setDatacashEnabled method.
    */
   public void testSetDatacashEnabled()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.setDatacashEnabled(true);
   }

   /**
    * Test case to test setCardChargePercent method.
    */
   /*
   public void testSetCardChargePercent()
   {
      BookingInfo bookingInfoLocal = TestDataSetup.getBookingInfo(PackageType.ComponentPackage);
      bookingInfoLocal.setCardChargePercent(new BigDecimal(10));
   }*/
}
