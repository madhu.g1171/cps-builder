/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PaymentTestCase.java,v $
 *
 * $Revision: $
 *
 * $Date: 2008-07-18 04:35:06 $
 *
 * $author : Vinodha.S $
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PaymentMethod;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the Payment class.
 */
public class PaymentTestCase extends TestCase
{

   /** Total amount. */
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100.00);

   /** The transaction amount. */
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance("GBP"));

   /** The transaction charge. */
   private Money transactionCharge;

   /** The character array holing cv2. */
   private char[] cv2 = {'4', '4', '4' };

   /** The character array holing cv2. */
   private char[] validPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1', '1', '1', '1' };

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * This <code>Map</code> holds voucher series code and amount as
    * entry.
    */
   private Map<String, Money> voucherLineItems;

   /**
    * Setting valid card details.
    *
    * @return Card
    */
   private CnpCard getValidCard()
   {
      return new CnpCard(validPan, "test", "08/10", cv2, "aaa", "visa", THREE, THREE);
   }

   /**
    * Checks for valid PaymentTransactions based on the payment method passed.
    */
   public void testGetPaymentTransactions()
   {
      LogWriter.logMethodStart("testGetPaymentTransactions");
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactions());
      List<PaymentTransaction> paymentTransactions =
         payment.getPaymentTransactions(PaymentMethod.DATACASH);
      LogWriter.logMethodEnd("testGetPaymentTransactions");
      assertEquals("Returned datacash payment transaction's size is 1", 1, paymentTransactions
         .size());
   }

   /**
    * Checks for empty PaymentTransactions based on the payment method passed.
    */
   public void testGetPaymentTransactionsForEmpty()
   {
      LogWriter.logMethodStart("testGetPaymentTransactionsForEmpty");
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactions());
      List<PaymentTransaction> paymentTransactions =
         payment.getPaymentTransactions(PaymentMethod.CHEQUE);
      LogWriter.logMethodEnd("testGetPaymentTransactionsForEmpty");
      assertEquals("Returned datacash payment transaction's size is 1", 0, paymentTransactions
         .size());
   }

   /**
    * Creates the transactions.
    *
    * @return the list< payment transaction>
    */
   private List<PaymentTransaction> createTransactions()
   {
      List<PaymentTransaction> paymentTransactions = new ArrayList<PaymentTransaction>();
      DataCashPaymentTransaction dataCashPaymentTransaction =
         new DataCashPaymentTransaction("DC", transactionAmount, transactionCharge, getValidCard(),
            getMerchantReference());
      paymentTransactions.add(dataCashPaymentTransaction);
      CashPaymentTransaction cashPaymentTransaction =
         new CashPaymentTransaction("CA", transactionAmount);
      paymentTransactions.add(cashPaymentTransaction);
      VoucherPaymentTransaction voucherPaymentTransaction =
         new VoucherPaymentTransaction("VC", transactionAmount, voucherLineItems);
      paymentTransactions.add(voucherPaymentTransaction);

      return paymentTransactions;
   }

   /**
    * Test case for clearing transactions.
    */
   public void testClearTransactions()
   {
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactions());
      payment.clearPaymentTransactions();
      assertEquals("Successfully cleared payment transactions", 0, payment.getPaymentTransactions()
         .size());
   }

   /**
    * Test case for clearing sensitive data.
    */
   public void testClearSensitiveData()
   {
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactions());
      payment.clearSensitiveData();
      List<PaymentTransaction> datacashTrans =
         payment.getPaymentTransactions(PaymentMethod.DATACASH);
      //In order to make sure that card data are cleared we will call validate on
      // DatacashPaymentTransaction, so that validate will fail as card details are cleared
      try
      {
         datacashTrans.get(0).validate();
         fail("Exception expected");
      }
      catch (PaymentValidationException pve)
      {
         LogWriter.logDebugMessage(pve.getMessage(), pve);
         assertTrue("Successfully cleared sensitive data", true);
      }
   }

   /**
    * Test case for  testing getAllEssentialTransactionsData.
    */
   public void testgetAllEssentialTransactionsData()
   {
      Payment payment = new Payment();
      payment.setPaymentTransaction(createTransactions());
      payment.getAllEssentialTransactionsData();
      assertTrue("Successfully got Essential transaction data", payment.getPaymentTransactions()
         .size() > 0);
   }

   /**
    * Gets a unique merchant reference using <code>UUID</code>.
    *
    * @return The generated unique merchant reference.
    */
   private static String getMerchantReference()
   {
      return String.valueOf(UUID.randomUUID().getMostSignificantBits()).replaceAll("-", "");
   }
}
