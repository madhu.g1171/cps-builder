/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: PDQPaymentTransactionTestCase.java$
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author: Vinodha.S $
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;

import junit.framework.TestCase;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the PDQPaymentTransaction class for valid and invalid
 * authorization codes.
 */
public class PDQPaymentTransactionTestCase extends TestCase
{

   /** Total amount. */
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100.00);

   /** Total charge. */
   private static final BigDecimal TOTAL_CHARGE = BigDecimal.valueOf(2.00);

   /** The transaction amount. */
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));

   /** The transaction charge. */
   private Money transactionCharge = new Money(TOTAL_CHARGE, Currency.getInstance(GBP));

   /** The payment type code. */
   private String paymentTypeCode = "VC";

   /** The Constant GBP. */
   private static final String GBP = "GBP";

   /**
    * Test for valid authorization code.
    */
   public void testAuthCode()
   {
      String authCode = "1234";
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test valid auth code", params);
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
               authCode);
         LogWriter.logDebugMessage("Test Valid auth code");
         pdqPaymentTransaction.validate();
         LogWriter.logMethodEnd("test Valid auth code");
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Test for authCode which is less than max length of the
    * authCode.
    */
   public void testInvalidAuthCodeForMinLength()
   {
      String authCode = "12";
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test valid auth code", params);
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
               authCode);
         LogWriter.logDebugMessage("test Valid auth code");
         pdqPaymentTransaction.validate();
         LogWriter.logErrorMessage("exception while validating auth code");
         fail("Exception araised while validating auth code");
         LogWriter.logMethodEnd("Test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("Successfully validated the auth code", true);
      }
   }

   /**
    * Test for authCode which is more than max length of the
    * authCode.
    */
   public void testInvalidAuthCodeForMaxLength()
   {
      String authCode = "12345678901234567890123456";
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test valid auth code", params);
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
               authCode);
         LogWriter.logDebugMessage("test Valid auth code");
         pdqPaymentTransaction.validate();
         LogWriter.logErrorMessage("exception while validating auth code");
         fail("Exception while validating auth code");
         LogWriter.logMethodEnd("test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Test null authCode .
    */
   public void testNullAuthCode()
   {
      String authCode = null;
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test invalid authcode", params);
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
               authCode);
         LogWriter.logDebugMessage("test invalid auth code");
         pdqPaymentTransaction.validate();
         fail("Exception while validating auth code");
         LogWriter.logMethodEnd("test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Test blank authCode .
    */
   public void testBlankAuthCode()
   {
      String authCode = "";
      try
      {
         Object[] params = new Object[] {paymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test invalid authcode", params);
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
               authCode);
         LogWriter.logDebugMessage("test invalid auth code");
         pdqPaymentTransaction.validate();
         LogWriter.logErrorMessage("error while validating auth code");
         fail("Exception while validating auth code");
         LogWriter.logMethodEnd("test auth code ");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Test for valid American express payment code.
    */
   public void testAmexPaymentTypeCode()
   {
      String authCode = "1234";
      String amexPaymentTypeCode = "AMERICAN_EXPRESS";
      try
      {
         Object[] params = new Object[] {amexPaymentTypeCode, transactionAmount, authCode };
         LogWriter.logMethodStart("test American express payment code", params);
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(amexPaymentTypeCode, transactionAmount, transactionCharge,
               authCode);
         LogWriter.logDebugMessage("Test American express Payment code");
         pdqPaymentTransaction.validate();


         LogWriter.logMethodEnd("test American express Payment code");
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Tests essential transaction data.
    */
   public void testEssentialTransactionData()
   {
      try
      {
         PDQPaymentTransaction pdqPaymentTransaction =
            new PDQPaymentTransaction(paymentTypeCode, transactionAmount, transactionCharge,
               "1234");
         LogWriter.logDebugMessage("Test essential transaction data");
         pdqPaymentTransaction.validate();

         EssentialTransactionData essentialTransactionData =
            pdqPaymentTransaction.getEssentialTransactionData();
         Money money = essentialTransactionData.getTransactionAmount();
         assertNotNull(money);
         assertNotNull(essentialTransactionData.getTransactionReference());
         assertNotNull(pdqPaymentTransaction.getAuthCode());
         assertNotNull(pdqPaymentTransaction.getTransactionAmount());
         assertNotNull(pdqPaymentTransaction.getTransactionCharge());
         assertNotNull(pdqPaymentTransaction.getPaymentTypeCode());
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }


}
