/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: Aug 11, 2009
 *
 * $Author: sindhushree.g
 *
 * $Log: $
*/
package com.tui.uk.payment.domain;

import com.tui.uk.payment.exception.PaymentValidationException;

import junit.framework.TestCase;

/**
 * This test case tests the GiftCard class.
 *
 * @author sindhushree.g
 *
 */
public class GiftCardTestCase extends TestCase
{

   /** Message for expected exception. */
   private static final String EXPECTED_EXCEPTION = "Expected an exception";

   /** The payment type code. */
   private static final String CARD_TYPE = "GiftCard";

   /** The GiftCard object. */
   private GiftCard giftCard;

   /**
    * Sets up the initial data.
    */
   protected void setUp()
   {
      giftCard = new GiftCard("6335860000000018".toCharArray(), "1124".toCharArray(),
         CARD_TYPE);
   }

   /**
    * Tests the purgeCardDetails method. This checks if the pan is null, after purging.
    */
   public void testPurgeCardDetailsForPan()
   {
      giftCard.purgeCardDetails();
      assertNull(giftCard.getPan());
   }

   /**
    * Tests the purgeCardDetails method. This checks if the cv2 is null, after purging.
    */
   public void testPurgeCardDetailsForCv2()
   {
      giftCard.purgeCardDetails();
      assertNull(giftCard.getCv2());
   }

   /**
    * Tests for the getPaymentTypeCode method.
    */
   public void testGetPaymentTypeCode()
   {
      assertEquals(CARD_TYPE, giftCard.getCardtype());
   }

   /**
    * Tests the getExpiryDate method.
    */
   public void testGetExpiryDate()
   {
      assertNull(giftCard.getExpiryDate());
   }

   /**
    * Tests the getMaskedCardNumber method.
    */
   public void testGetMaskedCardNumber()
   {
      assertNotNull(giftCard.getMaskedCardNumber());
   }

   /**
    * Tests the getNameOncard method.
    */
   public void testGetNameOncard()
   {
      assertNull(giftCard.getNameOncard());
   }

   /**
    * Tests for valid GiftCard.
    */
   public void testValidGiftCard()
   {
      try
      {
         giftCard.validate();
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception:" + pve.getMessage());
      }
   }

   /**
    * Tests for blank pan.
    */
   public void testBlankPanGiftCard()
   {
      try
      {
         giftCard = new GiftCard("".toCharArray(), "1124".toCharArray(), CARD_TYPE);
         giftCard.validate();
         fail(EXPECTED_EXCEPTION);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(true);
      }
   }

   /**
    * Tests for blank cv2.
    */
   public void testBlankCv2GiftCard()
   {
      try
      {
         giftCard = new GiftCard("6335860000000018".toCharArray(), "".toCharArray(),
            CARD_TYPE);
         giftCard.validate();
         fail(EXPECTED_EXCEPTION);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(true);
      }
   }

   /**
    * Tests for invalid pan.
    */
   public void testInvalidPanGiftCard()
   {
      try
      {
         giftCard = new GiftCard("63358600000000181".toCharArray(), "1124".toCharArray(),
            CARD_TYPE);
         giftCard.validate();
         fail(EXPECTED_EXCEPTION);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(true);
      }
   }

   /**
    * Tests for invalid cv2.
    */
   public void testInvalidCv2GiftCard()
   {
      try
      {
         giftCard = new GiftCard("6335860000000018".toCharArray(), "112".toCharArray(),
            CARD_TYPE);
         giftCard.validate();
         fail(EXPECTED_EXCEPTION);
      }
      catch (PaymentValidationException pve)
      {
         assertTrue(true);
      }
   }


}
