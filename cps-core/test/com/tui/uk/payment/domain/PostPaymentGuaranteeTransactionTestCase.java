/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile:PostPaymentGuaranteeTransactionTestCase.java $
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author: Vinodha.S$
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;

import junit.framework.TestCase;

import com.tui.uk.client.domain.EncryptedCard;
import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the PostPaymentGuaranteeTransaction class for valid and
 * invalid card numbers.
 */
public class PostPaymentGuaranteeTransactionTestCase extends TestCase
{

   /** Total amount. */
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100.00);

   /** The character array holing cv2. */
   private char[] cv2 = {'4', '4', '4' };

   /** The character array holding a valid card number. */
   private char[] validPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2', '1', '1', '1', '1' };

   /** The character array holding a invalid card number. */
   private char[] invalidPan =
      {'4', '4', '4', '4', '3', '3', '3', '3', '2', '2', '2', '2'};

   /** String holding expiry date. */
   private String expiryDate = "08/13";

   /** The transaction amount. */
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));

   /** The Card object. */
   private CnpCard card;

   /** The cardtype. */
   private String cardtype = "visa";

   /** The payment type code. */
   private String paymentTypeCode = "VC";

   /** The Constant GBP. */
   private static final String GBP = "GBP";

   /** The constant THREE. */
   private static final int THREE = 3;

   /**
    * Setting valid card details.
    */
   private void setValidCardDetails()
   {
      card = new CnpCard(validPan, "test", expiryDate, cv2, "aaa", cardtype, THREE, THREE);
      ((CnpCard) card).setCv2AVSEnabled(Boolean.FALSE);
   }

   /**
    * Setting invalid card details.
    */
   private void setInvalidCardDetails()
   {
      card = new CnpCard(invalidPan, "test", expiryDate, cv2, "aaa", cardtype, THREE, THREE);
      ((CnpCard) card).setCv2AVSEnabled(Boolean.FALSE);
   }

   /**
    * Checks whether the card number is valid .
    */
   public void testValidCard()
   {
      try
      {
         Object[] params = new Object[] {card, paymentTypeCode, transactionAmount };
         LogWriter.logMethodStart("test valid Card", params);
         LogWriter.logDebugMessage("test Valid Card Number");
         setValidCardDetails();
         PostPaymentGuaranteeTransaction postPaymentGuaranteeTransaction =
            new PostPaymentGuaranteeTransaction(card, paymentTypeCode, transactionAmount);
         postPaymentGuaranteeTransaction.validate();
         LogWriter.logMethodEnd("testValidCurrency");
      }
      catch (PaymentValidationException pve)
      {
         fail("Exception while validating the card number." + pve.getMessage());
      }
   }

   /**
    * Check whether the card number is Invalid .
    */
   public void testInvalidCard()
   {
      try
      {
         Object[] params = new Object[] {card, paymentTypeCode, transactionAmount };
         LogWriter.logMethodStart("test Invalid Card Number", params);
         setInvalidCardDetails();
         PostPaymentGuaranteeTransaction postPaymentGuaranteeTransaction =
            new PostPaymentGuaranteeTransaction(card, paymentTypeCode, transactionAmount);
         postPaymentGuaranteeTransaction.validate();
         LogWriter.logMethodEnd("test Invalid Card Number");
         LogWriter.logErrorMessage("exception while validating the card number");

         fail("Exception while validating");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the card number", true);
      }
   }

   /**
    * Check whether the card number is encrypted .
    */
   public void testGetEncryptedCardWithEncrypted()
   {
      Object[] params = new Object[] {card, paymentTypeCode, transactionAmount };
      LogWriter.logMethodStart("testEncryptedCard", params);
      setValidCardDetails();
      PostPaymentGuaranteeTransaction postPaymentGuaranteeTransaction =
         new PostPaymentGuaranteeTransaction(card, paymentTypeCode, transactionAmount);
      EncryptedCard encryptedCard = postPaymentGuaranteeTransaction.getEncryptedCard();
      String encryptedPan = encryptedCard.getEncryptedPan();
      encryptedCard.setIssueNumber("1");
      encryptedCard.setStartDate("02/08");
      encryptedCard.getExpiryDate();
      encryptedCard.getIssueNumber();
      encryptedCard.getNameOnCard();
      encryptedCard.getPostCode();
      encryptedCard.getStartDate();
      LogWriter.logMethodEnd("testEncryptedCard ");
      if (encryptedPan.equals(String.valueOf(card.getPan())))
      {
         LogWriter.logErrorMessage("exception testGetEncryptedCardWithEncrypted");
         fail("Card number is not encrypted");
      }
   }

   /**
    * Check whether the card number is encrypted .
    */
   public void testGetEncryptedCard()
   {
      Object[] params = new Object[] {card, paymentTypeCode, transactionAmount };
      LogWriter.logMethodStart("testGetEncryptedCardWithoutEncrypted", params);
      setValidCardDetails();
      PostPaymentGuaranteeTransaction postPaymentGuaranteeTransaction =
         new PostPaymentGuaranteeTransaction(card, paymentTypeCode, transactionAmount);
      EncryptedCard encryptedCard = postPaymentGuaranteeTransaction.getEncryptedCard();
      String encryptedPan = encryptedCard.getEncryptedPan();
      LogWriter.logMethodEnd("testEncryptedCard ");
      if (encryptedPan.equals(String.valueOf(card.getPan())))
      {
         LogWriter.logErrorMessage("exception testGetEncryptedCardWithoutEncrypted");
         fail("Card number is encrypted");
      }
   }

   /**
    * Check whether the card number is encrypted .
    */
   public void testGetEssentialTransactionData()
   {
      Object[] params = new Object[] {card, paymentTypeCode, transactionAmount };
      LogWriter.logMethodStart("testGetEssentialTransactionData", params);
      setValidCardDetails();
      PostPaymentGuaranteeTransaction postPaymentGuaranteeTransaction =
         new PostPaymentGuaranteeTransaction(card, paymentTypeCode, transactionAmount);
      EssentialTransactionData essentialTransactionData =
         postPaymentGuaranteeTransaction.getEssentialTransactionData();
      assertNotNull(essentialTransactionData.getTransactionAmount());
      assertNotNull(postPaymentGuaranteeTransaction.getCard());
      assertNotNull(postPaymentGuaranteeTransaction.getPaymentTypeCode());
      essentialTransactionData.getMerchantCopy();
      essentialTransactionData.getCustomerCopy();
      essentialTransactionData.setThreeDSecureAuthFlag(true);
      essentialTransactionData.getThreeDSecureAuthFlag();
      LogWriter.logMethodEnd("testGetEssentialTransactionData ");
   }
}
