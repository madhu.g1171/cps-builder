/*
 * Copyright (C)2008 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile:VoucherPaymentTransactionTestCase.java $
 *
 * $Revision: $
 *
 * $Date: 2008-06-27 $
 *
 * $Author: Vinodha.S$
 *
 * $Log: $
 */
package com.tui.uk.payment.domain;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.tui.uk.client.domain.EssentialTransactionData;
import com.tui.uk.client.domain.Money;
import com.tui.uk.payment.exception.PaymentValidationException;

/**
 * This class tests the VoucherPaymentTransaction class for
 * valid and invalid voucher codes.
 */
public class VoucherPaymentTransactionTestCase extends TestCase
{

   /** Total amount. */
   private static final BigDecimal TOTAL_AMOUNT = BigDecimal.valueOf(100.00);

   /** The transaction amount. */
   private Money transactionAmount = new Money(TOTAL_AMOUNT, Currency.getInstance(GBP));

   /** The payment type code. */
   private String paymentTypeCode = "VC";

   /** The Constant GBP. */
   private static final String GBP = "GBP";

   /**
    * Tests for a valid voucher code.
    */
   public void testVoucherCodeForNumeric()
   {
      String voucherCode = "12345678";
      try
      {
         Map<String, Money> voucherLineItem = new HashMap<String, Money>();
         voucherLineItem.put(voucherCode, transactionAmount);
         VoucherPaymentTransaction voucherPaymentTransaction =
            new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
         voucherPaymentTransaction.validate();
         assertTrue("successfully validated the voucher Code", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Tests for invalid voucher code which is less than 8 digits.
    */
   public void testInValidVoucherCode()
   {
      String voucherCode = "1234";
      try
      {
         Map<String, Money> voucherLineItem = new HashMap<String, Money>();
         voucherLineItem.put(voucherCode, transactionAmount);
         VoucherPaymentTransaction voucherPaymentTransaction =
            new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
         voucherPaymentTransaction.validate();
         fail("Exception while validating voucher code");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Tests for invalid voucher code which has alphabets.
    */
   public void testValidAlphabeticVoucherCode()
   {
      String voucherCode = "ABCDEFGH";
      try
      {
         Map<String, Money> voucherLineItem = new HashMap<String, Money>();
         voucherLineItem.put(voucherCode, transactionAmount);
         VoucherPaymentTransaction voucherPaymentTransaction =
            new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
         voucherPaymentTransaction.validate();
         assertTrue("successfully validated the voucher code", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("successfully validated the auth code");
      }
   }

   /**
    * Tests for invalid voucher code with special characters.
    */
   public void testInValidVoucherSpecialCharCode()
   {
      String voucherCode = "1234567$";
      try
      {
         Map<String, Money> voucherLineItem = new HashMap<String, Money>();
         voucherLineItem.put(voucherCode, transactionAmount);
         VoucherPaymentTransaction voucherPaymentTransaction =
            new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
         voucherPaymentTransaction.validate();
         fail("Exception while validating voucher code");
      }
      catch (PaymentValidationException pve)
      {
         assertTrue("successfully validated the auth code", true);
      }
   }

   /**
    * Tests for valid voucher code with both alphabets and numbers.
    */
   public void testValidVoucherAlphaNumeric()
   {
      String voucherCode = "12345678";
      try
      {
         Map<String, Money> voucherLineItem = new HashMap<String, Money>();
         voucherLineItem.put(voucherCode, transactionAmount);
         VoucherPaymentTransaction voucherPaymentTransaction =
            new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
         voucherPaymentTransaction.validate();
         assertTrue("successfully validated the voucher Code", true);
      }
      catch (PaymentValidationException pve)
      {
         fail("Unexpected exception while validation." + pve.getMessage());
      }
   }

   /**
    * Tests for valid EssentialTransactions.
    */
   public void testEssentialTransactionsDataForPaymentMethod()
   {
      String voucherCode = "1a2k3c4d";

      Map<String, Money> voucherLineItem = new HashMap<String, Money>();
      voucherLineItem.put(voucherCode, transactionAmount);
      VoucherPaymentTransaction voucherPaymentTransaction =
         new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
      EssentialTransactionData essentialTransactionData =
         voucherPaymentTransaction.getEssentialTransactionData();
      if (essentialTransactionData.getPaymentMethod() == voucherPaymentTransaction
         .getPaymentMethod())
      {
         assertTrue("successfully validated the ", true);
      }
      else
      {
         fail(" Test case Failed");
      }
   }

   /**
    * Tests for valid PaymentTypeCode.
    */
   public void testEssentialTransactionsDataForPaymentTypeCode()
   {
      String voucherCode = "1a2b3c4d";

      Map<String, Money> voucherLineItem = new HashMap<String, Money>();
      voucherLineItem.put(voucherCode, transactionAmount);
      VoucherPaymentTransaction voucherPaymentTransaction =
         new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
      EssentialTransactionData essentialTransactionData =
         voucherPaymentTransaction.getEssentialTransactionData();
      if (essentialTransactionData.getPaymentTypeCode().equalsIgnoreCase(voucherPaymentTransaction
         .getPaymentTypeCode()))
      {
         assertTrue("successfully validated  ", true);
      }
      else
      {
         fail("Failed");
      }
   }

   /**
    * Tests for valid TransactionAmount.
    */
   public void testEssentialTransactionsDataForVoucherLineItems()
   {
      String voucherCode = "1a2b3c4d";

      Map<String, Money> voucherLineItem = new HashMap<String, Money>();
      voucherLineItem.put(voucherCode, transactionAmount);
      VoucherPaymentTransaction voucherPaymentTransaction =
         new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
      EssentialTransactionData essentialTransactionData =
         voucherPaymentTransaction.getEssentialTransactionData();
      if (essentialTransactionData.getTransactionLineItems() == voucherPaymentTransaction
         .getVoucherLineItems())
      {
         assertTrue("successfully validated  ", true);
      }
      else
      {
         fail("Failed");
      }
   }

   /**
    * Tests for valid TransactionAmount.
    */
   public void testEssentialTransactionsDataForTransactionAmount()
   {
      String voucherCode = "1a2b3c4d";

      Map<String, Money> voucherLineItem = new HashMap<String, Money>();
      voucherLineItem.put(voucherCode, transactionAmount);
      VoucherPaymentTransaction voucherPaymentTransaction =
         new VoucherPaymentTransaction(paymentTypeCode, transactionAmount, voucherLineItem);
      EssentialTransactionData essentialTransactionData =
         voucherPaymentTransaction.getEssentialTransactionData();
      if (essentialTransactionData.getTransactionAmount() == voucherPaymentTransaction
         .getTransactionAmount())
      {
         assertTrue("successfully validated  ", true);
      }
      else
      {
         fail("Failed");
      }
   }

}
