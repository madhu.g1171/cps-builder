/*
 * Copyright (C)2009 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: $
 *
 * $Revision: $
 *
 * $Date: $
 *
 * $Author: $
 *
 * $Log: $
 */
package com.tui.uk.payment.processor;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import junit.framework.TestCase;

import com.tui.uk.exception.ValidationException;
import com.tui.uk.log.LogWriter;
import com.tui.uk.payment.dispatcher.DispatcherConstants;
import com.tui.uk.payment.domain.PaymentConstants;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.exception.PaymentValidationException;
import com.tui.uk.payment.exception.PostPaymentProcessorException;
import com.tui.uk.utility.TestDataSetup;

/**
 *
 * TestCase for FirstChoicePostPaymentProcessor Class.
 *
 */
public class FirstChoicePostPaymentProcessorTestCase extends TestCase
{
   /** variable to hold the UUID. */
   private static UUID uuid;

   /** The request parameter map. */
   private Map<String, String[]> requestParameterMap = new HashMap<String, String[]>();

   /** The exception constant. */
   private static final String PAYMENT_0_PAYMENTTYPECODE = "payment_0_paymenttypecode";

   /** The term and condition constant. */
   private static final String AGREE_TERM_AND_CONDITION = "agreeTermAndCondition";

   /** The true. */
   private static final String TRUE = "true";

   /** The transaction amount constant. */
   private static final String PAYMENT_0_TRANSAMT = "total_transamt";

   /** The calculated total transaction amount. */
   private static final String PAYMENT_TRANSAMT = "512.5";

   /** The full cost constant. */
   private static final String FULL_COST = "FullCost";

   /**The name on card.*/
   private static final String NAME_ON_CARD = "test";

   /** Unexpected exception constant. */
   private static final String UNEXPECTED_EXCEPTION = "Unexpected Exception";

   /**The payment data variable.*/
   private PaymentData paymentData;

   /**
    * Sets request path for the class under test.
    *
    * @throws Exception as the base class does.
    */
   @Override
   protected void setUp() throws Exception
   {
      super.setUp();

      requestParameterMap.put("balanceType", new String[] {"PC"});

      requestParameterMap.put("payment_totalTrans", new String[] {"1"});
      requestParameterMap.put("payment_0_type", new String[] {"VISA|Dcard"});
      requestParameterMap.put(PAYMENT_0_PAYMENTTYPECODE, new String[] {"VISA"});
      requestParameterMap.put("payment_0_cardNumber", new String[] {"4242425000000009"});
      requestParameterMap.put("payment_0_nameOnCard", new String[] {NAME_ON_CARD});
      requestParameterMap.put("payment_0_expiryMonth", new String[] {"12"});
      requestParameterMap.put("payment_0_expiryYear", new String[] {"15"});
      requestParameterMap.put("payment_0_securityCode", new String[] {"444"});

      requestParameterMap.put("payment_0_issueNumber", new String[] {"22"});
      requestParameterMap.put("payment_0_startMonth", new String[] {"04"});
      requestParameterMap.put("payment_0_startYear", new String[] {"07"});
      requestParameterMap.put("selectedCard", new String[] {"VISA"});

      requestParameterMap.put(PaymentConstants.PAYMENT
         + PaymentConstants.PINPADTRANSACTIONCOUNT, new String[] {"1"});
      requestParameterMap.put("promotionalCode", new String[] {"11"});
      requestParameterMap.put(DispatcherConstants.BALANCE_TYPE, new String[] {"FullBalance"});
      requestParameterMap.put("tourOperatorTermsAccepted", new String[]{"on"});
      requestParameterMap.put("privacyPolicyAccepted", new String[]{"on"});
      paymentData = new PaymentData(TestDataSetup.getBookingInfo());

   }

   /** To test the valid case.
    * @throws ValidationException the validation exception.
    * */
   public void testValidProcess() throws ValidationException
   {
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(AGREE_TERM_AND_CONDITION, new String[] {TRUE});
      requestParameterMap.put(DispatcherConstants.TOKEN,
         new String[] {uuid.toString()});
      requestParameterMap.put(PAYMENT_0_TRANSAMT, new String[] {PAYMENT_TRANSAMT});
      requestParameterMap.put("depositType", new String[] {FULL_COST});

      FirstChoicePostPaymentProcessor processor = new FirstChoicePostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage(UNEXPECTED_EXCEPTION + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage(UNEXPECTED_EXCEPTION + pve.getMessage());
      }
   }

   /** To test the invalid case.
    * @throws ValidationException the validation exception.
    * */
   public void testInValidProcess() throws ValidationException
   {
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(DispatcherConstants.TOKEN, new String[] {uuid.toString()});

      requestParameterMap.put(PAYMENT_0_TRANSAMT, new String[] {"105"});
      requestParameterMap.put("depositType", new String[] {FULL_COST});

      FirstChoicePostPaymentProcessor processor =
         new FirstChoicePostPaymentProcessor(paymentData,
            requestParameterMap);
      try
      {
         processor.process();
         fail("Should have got an exception");
      }
      catch (PostPaymentProcessorException ppe)
      {
         assertTrue("Please agree to our Terms & Conditions.", true);
         LogWriter.logErrorMessage("" + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage(UNEXPECTED_EXCEPTION + pve.getMessage());
      }
   }

   /**To test the invalid case for pre process.
    * */
   public void testInValidPreProcess()
   {
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(DispatcherConstants.TOKEN,
         new String[] {uuid.toString()});
      requestParameterMap.put(PAYMENT_0_TRANSAMT, new String[] {"105"});
      requestParameterMap.put("depositType", new String[] {FULL_COST});

      FirstChoicePostPaymentProcessor processor = new FirstChoicePostPaymentProcessor(
         paymentData, requestParameterMap);
      try
      {
         processor.preProcess();
         fail("Should have got an exception");
      }
      catch (PostPaymentProcessorException ppe)
      {
         assertTrue("Please agree to our Terms & Conditions.", true);
         LogWriter.logErrorMessage("" + ppe.getMessage());
      }
   }
   /**
    * This method tests the null depositType.
    * @throws ValidationException the validation exception.
    */
   public void testNullDepositTypeCase() throws ValidationException
   {
      uuid = PaymentStore.getInstance().newPaymentData(paymentData);
      requestParameterMap.put(AGREE_TERM_AND_CONDITION, new String[] {TRUE});
      requestParameterMap.put(DispatcherConstants.TOKEN, new String[] {uuid.toString()});
      requestParameterMap.put(PAYMENT_0_TRANSAMT, new String[] {PAYMENT_TRANSAMT});

      FirstChoicePostPaymentProcessor processor = new FirstChoicePostPaymentProcessor(paymentData,
            requestParameterMap);
      try
      {
         processor.preProcess();
         processor.process();
      }
      catch (PostPaymentProcessorException ppe)
      {
         ppe.printStackTrace();
         LogWriter.logErrorMessage(UNEXPECTED_EXCEPTION + ppe.getMessage());
      }
      catch (PaymentValidationException pve)
      {
         pve.printStackTrace();
         LogWriter.logErrorMessage(UNEXPECTED_EXCEPTION + pve.getMessage());
      }
   }
}