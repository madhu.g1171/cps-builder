/*
 * Copyright (C)2011 TUI UK Ltd
 *
 * TUI UK Ltd, Columbus House, Westwood Way, Westwood Business Park,
 * Coventry, United Kingdom CV4 8TT
 *
 * Telephone - (024)76282828
 *
 * All rights reserved - The copyright notice above does not evidence any
 * actual or intended publication of this source code.
 *
 * $RCSfile: BookingComponent.java,v $
 *
 * $Revision: 1.1$
 *
 * $Date: 2011-05-13 08:08:25$
 *
 * $author: sindhushree.g$
 *
 * $Log : $
 *
 */
package com.tui.uk.payment.processor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import junit.framework.TestCase;
import com.tui.uk.client.domain.BookingComponent;
import com.tui.uk.client.domain.BookingData;
import com.tui.uk.client.domain.ContactInfo;
import com.tui.uk.client.domain.Money;
import com.tui.uk.client.domain.PackageType;
import com.tui.uk.domain.BookingInfo;
import com.tui.uk.domain.TransactionTrackingData;
import com.tui.uk.payment.domain.CnpCard;
import com.tui.uk.payment.domain.DataCashPaymentTransaction;
import com.tui.uk.payment.domain.Payment;
import com.tui.uk.payment.domain.PaymentData;
import com.tui.uk.payment.domain.PaymentStore;
import com.tui.uk.payment.domain.PaymentTransaction;
import com.tui.uk.utility.TestDataSetup;

/**
 * This test case is responsible for testing FraudScreeningProcessor.
 *
 * @author sindhushree.g
 *
 */
public class FraudScreeningProcessorImplTestCase extends TestCase
{

   /** The security code length. */
    private static final int SECURITY_CODE_LENGTH = 3;

   /** The currency. */
   private static final Currency CURRENCY_GBP = Currency.getInstance("GBP");

   /** CARD NAME .*/
   private static final String CARD_NAME = "MASTERCARD";

   /** client application id .*/
   private static final String CLIENT_ID = "ThomsonBYOef6ec158db91403ebb17";

   /**
    * This method tests for processFraudScreening method.
    */
   public void testProcessFraudScreening()
   {
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(PackageType
      .PreDefinedPackage));
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCard(), CLIENT_ID);
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui");

      ContactInfo contactInfo = new ContactInfo();
      contactInfo.setEmailAddress("savitha.h@sonata-software.com");
      paymentData.getBookingInfo().getBookingComponent().setContactInfo(contactInfo);

      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      assertTrue(fraudScreeningProcessor.processFraudScreening());
    }

   /**
    * Tests the updateFailureCardData method.
    */
   public void testUpdateFailureCardData()
   {
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(PackageType
         .PreDefinedPackage));
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCard(), CLIENT_ID);
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui2");
      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      fraudScreeningProcessor.updateFailureCardData();
      assertNotNull(paymentData.getFailedCardDataList().get(0));
    }

   /**
    * This method tests for updateBookingDetails method.
    */
   public void testUpdateBookingDetails()
   {
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(PackageType
              .PreDefinedPackage));
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCard(), CLIENT_ID);
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui33");
      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      fraudScreeningProcessor.processFraudScreening();
      fraudScreeningProcessor.updateBookingDetails("123456", new Money(BigDecimal.TEN,
      CURRENCY_GBP));
      assertTrue(true);
   }

   /**
    * This method is to test fraudscreening reject response.
    */
   public void testFraudScreeningRejectResponse()
   {
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(PackageType
         .PreDefinedPackage));
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCardForReject(), "ThomsonBYOef6ec158db91403ebb17");
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui4");

      ContactInfo contactInfo = new ContactInfo();
      contactInfo.setEmailAddress("naresh.gl@sonata-software.com");
      paymentData.getBookingInfo().getBookingComponent().setContactInfo(contactInfo);

      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      assertFalse(fraudScreeningProcessor.processFraudScreening());
   }

   /**
    * This method is to test fraudscreening review response.
    *
    */
   public void testFraudScreeningReviewResponse()
   {
      PaymentData paymentData = new PaymentData(TestDataSetup.getBookingInfo(
         PackageType.PreDefinedPackage));
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCardForReview(), "ThomsonBYOef6ec158db91403ebb17");
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui5");

      ContactInfo contactInfo = new ContactInfo();
      contactInfo.setEmailAddress("swathi.bp@sonata-software.com");
      paymentData.getBookingInfo().getBookingComponent().setContactInfo(contactInfo);

      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      assertTrue(fraudScreeningProcessor.processFraudScreening());

    }

   /**
    * This method tests for processFraudScreening method with bookingReference as  null.
    */
   public void testProcessFraudScreeningWithBookingReferenceNull()
   {
      BookingComponent bookingComponent = TestDataSetup.getBookingComponent1();
      BookingData bookingData = new BookingData(null, "ok", new Date());
      bookingComponent.setBookingData(bookingData);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      TransactionTrackingData trackingData = new TransactionTrackingData("", "");
      bookingInfo.setTrackingData(trackingData);
      PaymentData paymentData = new PaymentData(bookingInfo);
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCard(), CLIENT_ID);
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui");
      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      assertTrue(fraudScreeningProcessor.processFraudScreening());
}

   /**
    * This method tests for updateBookingDetails method with BookingReference null.
    */
   public void testUpdateBookingDetailsWithBookingReferenceNull()
   {
      BookingComponent bookingComponent = TestDataSetup.getBookingComponent1();
      BookingData bookingData = new BookingData(null, "ok", new Date());
      bookingComponent.setBookingData(bookingData);
      BookingInfo bookingInfo = new BookingInfo(bookingComponent);
      TransactionTrackingData trackingData = new TransactionTrackingData("", "");
      bookingInfo.setTrackingData(trackingData);
      PaymentData paymentData = new PaymentData(bookingInfo);
      String token = PaymentStore.getInstance().newPaymentData(paymentData).toString();
      Payment payment = new Payment();
      List<PaymentTransaction> paymentTransactionList = new ArrayList<PaymentTransaction>();
      PaymentTransaction paymentTransaction = new DataCashPaymentTransaction(CARD_NAME,
         new Money(BigDecimal.TEN, CURRENCY_GBP), new Money(BigDecimal.ONE, CURRENCY_GBP),
         getCard(), CLIENT_ID);
      paymentTransactionList.add(paymentTransaction);
      payment.setPaymentTransaction(paymentTransactionList);
      paymentData.setPayment(payment);
      paymentData.setBookingSessionId("57tui33");
      FraudScreeningProcessor fraudScreeningProcessor = new FraudScreeningProcessorImpl(token);
      fraudScreeningProcessor.processFraudScreening();
      fraudScreeningProcessor.updateBookingDetails("123456", new Money(BigDecimal.TEN,
         CURRENCY_GBP));
      assertTrue(true);
   }

   /**
    * gets the card object.
    *
    * @return cnpCard the card object.
    */
   private CnpCard getCard()
   {
      CnpCard card = new CnpCard("1000011000000005".toCharArray(), "test",
      "11/12", "444".toCharArray(), "lu2 9tp", "Mastercard", 0, SECURITY_CODE_LENGTH);
      return card;
   }

   /**
    *This method gives card number for testing reject response from Fraudscreeningstub.
    *
    * @return cnpCard
    */
    private CnpCard getCardForReject()
    {
       CnpCard card = new CnpCard("1000011100000004".toCharArray(), "test",
       "11/12", "444".toCharArray(), "lu2 9tp", "Mastercard", 0, SECURITY_CODE_LENGTH);
       return card;
    }

   /**
    *This method gives card number for testing review response from Fraudscreening stub.
    *
    * @return cnpCard
    */
   private CnpCard getCardForReview()
   {
      CnpCard card = new CnpCard("1000350000000007".toCharArray(), "test",
      "11/12", "444".toCharArray(), "lu2 9tp", "Mastercard", 0, SECURITY_CODE_LENGTH);
      return card;
   }

}
