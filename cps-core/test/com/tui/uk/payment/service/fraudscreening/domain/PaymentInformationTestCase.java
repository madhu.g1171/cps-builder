package com.tui.uk.payment.service.fraudscreening.domain;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import com.tui.uk.payment.service.fraudscreening.FraudScreeningCardData;

import junit.framework.TestCase;

/**
*
* TestCase for FraudScreeningRequest Class.
*
*/
//CHECKSTYLE:OFF
public class PaymentInformationTestCase extends TestCase
{
   /** The Constant for AuthInfo. */
   AuthInfo authInfo;

   /** The Constant for FraudScreeningCard. */
   FraudScreeningCard fraudScreeningCard;

   /** The Constant for ContactInformation. */
   ContactInformation contactInformation;

   /** The Constant for FraudScreeningCardData. */
   FraudScreeningCardData fraudScreeningCardData;

   /** The Constant for FraudScreeningCardData. */
   List<FraudScreeningCardData> fraudScreeningCardDatalist;
    /**
        * Setup method for PaymentInformation.
        */
       @Override

   protected void setUp()
   {
            authInfo = new AuthInfo("004587", "ACCEPTED", "Y", "4100201011534793",
                   "ThomsonBYO962027918ea503ad3", "001", "","","","","","");

            fraudScreeningCard = new
            FraudScreeningCard("4242425000000009".toCharArray(),"Test","TODO");

            contactInformation =
               new ContactInformation("18, Twyford Drive", "","Luton","","","","");
            fraudScreeningCardData = new
            FraudScreeningCardData(authInfo, fraudScreeningCard, "GBP",new BigDecimal(307.50),
                    new BigDecimal(7.50), contactInformation);

            fraudScreeningCardDatalist =
               new ArrayList<FraudScreeningCardData>();


       }

     /**
        * Testing getter and setter.
        * */
       public void testSucessCard()
       {

          String responseXml = "<paymentInformation><cardInfo><cardholderName>Test</cardholderName>" +
                  "<cardAmount>307.5</cardAmount><cardCharge>7.5</cardCharge>" +
                  "<cardNumber>424242******0009</cardNumber><cardScheme></cardScheme>" +
                  "<currency>GBP</currency><cardIssuer></cardIssuer><cardIssuedCountry></cardIssuedCountry>" +
                  "<expireDate>TODO</expireDate><billingAddress>18, Twyford Drive</billingAddress>" +
                  "<billingAddress2></billingAddress2><billingCounty></billingCounty>" +
                  "<billingCity>Luton</billingCity><billingZipCode></billingZipCode>" +
                  "<billingCountry></billingCountry><billingPhoneNumber></billingPhoneNumber>" +
                  "<authorizationInformation><authorizationCode>004587</authorizationCode>" +
                  "<avsResponse>ACCEPTED</avsResponse><cvvResponse></cvvResponse>" +
                  "<authorizedBy3dSecurity>Y</authorizedBy3dSecurity>" +
                  "<paymentGatewayreferenceNumber>4100201011534793</paymentGatewayreferenceNumber>" +
                  "<merchantReferenceNumber>ThomsonBYO962027918ea503ad3</merchantReferenceNumber>" +
                  "<paymentGatewayResponseCode>001</paymentGatewayResponseCode><referralCode></referralCode>" +
                  "</authorizationInformation></cardInfo><promoId>00001234</promoId>" +
                  "<promoAmount>50.00</promoAmount></paymentInformation>";


           fraudScreeningCardDatalist.add(fraudScreeningCardData);

           PaymentInformation paymentInformation =
               new PaymentInformation(fraudScreeningCardDatalist,"00001234","50.00");

         assertEquals(responseXml, paymentInformation.toString(true));
       }
         /**
          * Testing getter and setter.
          * */
         public void testFailedCard()
         {

         AuthInfo authInfo_1 = new AuthInfo("004587", "REJECTED", "N", "4100201011534793",
                 "ThomsonBYO962027918ea503ad3", "004", "","","","","","");

         FraudScreeningCard fraudScreeningCard_1 = new
          FraudScreeningCard("4242425000000009".toCharArray(),"Test","TODO");

         ContactInformation contactInformation_1 =
             new ContactInformation("18, Twyford Drive", "","Luton","","","","");
         FraudScreeningCardData fraudScreeningCardData_1 = new
          FraudScreeningCardData(authInfo_1, fraudScreeningCard_1, "GBP",new BigDecimal(307.50),
                  new BigDecimal(7.50), contactInformation_1);

         fraudScreeningCardDatalist.add(fraudScreeningCardData_1);


         PaymentInformation paymentInformation_1 =
             new PaymentInformation(fraudScreeningCardDatalist,"00001234","50.00");

         String failedResponseXml = "<paymentInformation>"+
                "<failedCardInfo><cardholderName>Test</cardholderName><cardAmount>307.5</cardAmount>" +
                "<cardCharge>7.5</cardCharge><cardNumber>424242******0009</cardNumber>" +
                "<cardScheme></cardScheme><currency>GBP</currency><cardIssuer></cardIssuer>" +
                "<cardIssuedCountry></cardIssuedCountry><expireDate>TODO</expireDate>" +
                "<billingAddress>18, Twyford Drive</billingAddress><billingAddress2></billingAddress2>" +
                "<billingCounty></billingCounty><billingCity>Luton</billingCity><billingZipCode>" +
                "</billingZipCode><billingCountry></billingCountry><billingPhoneNumber></billingPhoneNumber>" +
                "<authorizationInformation><authorizationCode>004587</authorizationCode>" +
                "<avsResponse>REJECTED</avsResponse><cvvResponse></cvvResponse>" +
                "<authorizedBy3dSecurity>N</authorizedBy3dSecurity>" +
                "<paymentGatewayreferenceNumber>4100201011534793</paymentGatewayreferenceNumber>" +
                "<merchantReferenceNumber>ThomsonBYO962027918ea503ad3</merchantReferenceNumber>" +
                "<paymentGatewayResponseCode>004</paymentGatewayResponseCode><referralCode></referralCode>" +
                "</authorizationInformation></failedCardInfo>" +
                "<promoId>00001234</promoId><promoAmount>50.00</promoAmount></paymentInformation>";

        assertEquals(failedResponseXml, paymentInformation_1.toString(true));

       }

}
