var indexForCCResponse;
var indexForROResponse;
/**********Ajax functions************/

function updatePriceBeatAndDiscountValue(name)
{
   var params='';
   var tomcatInstance = $("tomcatInstance").value;
   if (checkForPaymentMade(name))
   {
      //var url = "discount";
      if (name == "priceBeat")
      {
         if($("priceBeat") != null)
         {
            discountVal = $('priceBeat').value;
            params= "priceBeat="+discountVal;
            discountType='Price Beat / Match';
         }
      }
      if (name == "todaySaving")
      {
         discountVal= $('todaySaving').value;
         params+="todaySaving="+discountVal;
         discountType= "Today's Savings";
      }
      balanceType=getBalanceType();
      var token = $("token").value;
      params+="&balanceType="+balanceType+"&token="+token;
      var url="/cps/discount?"+params;
      url = uncache(url);
      url += "&tomcat="+tomcatInstance;
      var request = new Ajax.Request(url,{method:"GET", onSuccess:showDiscountResponse});
   }
}

function showDiscountResponse(request)
{
   if ((request.readyState == 4) && (request.status == 200))
   {
       var respContains= request.responseText;
       var temp=respContains.split("|");
       var discountPercentage=0.0;
       var totalAmount='';
       var discountAmt=temp[0];
       discountPercentage=temp[1];
       totalAmount=temp[2];
       payableAmount=temp[3];
       totalAmount=(1*totalAmount).toFixed(2);
       discountPercentage=(discountAmt/totalAmount)*100;
       discountPercentage=discountPercentage.toFixed(2);
       //Update related fields after discount
       PaymentInfo.totalAmount = totalAmount;
       //PaymentInfo.basePayableAmount = totalAmount;
       PaymentInfo.basePayableAmount = payableAmount;
       amtRec = payableAmount;
       PaymentInfo.calculatedPayableAmount = payableAmount;
       if (PaymentInfo.depositType == null)
          PaymentInfo.calculatedTotalAmount = totalAmount;
       depositAmountsMap.put(bookingConstants.FULL_COST, PaymentInfo.totalAmount);
       refreshToDefault();

       updatePaymentInfoForMultipleTransactions();
       intialUpdatePaymentInfo();
       var deposCardChargeDivs =  $$('.depositAmount');
       if($("fullCost") && $("totalholidaycost"))
         displayFullCost(PaymentInfo.totalAmount);

       if (PaymentInfo.depositType != null)
          refreshToDefaultDepositOptions(PaymentInfo.totalAmount);
       displayTotalAmounts(bookingConstants.TOTAL_CLASS);
       updateDiscountsInSummaryPanel(PaymentInfo.totalAmount,discountAmt,discountType,discountPercentage);
       displayAmt();
       updateLastTransactionAmount();
    }
}

/*
 * This function calls promCodeServlet to calculate
 * Promotional code discounts and gets calculated amount
 * and discount amount.
*/
function updatePromotionalCode()
{
   var name = "Promotional Code";
   var tomcatInstance = $("tomcatInstance").value;
   if (checkForPaymentMade(name))
   {
      var promCode = $("promotionalCode").value;
      if (promCode == "")
      {
         return SetFocus('Please enter a valid Promotional Code. It accepts only Alphabets, Numerics and Hyphen(-)', $("promotionalCode"));
      }
      var balanceType;
      if ($('fullDeposit'))
      {
         if ($('fullDeposit').checked)
            balanceType = "FullBalance";
         else
            balanceType = "Deposit";
      }
      var token = $("token").value;
      var url="/cps/promCodeServlet?promotionalCode="+promCode+"&balanceType="+balanceType+"&token="+token;
      url = uncache(url);
      url += "&tomcat="+tomcatInstance;
      var request = new Ajax.Request(url,{method:"GET", onSuccess:responseUpdatePromotionalCodeDiscount});
   }
}

function responseUpdatePromotionalCodeDiscount(request)
{
   if ((request.readyState == 4) && (request.status == 200))
   {
      var temp=request.responseText;
      temp=temp.split("|");
      var promCodeDiscount= temp[0];
      var totalAmount=temp[1];
      if(parseFloat(promCodeDiscount))
      {
         var priceBeatObj=$("priceBeat");
         var maxAllowedPriceBeatValue = 0;
         if ($("maxPriceBeatAmount"))
         {
            var maxAllowedPriceBeatObj = $("maxPriceBeatAmount");
            maxAllowedPriceBeatValue = (maxAllowedPriceBeatObj.value > 0 || maxAllowedPriceBeatObj.value != "") ? maxAllowedPriceBeatObj.value : 0;
         }

         $('promoText').style.display='block';
         $('promoText').innerHTML = "Promotional Discount";
         $('promoCodeDiscount').style.display='block';
         $('promoCodeDiscount').innerHTML="-&pound; "+ promCodeDiscount;
         $('isPromoCodeApplied').value ="true";
         $('promoDiscount').value =promCodeDiscount;
         if ($('promoError').innerHTML != "")
         {
            $('promoError').innerHTML= "";
            $('promoError').style.display='none';
         }
         if (priceBeatObj !=undefined && priceBeatObj.value != "")
         {
            if(parseFloat(1*priceBeatObj.value + 1*promCodeDiscount)>parseFloat(maxAllowedPriceBeatValue))
            {
               clearAndFocus(maxAlert,priceBeatObj);
               return false;
            }
         }
         else
         {
            if(priceBeatObj && (parseFloat(promCodeDiscount)>parseFloat(maxAllowedPriceBeatValue)))
            {
            clearAndFocus(maxAlert,priceBeatObj);
            return false;
            }
         }
         $('totalAmount').innerHTML="&pound; "+totalAmount;
         $('promoText1').innerHTML="Amount of -&pound;"+promCodeDiscount+" has been deduced from the total cost.";
         $('promoText1').style.display='block';
         var transAmount, totAmount;

         PaymentInfo.totalAmount = totalAmount;
         PaymentInfo.basePayableAmount = totalAmount;
         if (PaymentInfo.depositType == null)
            PaymentInfo.calculatedTotalAmount = totalAmount;
         depositAmountsMap.put(bookingConstants.FULL_COST, PaymentInfo.totalAmount);
         refreshToDefault();
         updatePaymentInfoForMultipleTransactions();
         var deposCardChargeDivs =  $$('.depositAmount');
         if($("fullCost") && $("totalholidaycost"))
           displayFullCost(PaymentInfo.totalAmount);
         refreshToDefaultDepositOptions(PaymentInfo.totalAmount);
         displayTotalAmounts(bookingConstants.TOTAL_CLASS);
         displayAmt();
         updateLastTransactionAmount();
      }
      else
      {
         $('totalAmount').innerHTML=currencySymbol+totalAmount;
         $('isPromoCodeApplied').value ="false";
         $('promoText').style.display='none';
         $('promoCodeDiscount').style.display='none';
         $('promoError').style.display='block';
         $('promoError').innerHTML= promCodeDiscount;
      }
   }
}

var radioMaxCount = 0;
function generateTransactionRadios(index)
{
      var token = $("token").value;
      var tomcatInstance = $("tomcatInstance").value;
      var url="/cps/RefundOptionServlet?transactionIndex="+index+"&token="+token;
      url = uncache(url);
      url += "&tomcat="+tomcatInstance;
      indexForROResponse = index;
      var request = new Ajax.Request(url,{method:"GET", onSuccess: responseUpdateRefundOptions});
}

function responseUpdateRefundOptions(request)
{
   var requiredFields_rfndCard = '';
   var index = indexForROResponse;
   if ((request.readyState == 4) && (request.status == 200))
   {
      var temp = request.responseText;
      var responseTextValue = temp.split("~");
      radioMaxCount = responseTextValue[0];
      requiredFields_rfndCard += responseTextValue[1];
      $("cardRefundArea"+index).innerHTML = requiredFields_rfndCard;
   }
}

function displayFullCost(amount)
{
   $("totalholidaycost").innerHTML = formatDepositAmount(amount);
   $("fullCost").innerHTML = formatDepositAmount(amount);
   depositAmountsMap.put(bookingConstants.FULL_COST, amount);
}

function uncache(url)
{
   var d = new Date();
   var time = d.getTime();
   var newUrl;
   if (url.indexOf("?") != -1)
      newUrl = url + "&time="+time;
   else
      newUrl = url + "?time="+time;

   return newUrl;
}
/*********************AJAX helpers ******************/
///////////////////////////////////functions related to Updating Summary panel //////////////////////////////////////////////
/** This function is responsible for updating totalAmount in summary panel
*   @param totalAmount -totalamount to be updated in summary panel
*/
function updateTotalAmtInSummaryPanel(totalAmount)
{
   if ($('totalAmount'))
   {
    if((parseFloat(totalAmount).toFixed(2)).length>6)
    {
       var totalAmountLength = totalAmount.length;
       var count = (""+parseInt(totalAmount/1000)).length;
       totalAmount=parseInt(""+(totalAmount/1000))+","+totalAmount.substring(count, totalAmountLength);
    }
    $('totalAmount').innerHTML = "&pound;"+totalAmount;
   }
}

/** This function is responsible for updating card charges in summary panel
*   @params cardCharge -represents cardChargeAmt to be displayed for the transaction
*/
function updateCardChargesInSummaryPanel(cardCharge)
{
    if($('cardChargeText'))
     {
         if(cardCharge > 0)
        {
           $('cardChargeAmount').innerHTML="&pound;"+parseFloat(cardCharge).toFixed(2);
           $('cardChargeText').style.display='block';
           $('cardChargeAmount').style.display='block';
        }
        else
        {
           $('cardChargeText').style.display='none';
           $('cardChargeAmount').style.display='none';
         }
    }
}

/** This function updates summary panel with discount details
*  @param totalAmount- Total payable amount
*         discountAmt - discount given for the package
*         discountType - Either price beat or todays saving
*         discountPercentages - % of discount given on total amount
*/
function updateDiscountsInSummaryPanel(totalAmount,discountAmt,discountType,discountPercentage)
{
    if($('deposit') && $('deposit') != null)
    {
      $('outstandingbalance').innerHTML=parseFloat(totalAmount-1*stripChars($('deposit').innerHTML,',')).toFixed(2);
    }

    if($('priceBeatText'))
    {
      if(discountAmt>0.0)
       {
         $('priceBeatText').innerHTML=discountType;
         $('priceBeatAmount').innerHTML="- &pound;"+discountAmt;
         $('priceBeatPercentageText').innerHTML="Total discount %";
         $('priceBeatPercentage').innerHTML=discountPercentage;


         $('priceBeatPercentageText').style.display='block';
         $('priceBeatPercentage').style.display='block';
         $('priceBeatText').style.display='block';
         $('priceBeatAmount').style.display='block';
       }
      else
      {
         $('priceBeatPercentageText').style.display='none';
         $('priceBeatPercentage').style.display='none';
         $('priceBeatText').style.display='none';
         $('priceBeatAmount').style.display='none';
         if ($("todaySaving"))
            $("todaySaving").disabled = false;
         if ($("priceBeat"))
            $("priceBeat").disabled = false;
      }
    }
}
///////////////////////////////////End of functions related to Updating Summary panel //////////////////////////////////////////////

///////////////////////////////////functions related to Updating Payment panel //////////////////////////////////////////////
function getTransactionAmountWithOutcardCharge(index)
{
   var totamtWithoutCardCharge;
   totamtWithoutCardCharge = $("transamt_carry"+index).value;
   if(totamtWithoutCardCharge.length <=0 || totamtWithoutCardCharge==0)
   {
    totamtWithoutCardCharge = $("transamt"+index).value;
   }
   totamtWithoutCardCharge = stripChars(totamtWithoutCardCharge,currencySymbol+''+','+' ');
   return totamtWithoutCardCharge;
}

/** This function is responsible for  Displaying card charges and transactionAmount in payment panel
*  @param index -represents index of the transaction
*         cardChargeAmt -represents cardChargeAmt to be displayed for the transaction
*         transactionAmount -represents transactionAmount to be displayed for the transaction
*/
function displayCardChargesAndTransAmount(index,cardChargeAmt,transactionAmount)
{
   //Code to display card charge and transaction amount in payment panel
   if ($('payment_'+index+'_chargeIdAmount') &&$("transamt"+index).value.substring(1)!="")
   {
       cardChargeAmt = parseFloat(cardChargeAmt).toFixed(2);
      if(cardChargeAmt<0)
      {
        //If card charge amount is -ve, we need absolute value
        cardChargeAmt = Math.abs(cardChargeAmt);
      }

      //Update hidden field
      $('payment_'+index+'_chargeIdAmount').value = cardChargeAmt;

      //Display on payment panel
      $('cardChargePercentage'+index).innerHTML = currencySymbol+cardChargeAmt;

      var  totcaramt = stripChars($("transamt"+index).value,currencySymbol+''+','+' ');
      totcaramtperval =1*totcaramt + 1*cardChargeAmt;
      //Display transaction amount in payment panel ( "Total amount charged in this transaction:")
      $("tottransamt"+index).innerHTML=currencySymbol+parseFloat(totcaramtperval).toFixed(2);
      $("total_amount_"+index).value=totcaramtperval;

      //Add cardChargeAmt to total amount due to be displayed in Accumulation section
      totamtdue += cardChargeAmt;
   }
   showPayDetails();
}

/* Function related to updating all paydetails with totalAmount
*
*/
function  showUpdatedPayDetails(totalAmount)
{
   if((!$("fullCost")) || $("fullDeposit").checked)
   {
 /*Delegate it to payment details showPayDetails() */
    var amtdue = amtRec-collectTotalAmountPaid();
   totamtstilldue=parseFloat(amtRec)+parseFloat(collectCharges(false))+parseFloat(collectCharges(true));
   //Includes card charges too
   var amtpaid = parseFloat(collectTotalAmountPaid());
   $("totamtpaid").value=currencySymbol+amtpaid.toFixed(2);
   $("totamtpaid_carry").value=amtpaid.toFixed(2);
   $("totamtpaid").disabled=true;
   $("amtstilltxt").value=currencySymbol+parseFloat((amtRec>0)?amtRec:0.0).toFixed(2);
   $("amtstilltxt").disabled=true;
   $("totamtduetxt").value=currencySymbol+(Number(totamtstilldue).toFixed(2));
   $("totamtduetxt").disabled=true;
 /**End of Delegate it to payment details showPayDetails() */
//showPayDetails();
   $("transamt0").disabled=true;
   $("transamt0").value=currencySymbol+amtRec;
   $("transamt_carry0").value=amtRec;
   $("tottransamt0").innerHTML=currencySymbol+amtRec;
   $("total_amount_0").value=amtRec;

   }
   if($("fullCost"))
   {
      $("fullCost").innerHTML=totalAmount;
   }
   if($("totalholidaycost"))
   {
      $("totalholidaycost").innerHTML=totalAmount;
   }
   $("totamtpaidWithoutCardCharge").value=parseFloat(totalAmount-totCardCharge);
   updateCardCharges(0);
}

/* function to add or subtract card charge in deposit section.
   *
  *@param cardCharge represents the charge that has to added or subtracted
   *             totalAmount
   *             action specifies whether to Add  or Subtract card charge
  */
function updateDepositOptionsBycardCharges(oldCardCharge, cardCharge,totalAmount)
{
    if($("totalholidaycost"))
    {
      $("totalholidaycost").innerHTML=totalAmount;
      var depositDiv='';
      if(selectedDepositOption=='lowdeposit')
      {
         depositDiv=$("lowdeposit");
      }
      else if(selectedDepositOption=='deposit')
      {
         depositDiv=$("deposit")
      }
      else if(selectedDepositOption=='full')
      {
          depositDiv=$("fullCost");
      }
      var val=stripChars(depositDiv.innerHTML,currencySymbol+''+','+' ')
      val=  parseFloat(val).toFixed(2);
      var updatedValue = (1*val)-(1*oldCardCharge)
      updatedValue = updatedValue + cardCharge;
      if(depositDiv && depositDiv !=undefined)
      {
        depositDiv.innerHTML= parseFloat(1*updatedValue).toFixed(2);
      }

  }
}
///////////////////////////////////End of functions related to Updating Payment panel //////////////////////////////////////////////


/* Function is a helper used in updatePriceBeatAndDiscountValue().
*
*/
function getBalanceType()
{
   var bal=document.getElementsByName("depositType");
   var balanceTypeText='';
   var balanceType='';
   if(bal.length&&bal.length>0)
   {
      for(i=0;i<bal.length;i++)
      {

         if(bal[i].checked==true)
         {
            balanceTypeText=bal[i].value;
         }
     }
      if(balanceTypeText=='full')
      {
        balanceType='FullBalance';
      }
   }//End if(outer if)
   else
   {
       balanceType='FullBalance';
   }
   return  balanceType;
}
/**********************End of AJAX Helpers ********/

/**********************End of AJAX  related functions *********/


/** The common function resonsible for updating the display fields.
 ** Listeners can be added or removed as necessary.
**/
function displayFieldsWithValuesForDeposit()
{
   refreshToDefaultDepositOptions(PaymentInfo.totalAmount);
   if (PaymentInfo.selectedCardType != null)
   {
      updatePaymentInfoForMultipleTransactions();
      updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
      displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   }
   showPayDetails();
}

/** The common function resonsible for updating the display fields.
 ** Listeners can be added or removed as necessary.
**/
function displayFieldsWithValuesForCard()
{
   refreshToDefaultDepositOptions(PaymentInfo.totalAmount);
   for (var i = 0; i < PaymentInfo.totalTransactions; i++)
   {
      if (PaymentInfo.currentIndex == i)
      {
         updateCardCharges(PaymentInfo.currentIndex);
      }
   }
   displaySelectedDepositOptionsWithCardCharge();
   displayTotalAmounts(bookingConstants.TOTAL_CLASS);
   updateCardChargesInSummaryPanel(PaymentInfo.totalCardCharge);
}

function formatDepositAmount(amountForFormatting)
{
	var amount = setCommaSeperated(amountForFormatting);
	return amount;
}

function setCommaSeperated(value)
{
   if((parseFloat(value).toFixed(2)).length>6)
   {
      var valueLength = value.length;
      var count = (""+parseInt(value/1000)).length;
      value=parseInt(""+(value/1000))+","+value.substring(count, valueLength);
   }
   return value;
}

function updateEssentialFields()
{
   $("total_transamt").value = PaymentInfo.calculatedPayableAmount;
}

/**
 ** Function for updating the deposit amounts for the selected
 ** deposit option only. This is done as per the style class applied
 ** to the input radio element.
**/
function displaySelectedDepositOptionsWithCardCharge()
{
   var depositTypes = $$('input.deposit');
   var deposCardChargeDivs =  $$('.depositAmount');
   var depositAmount = null;
   var cardCharge = null;
   for(var i=0; i<depositTypes.length; i++)
   {
      if (depositTypes[i].checked)
      {
         depositAmount = parseFloat(depositAmountsMap.get((depositTypes[i].value).toUpperCase())).toFixed(2);
         if (!reverseCompleted && !reversalStatus)
         {
            cardCharge = parseFloat(addCardChargesForMultipleTransactions()).toFixed(2);
         }
         else
         {
            cardCharge = parseFloat(1*PaymentInfo.totalCardCharge).toFixed(2);
         }
         var depositAmountForFormatting = parseFloat(1*depositAmount + 1*cardCharge).toFixed(2);
         if (deposCardChargeDivs[i].innerHTML.indexOf(currencySymbol) != -1)
            deposCardChargeDivs[i].innerHTML = currencySymbol + formatDepositAmount(depositAmountForFormatting);
         else
            deposCardChargeDivs[i].innerHTML = formatDepositAmount(depositAmountForFormatting);
      }
   }
   if ($("totalholidaycost"))
   {
      var depositTotalAmt = stripChars($("totalholidaycost").innerHTML,currencySymbol+''+','+' ');
      var depositTotalAmount;
      if (!reverseCompleted && !reversalStatus)
      {
         depositTotalAmount = parseFloat(1*depositTotalAmt).toFixed(2);
         if ($("totalholidaycost").innerHTML.indexOf(currencySymbol) != -1)
            $("totalholidaycost").innerHTML = currencySymbol + formatDepositAmount(parseFloat(1*depositTotalAmount + 1*cardCharge).toFixed(2));
         else
            $("totalholidaycost").innerHTML = formatDepositAmount(parseFloat(1*depositTotalAmount + 1*cardCharge).toFixed(2));
      }
      else
      {
         depositTotalAmount = parseFloat(1*depositTotalAmt - 1*cardCharge).toFixed(2);
         if ($("totalholidaycost").innerHTML.indexOf(currencySymbol) != -1)
            $("totalholidaycost").innerHTML = currencySymbol + formatDepositAmount(parseFloat(1*depositTotalAmount).toFixed(2));
         else
            $("totalholidaycost").innerHTML = formatDepositAmount(parseFloat(1*depositTotalAmount).toFixed(2));
      }
   }
}


function setToPreviousSelection()
{
    if(typeOfCustomer == "payment_CNP")
       $('cnp').checked = true;
    else
       $('cpna').checked = true;
    setCustomerType(typeOfCustomer);
    if(selectedDepositType != null && selectedDepositType != "")
    {
       var depositsTypes = document.getElementsByName("depositType");
       if(depositsTypes && depositsTypes !=undefined && depositsTypes !=null)
       {
          for(var i=0;i<depositsTypes.length; i++)
          {
          if(depositsTypes[i].value == selectedDepositType)
          {
             PaymentInfo.depositType = selectedDepositType.toUpperCase();
             depositsTypes[i].checked = true;
          }
          }
       }
    }
    setCardType();
    paymentPanelController($("payment_type_0"),cardType+"|Dcard","0");
    var expiryArray=expiryDate;
    expiryArray=expiryArray.split("/");
    var expiryMonth= expiryArray[0];
    var expiryYear=expiryArray[1];
    selectedDropDown(cardType, expiryMonth, expiryYear);
    updateCardCharges(0);
    updateCardChargesInSummaryPanel(PaymentInfo.serverCardChargeAmount);
    $('payment_0_cardNumberId').value=maskedCardNumber;
    $('payment_0_nameOnCardId').value=name;
    $('payment_0_postCodeId').value=postCode;
    $('payment_0_securityCodeId').value = maskedCvv;
    if($('issueNumberInput'))
    {
       $('issueNumberInput').value=issueNumber;
    }
    if(responseMsg == "REFERRED")
    {
       $('authcodeText0').style.display="block";
       $('payment_0_authCodeId').style.display="block";
    }
    $('error0').style.display="block";
}

function selectedDropDown(cardType, expiryMonth, expiryYear)
{
   for(j=0;j<$('payment_0_expiryMonthId').length;j++)
   {
      if(($('payment_0_expiryMonthId').options[j].value.split("|"))[0]==expiryMonth)
      {
         $('payment_0_expiryMonthId').selectedIndex=j;
      }
   }
   for(k=0;k<$('payment_0_expiryYear').length;k++)
   {
      if(($('payment_0_expiryYear').options[k].value.split("|"))[0]==expiryYear)
      {
         $('payment_0_expiryYear').selectedIndex=k;
      }
   }
}

function setCardType()
{
   for(i=0;i<$('payment_type_0').length;i++)
   {
      if(($('payment_type_0').options[i].value.split("|"))[0]==cardType)
      {
         $('payment_type_0').selectedIndex=i;
      }
   }
}