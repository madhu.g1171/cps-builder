## cps entry test
UNITTEST.com.tui.uk.config.ConfReaderTestCase.STRING=Hello World
UNITTEST.com.tui.uk.config.ConfReaderTestCase.INT=2147483647
UNITTEST.com.tui.uk.config.ConfReaderTestCase.LONG=9223372036854775807
UNITTEST.com.tui.uk.config.ConfReaderTestCase.BOOLEAN=true
UNITTEST.com.tui.uk.config.ConfReaderTestCase.STRINGVALUES=Hello,World

##test entry override
UNITTEST.com.tui.uk.config.ConfReaderTestCase.OVERRIDE_STRING=Hello
UNITTEST.com.tui.uk.config.ConfReaderTestCase.OVERRIDE_STRING=Hello iScape


## Locale entry test
UNITTEST.com.tui.uk.config.ConfReaderTestCase.LOCALESTRING.en_GB=Hello England
UNITTEST.com.tui.uk.config.ConfReaderTestCase.LOCALESTRING.fr_FR=Hello France

## Negative values test
UNITTEST.com.tui.uk.config.ConfReaderTestCase.-INT=-2147483647
UNITTEST.com.tui.uk.config.ConfReaderTestCase.-LONG=-9223372036854775807
UNITTEST.com.tui.uk.config.ConfReaderTestCase.-BOOLEAN=false

## not a number test
UNITTEST.com.tui.uk.config.ConfReaderTestCase.NAN_INT=ABCD
UNITTEST.com.tui.uk.config.ConfReaderTestCase.NAN_LONG=efg

## Credit Card Validation Binary and Jar files.
cps.CreditCard.location=bin

cps.datacash.HostName=https://testserver.datacash.com/Transaction

#DataCash account password details
cps.99080800.PaymentPassword=5hw76UtbX
cps.99540800.PaymentPassword=R9yuWrG7
cps.99490900.PaymentPassword=GN9pZKywB

# Public key location for Encryption.
cps.encryption.PublicKeyPath=./bin/public.key

# Private key location for Decryption.
cps.encryption.PrivateKeyPath=./bin/private.key

# Confirm Waiting location.
cps.byo.ConfirmWaitingUrl=/thomson/page/shop/byo/waiting/confirmwaiting.page

# The algorithm for encryption.
cps.encryption.Algorithm=RSA
cps.encryptedcard.value=true

# The cps values for tracs connection
cps.promcodeservice.default.Url=http://10.36.10.9:3345/THOMSON/UAT/
cps.promcodeservice.FALCONBYO.Url=http://10.36.10.9:3326/THOMSON/UAT/
cps.promcodeservice.HUGOBYO.Url=http://10.36.10.9:3326/THOMSON/UAT/
cps.promcodeservice.ThomsonBYO.Url=http://10.36.10.9:3345/THOMSON/UAT/
cps.promcodeservice.ThomsonAO.Url=http://10.36.10.9:3345/THOMSON/UAT/
cps.promcodeservice.ThomsonCruise.Url=http://10.36.10.9:3345/THOMSON/UAT/
cps.promcodeservice.WiSHAO.Url=http://10.36.10.9:3345/THOMSON/UAT/
cps.promcodeservice.WiSHBYO.Url=http://10.36.10.9:3326/THOMSON/UAT/
cps.promcodeservice.Cruise.Url=http://10.36.10.9:3345/THOMSON/UAT/

# If tracs connection is with SWS specify pdp url as GREENFIELD/BOOKING and non pdp url as MERLIN/BOOKING
# otherwise use greenfieldbooking and merlinbooking
cps.promcodeservice.pdp.Url=GREENFIELD/BOOKING
cps.promcodeservice.nonpdp.Url=MERLIN/BOOKING

#The connection timeout in miiliseconds, which is be used by tracs.
cps.tracs.ConnectionTimeOut=60000

# The values for capscan connection
cps.capscan.Connection=com.tui.uk.payment.service.capscan.addressfinder.AddressFinderDummyConnectionImpl
#The connection timeout, which will be used by capscan search API.
cps.capscan.ConnectionTimeOut=15
# Note: The Standard port for Pool Manager's is configured as a constant in the capscan.jar - under
# capscan.client.McConnection as POOLMGR_PORT=27920
# If the Pool Manager resides on a non-standard port, the port number may be specified by
# adding a colon followed by the port number. e.g. 127.0.0.1:27920.
cps.capscan.HostIP=10.40.4.77
#Name of the pool or pools to connect to
cps.capscan.ConnectionPool=PAF

# Values for paymentdata cache
cps.paymentdata.cache.maxElementsInMemory=5000
cps.paymentdata.cache.ExpireTime=1800

# Entries for start and expiry year count
cps.expiry.year.count=8
cps.start.year.count=9

## Ground trader specific entries
#DataCash account password details.Here test is the directory name
gt-cps.test.account=99080800
gt-cps.99080800.PaymentPassword=5hw76UtbX
#gt-cps.data2.account=99540800
#gt-cps.99540800.PaymentPassword=R9yuWrG7

#File separator.Usage :Either use \\ or /
gt-cps.fileSeparator=/

//gt-cps test data directory
gt-cps.testdata.dir=./conf/test/gt

#File separator.Usage :Either use \\ or /
gt-cps.fileSeparator=/
gt-cps.inputCSVFolder=input
gt-cps.outputCSVFolder=output

#Post Payment Processors
cps.default.postprocessor=com.tui.uk.payment.processor.BasePostPaymentProcessor
cps.WiSHBYO.postprocessor=com.tui.uk.payment.processor.WishPostPaymentProcessor
cps.WiSHAO.postprocessor=com.tui.uk.payment.processor.WishPostPaymentProcessor
cps.Cruise.postprocessor=com.tui.uk.payment.processor.WishPostPaymentProcessor
cps.OLBP.postprocessor=com.tui.uk.payment.processor.WishPostPaymentProccesor
cps.BRACApplication.postprocessor=com.tui.uk.payment.processor.BracPostPaymentProcessor
cps.ThomsonAO.postprocessor=com.tui.uk.payment.processor.ThomsonPostPaymentProcessor
cps.ThomsonBYO.postprocessor=com.tui.uk.payment.processor.ThomsonPostPaymentProcessor
cps.ThomsonCruise.postprocessor=com.tui.uk.payment.processor.ThomsonPostPaymentProcessor
cps.HUGOBYO.postprocessor=com.tui.uk.payment.processor.FirstChoicePostPaymentProcessor
cps.FALCONBYO.postprocessor=com.tui.uk.payment.processor.FirstChoicePostPaymentProcessor
cps.KRONOS.postprocessor=com.tui.uk.payment.processor.BasePostPaymentProcessor

#CPS Common Key-Store
cps.keystore.KeystoreType=JCEKS
#Below entries should be modified based on the secure keystore deployed in CPS secure environment.
cps.keystore.KeystoreFileName=
cps.keystore.KeystorePassword=cpskeystore

# Merchant url for 3D security
cps.merchantUrl=http://wwww.thomson.co.uk

# error code for fatal and non fatal
cps.fatalError=151,155,156,163,164,166,170,186
cps.nonFatalError=158,159,160,187,162,173,59,157

#Below is brand specific non payment data validation list.
#In any brand, if certain field is mandatory in UI and it has to be validated on server side
#then that field has to be given in the below list so that it can be validated as part of non payment data validation.
#Value should match with one of the NonPaymentDataPanel enumeration value.
cps.ThomsonAO.validationList=houseName,addressLine1,city,postCode,county,dayTimePhone,emailAddress,foreName,surName,childAge,emailAddress1,infantAge,ageBetween65To75,ageBetween76To84
cps.ThomsonBYO.validationList=houseName,addressLine1,city,postCode,county,dayTimePhone,emailAddress,foreName,surName,emailAddress1,childAge,infantAge,ageBetween65To75,ageBetween76To84
cps.ThomsonCruise.validationList=foreName
cps.GENERICJAVA.validationList=
cps.GENERICDOTNET.validationList=
cps.HUGOBYO.validationList=
cps.FALCONBYO.validationList=
cps.Portland.validationList=
cps.TFly.validationList=travelWith,childMeals
cps.FCFO.validationList=foreName,surName,title,childTitle,childForeName,childLastName,infantForeName,infantLastName,addressLine1,city,postCode,dayTimePhone,emailAddress,tourOperatorTermsAccepted,travelWith
cps.KRONOS.validationList=
cps.FCAO.validationList=houseName,addressLine1,city,postCode,county,dayTimePhone,emailAddress,foreName,surName,childAge,emailAddress1,infantAge,ageBetween65To75,ageBetween76To84
cps.WSS.validationList=

#Flag  which indicates whether 3D secure should be enabled or not
cps.ThomsonAO.3DSecure=false
cps.ThomsonBYO.3DSecure=true
cps.ThomsonCruise.3DSecure=false
cps.GENERICJAVA.3DSecure=false
cps.GENERICDOTNET.3DSecure=false
cps.HUGOBYO.3DSecure=false
cps.FALCONBYO.3DSecure=false
cps.Portland.3DSecure=false
cps.TFly.3DSecure=true
cps.FCFO.3DSecure=false
cps.KRONOS.3DSecure=false
cps.FCAO.3DSecure=false
cps.WSS.3DSecure=false

# List of scheme.
cps.3DCardScheme=SWITCH

# term url protocol
cps.termURLProtocol=https

#Java mail entries
cps.mailService.fromAddress= roopesh.s@sonata-software.com
cps.mailService.recipients= roopesh.s@sonata-software.com
cps.mailService.host=NANDIMSG.SONATA.LOCAL
cps.mailService.smtpConnectionString=mail.smtp.host
cps.mailService.refundChequeBusinessInfo=
cps.mailService.refundChequeSubject=Refund Cheque -

# Below are List of schemes for 3DS. For supporting 3D security by scheme,
# card schemes should be added here with comma as separator.
# Following schemes are supported for 3DS by CPS and Datacash.
# Switch,Debit Mastercard,VISA,Maestro,VISA Delta,Mastercard,VISA Electron,VISA Purchasing
cps.default.3DCardScheme=Switch
cps.ThomsonAO.3DCardScheme=Switch
cps.ThomsonBYO.3DCardScheme=Switch,VISA,Mastercard
cps.ThomsonCruise.3DCardScheme=Switch
cps.GENERICJAVA.3DCardScheme=Switch
cps.GENERICDOTNET.3DCardScheme=Switch
cps.HUGOBYO.3DCardScheme=Switch
cps.FALCONBYO.3DCardScheme=Switch,VISA,Mastercard
cps.Portland.3DCardScheme=Switch
cps.TFly.3DCardScheme=Switch
cps.FCFO.3DCardScheme=Switch
cps.KRONOS.3DCardScheme=Switch
cps.FCAO.3DCardScheme=Switch
cps.WSS.3DCardScheme=Switch
cps.GREENFIELDBeach.3DCardScheme=Switch,VISA,Mastercard
cps.GREENFIELDSimply.3DCardScheme=Switch,VISA,Mastercard

# Categories of cards
cps.securelogo.applicablecardgroup=MasterCardGroup,VisaGroup
cps.securelogo.mastercardgroup=MasterCard,Switch,Maestro,Debit Mastercard
cps.securelogo.visagroup= VISA,VISA Delta,VISA Electron,VISA Purchasing
