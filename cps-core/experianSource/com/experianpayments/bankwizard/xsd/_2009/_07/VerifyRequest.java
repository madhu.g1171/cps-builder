
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * 
 *         Verify a bank account with personal details request.
 * 
 *         An optional language attribute can be passed in. English (en) is the
 *         default language returned if the requested type is not supported.
 *       
 * 
 * <p>Java class for VerifyRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifyAccountRequestType"/>
 *         &lt;element name="personalInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifyPersonalRequestType" minOccurs="0"/>
 *         &lt;element name="companyInformation" type="{http://experianpayments.com/bankwizard/xsd/2009/07}VerifyCompanyRequestType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="language" type="{http://www.w3.org/2001/XMLSchema}language" default="en" />
 *       &lt;attribute name="reportString" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ReportString" />
 *       &lt;attribute name="itemisationID" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ItemisationID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyRequest", propOrder = {
    "accountInformation",
    "personalInformation",
    "companyInformation"
})
public class VerifyRequest {

    @XmlElement(required = true)
    protected VerifyAccountRequestType accountInformation;
    protected VerifyPersonalRequestType personalInformation;
    protected VerifyCompanyRequestType companyInformation;
    @XmlAttribute(name = "language")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "language")
    protected String language;
    @XmlAttribute(name = "reportString")
    protected String reportString;
    @XmlAttribute(name = "itemisationID")
    protected String itemisationID;

    /**
     * Gets the value of the accountInformation property.
     * 
     * @return
     *     possible object is
     *     {@link VerifyAccountRequestType }
     *     
     */
    public VerifyAccountRequestType getAccountInformation() {
        return accountInformation;
    }

    /**
     * Sets the value of the accountInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyAccountRequestType }
     *     
     */
    public void setAccountInformation(VerifyAccountRequestType value) {
        this.accountInformation = value;
    }

    /**
     * Gets the value of the personalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link VerifyPersonalRequestType }
     *     
     */
    public VerifyPersonalRequestType getPersonalInformation() {
        return personalInformation;
    }

    /**
     * Sets the value of the personalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyPersonalRequestType }
     *     
     */
    public void setPersonalInformation(VerifyPersonalRequestType value) {
        this.personalInformation = value;
    }

    /**
     * Gets the value of the companyInformation property.
     * 
     * @return
     *     possible object is
     *     {@link VerifyCompanyRequestType }
     *     
     */
    public VerifyCompanyRequestType getCompanyInformation() {
        return companyInformation;
    }

    /**
     * Sets the value of the companyInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link VerifyCompanyRequestType }
     *     
     */
    public void setCompanyInformation(VerifyCompanyRequestType value) {
        this.companyInformation = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLanguage() {
        if (language == null) {
            return "en";
        } else {
            return language;
        }
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the reportString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportString() {
        return reportString;
    }

    /**
     * Sets the value of the reportString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportString(String value) {
        this.reportString = value;
    }

    /**
     * Gets the value of the itemisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemisationID() {
        return itemisationID;
    }

    /**
     * Sets the value of the itemisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemisationID(String value) {
        this.itemisationID = value;
    }

}
