
package com.experianpayments.bankwizard.xsd._2009._07;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import com.experianpayments.bankwizardabsolute.common.xsd._2009._09.PersonalDetailsVerificationType;


/**
 * 
 *         Verification personal details response element.
 *       
 * 
 * <p>Java class for VerifyPersonalResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VerifyPersonalResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personalDetailsScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="addressScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="accountSetupDateMatch" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}PersonalDetailsVerificationType" minOccurs="0"/>
 *         &lt;element name="accountSetupDateScore" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}VerifyScoreType" minOccurs="0"/>
 *         &lt;element name="accountTypeMatch" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}PersonalDetailsVerificationType" minOccurs="0"/>
 *         &lt;element name="accountOwnerMatch" type="{http://experianpayments.com/bankwizardabsolute/common/xsd/2009/09}PersonalDetailsVerificationType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VerifyPersonalResponseType", propOrder = {
    "personalDetailsScore",
    "addressScore",
    "accountSetupDateMatch",
    "accountSetupDateScore",
    "accountTypeMatch",
    "accountOwnerMatch"
})
public class VerifyPersonalResponseType {

    @XmlSchemaType(name = "integer")
    protected Integer personalDetailsScore;
    @XmlSchemaType(name = "integer")
    protected Integer addressScore;
    @XmlSchemaType(name = "token")
    protected PersonalDetailsVerificationType accountSetupDateMatch;
    @XmlSchemaType(name = "integer")
    protected Integer accountSetupDateScore;
    @XmlSchemaType(name = "token")
    protected PersonalDetailsVerificationType accountTypeMatch;
    @XmlSchemaType(name = "token")
    protected PersonalDetailsVerificationType accountOwnerMatch;

    /**
     * Gets the value of the personalDetailsScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPersonalDetailsScore() {
        return personalDetailsScore;
    }

    /**
     * Sets the value of the personalDetailsScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPersonalDetailsScore(Integer value) {
        this.personalDetailsScore = value;
    }

    /**
     * Gets the value of the addressScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAddressScore() {
        return addressScore;
    }

    /**
     * Sets the value of the addressScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAddressScore(Integer value) {
        this.addressScore = value;
    }

    /**
     * Gets the value of the accountSetupDateMatch property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public PersonalDetailsVerificationType getAccountSetupDateMatch() {
        return accountSetupDateMatch;
    }

    /**
     * Sets the value of the accountSetupDateMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public void setAccountSetupDateMatch(PersonalDetailsVerificationType value) {
        this.accountSetupDateMatch = value;
    }

    /**
     * Gets the value of the accountSetupDateScore property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAccountSetupDateScore() {
        return accountSetupDateScore;
    }

    /**
     * Sets the value of the accountSetupDateScore property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAccountSetupDateScore(Integer value) {
        this.accountSetupDateScore = value;
    }

    /**
     * Gets the value of the accountTypeMatch property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public PersonalDetailsVerificationType getAccountTypeMatch() {
        return accountTypeMatch;
    }

    /**
     * Sets the value of the accountTypeMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public void setAccountTypeMatch(PersonalDetailsVerificationType value) {
        this.accountTypeMatch = value;
    }

    /**
     * Gets the value of the accountOwnerMatch property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public PersonalDetailsVerificationType getAccountOwnerMatch() {
        return accountOwnerMatch;
    }

    /**
     * Sets the value of the accountOwnerMatch property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetailsVerificationType }
     *     
     */
    public void setAccountOwnerMatch(PersonalDetailsVerificationType value) {
        this.accountOwnerMatch = value;
    }

}
