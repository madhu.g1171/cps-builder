
package com.experianpayments.bankwizard.xsd._2011._04;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GlobalSearchRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GlobalSearchRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SearchTerms" type="{http://experianpayments.com/bankwizard/xsd/2011/04}SearchTermList" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="page" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="pageSize" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="reportString" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ReportString" />
 *       &lt;attribute name="itemisationID" type="{http://experianpayments.com/bankwizard/common/xsd/2009/09}ItemisationID" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GlobalSearchRequest", propOrder = {
    "searchTerms"
})
public class GlobalSearchRequest {

    @XmlElement(name = "SearchTerms")
    protected SearchTermList searchTerms;
    @XmlAttribute(name = "page", required = true)
    protected int page;
    @XmlAttribute(name = "pageSize", required = true)
    protected int pageSize;
    @XmlAttribute(name = "reportString")
    protected String reportString;
    @XmlAttribute(name = "itemisationID")
    protected String itemisationID;

    /**
     * Gets the value of the searchTerms property.
     * 
     * @return
     *     possible object is
     *     {@link SearchTermList }
     *     
     */
    public SearchTermList getSearchTerms() {
        return searchTerms;
    }

    /**
     * Sets the value of the searchTerms property.
     * 
     * @param value
     *     allowed object is
     *     {@link SearchTermList }
     *     
     */
    public void setSearchTerms(SearchTermList value) {
        this.searchTerms = value;
    }

    /**
     * Gets the value of the page property.
     * 
     */
    public int getPage() {
        return page;
    }

    /**
     * Sets the value of the page property.
     * 
     */
    public void setPage(int value) {
        this.page = value;
    }

    /**
     * Gets the value of the pageSize property.
     * 
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * Sets the value of the pageSize property.
     * 
     */
    public void setPageSize(int value) {
        this.pageSize = value;
    }

    /**
     * Gets the value of the reportString property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportString() {
        return reportString;
    }

    /**
     * Sets the value of the reportString property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportString(String value) {
        this.reportString = value;
    }

    /**
     * Gets the value of the itemisationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getItemisationID() {
        return itemisationID;
    }

    /**
     * Sets the value of the itemisationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setItemisationID(String value) {
        this.itemisationID = value;
    }

}
