<?xml version="1.0" encoding="utf-8"?>
<xsd:schema
  targetNamespace="http://experianpayments.com/bankwizard/xsd/2011/04"
  xmlns="http://www.w3.org/2001/XMLSchema"
  xmlns:xsd="http://www.w3.org/2001/XMLSchema"
  xmlns:bw="http://experianpayments.com/bankwizard/xsd/2011/04"
  xmlns:bwc="http://experianpayments.com/bankwizard/common/xsd/2009/09"
  xmlns:bws="http://experianpayments.com/bankwizard/xsd/2009/07"
  elementFormDefault="qualified">

  <xsd:import namespace="http://experianpayments.com/bankwizard/common/xsd/2009/09" schemaLocation="BankWizardCommon-v1-0.xsd"/>
  <xsd:import namespace="http://experianpayments.com/bankwizard/xsd/2009/07" schemaLocation="BankWizardService-v1-0.xsd"/>

  <xsd:simpleType name="Categorization">
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="CreditUnion"/>
      <xsd:enumeration value="BuildingSociety"/>
      <xsd:enumeration value="Bank"/>
      <xsd:enumeration value="Business"/>
      <xsd:enumeration value="Charity"/>
      <xsd:enumeration value="Personal"/>
      <xsd:enumeration value="CentralGovernment"/>
      <xsd:enumeration value="LocalGovernment"/>
      <xsd:enumeration value="Unknown"/>
    </xsd:restriction>
  </xsd:simpleType>

  <xsd:complexType name="AccountInformation">
    <xsd:sequence>
      <xsd:element name="Categorization" minOccurs="1" maxOccurs="1" type="bw:Categorization" />
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="SearchExpression" abstract="true">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="page" type="xsd:int" use="required"/>
        <xsd:attribute name="pageSize" type="xsd:int" use="required"/>
        <xsd:attribute name="reportString" type="bwc:ReportString" use="optional"/>
        <xsd:attribute name="itemisationID" type="bwc:ItemisationID" use="optional"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:complexType name="BranchSearch">
    <xsd:simpleContent>
      <xsd:extension base="bw:SearchExpression">
        <xsd:attribute name="country" type="bwc:ISO3166-1" use="required" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:complexType name="SwiftSearch">
    <xsd:simpleContent>
      <xsd:extension base="bw:SearchExpression" />
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:complexType name="SearchResult" abstract="true">
    <xsd:attribute name="Token" type="xsd:string" use="required" />
    <xsd:attribute name="NewToken" type="xsd:boolean" use="required" />
    <xsd:attribute name="Size" type="xsd:int" use="required" />
  </xsd:complexType>

  <xsd:complexType name="BranchData">
    <xsd:complexContent>
      <xsd:extension base="bws:BranchDataType">
        <xsd:attribute name="Token" type="xsd:string" use="required" />
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <xsd:complexType name="SwiftData">
    <xsd:sequence>
      <xsd:element name="Name" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="Address" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="BranchInformation" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="ZipCode" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="City" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="Country" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="ISOCountryCode" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="CountryStateProvince" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="IbanCountryCode" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="BranchBic" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="IbanBicCode" minOccurs="0" maxOccurs="1" type="xsd:string" />
      <xsd:element name="UniqueBic" minOccurs="0" maxOccurs="1" type="xsd:string" />
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="BranchSearchResults">
    <xsd:complexContent>
      <xsd:extension base="bw:SearchResult">
        <xsd:sequence>
          <xsd:element name="BranchData" minOccurs="0" maxOccurs="unbounded" type="bw:BranchData" />
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <xsd:complexType name="SwiftSearchResults">
    <xsd:complexContent>
      <xsd:extension base="bw:SearchResult">
        <xsd:sequence>
          <xsd:element name="SwiftData" minOccurs="0" maxOccurs="unbounded" type="bw:SwiftData" />
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <xsd:complexType name="GetInputWithSearchRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Request the Bank Wizard checking levels available for an ISO 3166-1 two-alpha country code the input BBAN details
        associated with the checking levels.

        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="bwc:ISO3166-1">
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:complexType name="CountryInputMetaData">
    <xsd:sequence>
      <xsd:element name="countryInput"  minOccurs="1" maxOccurs="1" nillable="false" type="bws:GetCountryInputResponse" />
      <xsd:element name="searchCategories"  minOccurs="1" maxOccurs="1" nillable="false" type="bw:SearchCategories" />
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="SearchCategories">
    <xsd:sequence>
      <xsd:element name="category"  minOccurs="0" maxOccurs="unbounded" type="xsd:string" />
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="GetInputWithSearchResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        The BBAN inputs for various Bank Wizard checking levels associated the requested ISO 3166-1 two-alpha country code,
        also returns the relevance Search categories which could be used to filter search for the given country.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="countryInputMetaData"  minOccurs="1" maxOccurs="1" type="bw:CountryInputMetaData" />
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="GetLookupIbanInputRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Request the Bank Wizard checking levels available for an ISO 3166-1 two-alpha country code the input BBAN details
        associated with the checking levels.

        An optional language attribute can be passed in. English (en) is the
        default if the requested type is not supported.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:simpleContent>
      <xsd:extension base="bwc:ISO3166-1">
        <xsd:attribute name="language" type="xsd:language" use="optional" default="en" />
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:complexType name="GetLookupIbanInputResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        The BBAN inputs for various Bank Wizard checking levels associated the requested ISO 3166-1 two-alpha country code.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:sequence>
      <xsd:element name="LookupInputBBAN"  minOccurs="1" maxOccurs="5" type="bws:BBANResponseType" />
    </xsd:sequence>
    <xsd:attribute name="ISOCountry" type="bwc:ISO3166-1" use="required"/>
  </xsd:complexType>

  <xsd:complexType name="LookupIbanRequest">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Looukup IBAN and BIC for given BBAN.
      </xsd:documentation>
    </xsd:annotation>

    <xsd:sequence>
      <xsd:element name="BBAN" minOccurs="1" maxOccurs="4" type="bws:BBANBaseType"/>
    </xsd:sequence>

    <xsd:attribute name="ISOCountry" type="bwc:ISO3166-1" use="required"/>
    <xsd:attribute name="language" type="xsd:language" use="optional" default="en" />
    <xsd:attribute name="reportString" type="bwc:ReportString" use="optional"/>
    <xsd:attribute name="itemisationID" type="bwc:ItemisationID" use="optional"/>
  </xsd:complexType>

  <xsd:complexType name="LookupIbanResponse">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Results of an IBAN and BIC lookup.

        The following are returned:
          *<code>BBAN</code>: transposed BBANs
          *<code>IBAN</code>
          *<code>Routing BIC</code>
          *<code>Result</code>:
      </xsd:documentation>
    </xsd:annotation>

    <xsd:sequence>
      <xsd:element name="BBAN" minOccurs="1" maxOccurs="4" type="bws:BBANBaseType"/>
      <xsd:element name="IBAN" minOccurs="0" maxOccurs="1" type="bwc:IBAN"/>
      <xsd:element name="RoutingBic" minOccurs="0" maxOccurs="1" type="bw:BIC"/>

      <xsd:element name="Result" minOccurs="1" maxOccurs="1" type="bw:LookupResult"/>
    </xsd:sequence>
  </xsd:complexType>

  <xsd:simpleType name="BIC">
    <xsd:restriction base="xsd:string">
          <xsd:minLength value="11"/>
          <xsd:maxLength value="11"/>
        </xsd:restriction>
  </xsd:simpleType>

  <xsd:simpleType name="LookupResult">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Result match enumeration.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="Match"/>
      <xsd:enumeration value="No Match"/>
      <xsd:enumeration value="Not Supported"/>
      <xsd:enumeration value="Check Match"/>
    </xsd:restriction>
  </xsd:simpleType>

  <xsd:complexType name="GlobalSearchRequest">
      <xsd:sequence>
        <xsd:element name="SearchTerms"  minOccurs="0" maxOccurs="1" type="bw:SearchTermList"/>
      </xsd:sequence>
      <xsd:attribute name="page" type="xsd:int" use="required"/>
      <xsd:attribute name="pageSize" type="xsd:int" use="required"/>
      <xsd:attribute name="reportString" type="bwc:ReportString" use="optional"/>
      <xsd:attribute name="itemisationID" type="bwc:ItemisationID" use="optional"/>
  </xsd:complexType>

  <xsd:complexType name="GlobalSearchResponse">
    <xsd:complexContent>
      <xsd:extension base="bw:SearchResult">
        <xsd:sequence>
          <xsd:element name="SearchTerms" minOccurs="1" maxOccurs="1" type="bw:SearchTermList" />
          <xsd:element name="ResultsList" minOccurs="0" maxOccurs="1" type="bw:SearchResultList" />
        </xsd:sequence>
      </xsd:extension>
    </xsd:complexContent>
  </xsd:complexType>

  <xsd:complexType name="SearchTermList">
    <xsd:sequence>
      <xsd:element name="SearchTerm" minOccurs="1" maxOccurs="unbounded" type="bw:CategoryElement" />
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="FieldElement">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="fieldName" type="xsd:string"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:complexType name="CategoryElement">
    <xsd:simpleContent>
      <xsd:extension base="xsd:string">
        <xsd:attribute name="category" type="bw:CategoryType" use="optional"/>
      </xsd:extension>
    </xsd:simpleContent>
  </xsd:complexType>

  <xsd:simpleType name="CategoryType">
    <xsd:annotation>
      <xsd:documentation xml:lang="en">
        Allowed search categories enumeration.
      </xsd:documentation>
    </xsd:annotation>
    <xsd:restriction base="xsd:token">
      <xsd:enumeration value="address"/>
      <xsd:enumeration value="bank"/>
      <xsd:enumeration value="bic"/>
      <xsd:enumeration value="country"/>
      <xsd:enumeration value="token"/>
    </xsd:restriction>
  </xsd:simpleType>

  <xsd:complexType name="SearchResultList">
    <xsd:sequence>
      <xsd:element name="ResultItem"  minOccurs="1" maxOccurs="unbounded" type="bw:ResultFieldList"/>
    </xsd:sequence>
  </xsd:complexType>

  <xsd:complexType name="ResultFieldList">
    <xsd:sequence>
      <xsd:element name="Field"  minOccurs="1" maxOccurs="unbounded" type="bw:FieldElement"/>
    </xsd:sequence>
  </xsd:complexType>

</xsd:schema>
