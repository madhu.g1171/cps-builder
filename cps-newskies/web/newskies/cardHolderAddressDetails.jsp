<h4>Card holder address details</h4>
<p>To help ensure your card details remain secure, please confirm the address of the card holder.</p>
<div class="formError hide">
   <p class="formErrorMessage">Unfortunately we could not that too</p>
</div>
<ul class="formFormat cardHolderForm">
   <li class="indent" style="overflow:hidden;">
      <div class="checkboxWrap floatContainer">
         <input type="checkbox" id="useAddress" name="autoCheckCardAddress" onclick="AddressPopulationHandler.handle()"/>
		 <label for="useAddress">Same address as lead passenger</label>
      </div>
   </li>
   <li class="cardHouseName cardHouseNameError">
      <label for="cardHouseName"><div class="card_holder">House name/No</div></label>
      <div class="add_input"><input id="cardHouseName" type="alphanumericspec" name="payment_0_street_address1" class="textfield" maxlength="20" gfv_required="required" alt="CardHolder House Name/No" value="${bookingInfo.paymentDataMap['payment_0_street_address1']}"/></div>
   </li>
   <li class="cardAddress1 cardAddress1Error ">
      <label for="cardAddress1"><div class="card_holder_sec">Address 1</div></label>
      <div class="add_input"><input type="alpha" id="cardAddress1" name="payment_0_street_address2" class="textfield" maxlength="25" gfv_required="required" alt="CardHolder Street Address1" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address2']}"/>"/></div>
   </li>
   <li class="cardAddress2 cardAddress2Error ">
      <label for="cardAddress2"><div class="card_holder_sec">Address 2</div></label>
      <div class="add_input"><input id="cardAddress2" type="alpha" name="payment_0_cardHolderAddress2" class="textfield" maxlength="25" alt="CardHolder Street Address2" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_cardHolderAddress2']}"/>"/></div>
   </li>
   <li class="cardTownCity cardTownCityError ">
      <label for="cardTownCity"><div class="card_holder_sec">Town/City</div></label>
     <div class="add_input"><input type="alpha" id="cardTownCity" name="payment_0_street_address3" class="textfield" maxlength="25" gfv_required="required" alt="CardHolder Town/City" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address3']}"/>"/></div>
   </li>
   <li class="cardCounty cardCountyError ">
      <label for="cardCounty"><div class="card_holder_sec">County</div></label>
      <div class="add_input"><input id="cardCounty" type="alpha" name="payment_0_street_address4" class="textfield" maxlength="16" alt="CardHolder County" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_street_address4']}"/>"/></div>
   </li>
   <li class="cardcountry cardcountryError ">
	  <label for="cardcountry"><div class="card_holder_sec">Country</div></label>
      <div class="add_input">
	  <div style="margin-left: 8px; width: 191px; " class="styleSelect">
	  <c:set var="selectedCountryName" value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}" scope="page"/>
	  <select name="payment_0_selectedCountry" id="cardcountry"  requiredError="Your country"  
          value="<c:out value="${bookingInfo.paymentDataMap['payment_0_selectedCountry']}"/>"           gfv_required="required" alt="CardHolder Country"
          onchange="cardpostcodeValidation.cardpostcodeChange()">
	  <option id="pleaseSelect" value='' selected="selected">Please select</option>
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
             <c:if test='${countryCode=="GB"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
             <c:if test='${countryCode =="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
         <option class="select-dash" disabled="disabled">---------------------------</option>
         <c:forEach var="CountriesList" items="${bookingComponent.countryMap}">
            <c:set var="countryCode" value="${CountriesList.key}" />
            <c:set var="countryName" value="${CountriesList.value}" />
             <c:if test='${countryCode!="GB" && countryCode !="IE"}'>	
            <option value='<c:out value="${countryName}"/>'
              <c:if test='${selectedCountryName==countryName}'>selected='selected'</c:if>>
               <c:out value="${countryName}" />
            </option>
            </c:if>
         </c:forEach>
         </select>
		 </div>
	  </div>
   </li>
   <li class="cardPostCode cardPostCodeError ">
	  <label for="cardPostCode"><div class="card_holder_sec">Postcode</div></label>
      <div class="add_input_pin">
	  
	  <input type="cardpostcode" id="cardPostCode" name="payment_0_postCode" class="textfieldSmall" maxlength="8" gfv_required_cardpostcode="required_cardpostcode" alt="CardHolder Post Code" value="<c:out value="${bookingInfo.paymentDataMap['payment_0_postCode']}"/>"/>
	  
	  </div>
   </li>  
   
</ul>