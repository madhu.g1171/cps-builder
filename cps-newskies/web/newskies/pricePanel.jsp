<fmt:formatNumber var="balanceDue"
	value="${bookingInfo.calculatedTotalAmount.amount}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" />
<fmt:formatNumber var="amountPaid"
	value="${bookingComponent.nonPaymentData['amount_paid']}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" />
	<fmt:formatNumber var="totalAmount"
	value="${bookingComponent.nonPaymentData['transaction_total']}" type="number"
	maxFractionDigits="2" minFractionDigits="2" groupingUsed="false" pattern="######.##" />

<div id="summaryTable">
<div id="summaryTotal" class="properPadding">
<h3><span class="icon runningTotalIco" style="margin-right:5px"></span>RUNNING TOTAL</h3>
<div class="dividerLineNew"></div>
<c:set var="priceComponentLength"
	value="${fn:length(bookingComponent.priceComponents)}" /> <c:if
	test="${priceComponentLength > 0}">

	<c:forEach var="priceComponent"
		items="${bookingComponent.priceComponents}">
		<c:choose>
			<c:when
				test="${fn:contains(priceComponent.itemDescription, 'FLIGHT OUT')}">
				<h4 class="out"><c:out
					value="${priceComponent.itemDescription}" /></h4>
					<span class="flightNameOut"> <c:if test="${not empty bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode']}"> 
									&nbsp;<b style="margin-right: 5px;">&#8211;</b><c:out value="${bookingComponent.nonPaymentData['outBoundFlight_operatingAirlineCode']}" />
			</c:if> </span>
			</c:when>
			<c:when
				test="${fn:contains(priceComponent.itemDescription,'FLIGHT HOME')}">
				<div class="dividerLineFull"></div>
				<h4 class="in"><c:out value="${priceComponent.itemDescription}" /></h4>
				<span class="flightNameIn"> <c:if test="${not empty bookingComponent.nonPaymentData['inBoundFlight_operatingAirlineCode']}">&nbsp; 
				<b style="margin-right: 5px;">&#8211;</b> <c:out value="${bookingComponent.nonPaymentData['inBoundFlight_operatingAirlineCode']}" />
			</c:if> </span>
			</c:when>
		</c:choose></div>
		<%--START OF FLIGHT OUT DETAILS --%>
		<div class="timeAirportPadding"><c:if
			test="${priceComponent.itemDescription =='FLIGHT OUT'}">
			<div class="depDate"><c:if
				test="${not empty bookingComponent.nonPaymentData['outBoundFlight_departureDate']}">
				<c:out
					value="${bookingComponent.nonPaymentData['outBoundFlight_departureDate']}" />
			</c:if></div>
			<div class="depTime"><c:if
				test="${not empty bookingComponent.nonPaymentData['outBoundFlight_departureTime'] && not empty bookingComponent.nonPaymentData['outBoundFlight_arrivalTime']}">
				<c:set var="Fo_lengthDepTime" value="${fn:length(bookingComponent.nonPaymentData['outBoundFlight_departureTime'])}"/>
					<c:set var="Fo_meridianDepTime" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_departureTime'],Fo_lengthDepTime-2,Fo_lengthDepTime)}"/>
					<c:set var="Fo_dep_Time" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_departureTime'],0,Fo_lengthDepTime-3)}"/>
					<c:set var="Fo_lengthArrTime" value="${fn:length(bookingComponent.nonPaymentData['outBoundFlight_arrivalTime'])}"/>
					<c:set var="Fo_meridianArrTime" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_arrivalTime'],Fo_lengthArrTime-2,Fo_lengthArrTime)}"/>
					<c:set var="Fo_arr_Time" value="${fn:substring(bookingComponent.nonPaymentData['outBoundFlight_arrivalTime'],0,Fo_lengthArrTime-3)}"/>
				<div class="icon time"></div><div class="floatLeft"><c:out
					value="${Fo_dep_Time}"/></div>

				


			</c:if></div>


			<div class="clear"></div>
			<div class="airpottDetails"><c:if
				test="${not empty bookingComponent.nonPaymentData['outBoundFlight_departureAirportName'] 	&& not empty bookingComponent.nonPaymentData['outBoundFlight_arrivalAirportName']}">
				<span class="floatLeft outAirport"> <c:out
					value="${fn:toUpperCase(bookingComponent.nonPaymentData['outBoundFlight_departureAirportName'])}" />
				</span>
				
				<div class="dividerLine"></div>
			</c:if>
			<div style="clear: both;"></div>
			</div><h4 class="added">Added</h4>
		</c:if></div>
		<div class="clear"></div>

		<%--END OF FLIGHT OUT DETAILS --%>
		<%--START OF FLIGHT HOME DETAILS --%>
		<div class="timeAirportPadding">
		<c:if test="${priceComponent.itemDescription =='FLIGHT HOME'}">
			<div class="depDate"><c:if
				test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureDate']}">
				<c:out
					value="${bookingComponent.nonPaymentData['inBoundFlight_departureDate']}" />

			</c:if></div>
			<div class="depTime">
			<c:if
				test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureTime'] && not empty bookingComponent.nonPaymentData['inBoundFlight_arrivalTime']}">
				<c:set var="Fh_lengthDepTime" value="${fn:length(bookingComponent.nonPaymentData['inBoundFlight_departureTime'])}"/>
					<c:set var="Fh_meridianDepTime" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_departureTime'],Fh_lengthDepTime-2,Fh_lengthDepTime)}"/>
					<c:set var="Fh_dep_Time" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_departureTime'],0,Fh_lengthDepTime-3)}"/>
					<c:set var="Fh_lengthArrTime" value="${fn:length(bookingComponent.nonPaymentData['inBoundFlight_arrivalTime'])}"/>
					<c:set var="Fh_meridianArrTime" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_arrivalTime'],Fh_lengthArrTime-2,Fh_lengthArrTime)}"/>
					<c:set var="Fh_arr_Time" value="${fn:substring(bookingComponent.nonPaymentData['inBoundFlight_arrivalTime'],0,Fh_lengthArrTime-3)}"/>

			        <div class="icon time"></div><div class="floatLeft"><c:out
					value="${Fh_dep_Time}"/></div>
				
				<div class="floatLeft">
                  </div>
				</c:if></div>
			<div class="clear"></div>
			<div class="airpottDetails"><c:if
				test="${not empty bookingComponent.nonPaymentData['inBoundFlight_departureAirportName'] && not empty bookingComponent.nonPaymentData['inBoundFlight_arrivalAirportName']}">
				<span class="floatLeft inAirport"> <c:out
					value="${fn:toUpperCase(bookingComponent.nonPaymentData['inBoundFlight_departureAirportName'])}" />
				</span>
				
				<div class="dividerLine"></div>
			</c:if>
			<div style="clear: both;"></div>
			</div>
			<h4 class="added">Added</h4>
		</c:if></div>
		<div class="clear"></div>
		<%--END OF FLIGHT HOME DETAILS --%>
		<div class="flightpsgrDetails"><c:if
			test="${priceComponent.itemDescription!='FLIGHT OUT'&& priceComponent.itemDescription!='FLIGHT HOME'&& priceComponent.itemDescription !=null}">
			<div>
			<div class="psgrDetails"><c:out
				value="${priceComponent.itemDescription}" escapeXml="false" /></div>
			<div class="qtyDetails">X <c:if
				test="${priceComponent.quantity>0}">
				<c:out value="${priceComponent.quantity}" escapeXml="false" />
			</c:if></div>
			<c:if
				test="${priceComponent.amount.amount>=0 && priceComponent.itemDescription !=null}">
				<div class="amtDetails"><c:out value="${currencySymbol}"
					escapeXml="false" /><fmt:formatNumber
					value="${priceComponent.amount.amount}" type="number"
					maxFractionDigits="2" minFractionDigits="2" pattern="#,##,###.##" />
				</div>
			</c:if></div>
			<div style="clear: both;"></div>
		</c:if>
	</c:forEach>
</c:if>
</div>
<div class="clear"></div>
<div class="dividerLineFullNew"></div>
	<div class="totalDetails">


<div class="clear"></div>
<div class="paidAmt">

<div class="balDue">
<div class="balText">TOTAL</div>
<div class="balValue"><c:out value="${currencySymbol}" escapeXml="false"/>
<c:out value="${balanceDue}"/></div>
</div>
<div class="clear"></div>
</div>
</div>
	<div id="creditCardCharges">
	<c:if test="${bookingComponent.totalAmount.currency.currencyCode=='GBP'}">
	<c:if test="${applyCreditCardSurchargeGBP eq 'true'}">
   <a class="readMoreCharges" target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Our Credit Card Charges</a>
   </c:if>
   </c:if>
   <c:if test="${bookingComponent.totalAmount.currency.currencyCode=='EUR'}">
	<c:if test="${applyCreditCardSurchargeEUR eq 'true'}">
   <a class="readMoreCharges" target="_blank" href="http://www.thomson.co.uk/editorial/legal/credit-card-payments.html">Our Credit Card Charges</a>
   </c:if>
   </c:if>
   <p style="font-size: 11px;color: #aaa;">All extras are non-refundable. Check the running total above for confirmation of your selections and prices.</p>
   </div>
</div>
